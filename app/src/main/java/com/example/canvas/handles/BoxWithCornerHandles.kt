package com.example.canvas.handles

import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.dp

@Composable
fun BoxWithCornerHandles(
    modifier: Modifier = Modifier,
    contentAlignment: Alignment = Alignment.TopStart,
    handleSize: Dp = 10.dp,
    offset: DpOffset = DpOffset.Zero,
    onTopStartDrag: (Offset) -> Unit = { },
    onTopEndDrag: (Offset) -> Unit = { },
    onBottomStartDrag: (Offset) -> Unit = { },
    onBottomEndDrag: (Offset) -> Unit = { },
    onDragStart: (Offset) -> Unit = { },
    onDragEnd: () -> Unit = { },
    content: @Composable () -> Unit = { },
) {
    val offsetX = handleSize + offset.x // + 8.dp
    val offsetY = handleSize + offset.y // + 6.dp

    val topStart = HandleData(
        offset = DpOffset(x = -offsetX, y = -offsetY),
        onDrag = onTopStartDrag
    )
    val topEnd = HandleData(
        offset = DpOffset(x = offsetX, y = -offsetY),
        alignment = Alignment.TopEnd,
        onDrag = onTopEndDrag
    )
    val bottomStart = HandleData(
        offset = DpOffset(x = -offsetX, y = offsetY),
        alignment = Alignment.BottomStart,
        onDrag = onBottomStartDrag
    )
    val bottomEnd = HandleData(
        offset = DpOffset(x = offsetX, y = offsetY),
        alignment = Alignment.BottomEnd,
        onDrag = onBottomEndDrag
    )

    Box(
        modifier = modifier,
        contentAlignment = contentAlignment,
    ) {
        content()

        listOf(
            topStart, topEnd,
            bottomStart, bottomEnd
        ).forEach { data ->
            Handle(
                size = handleSize,
                alignment = data.alignment,
                offset = data.offset,
                onDrag = data.onDrag,
                onDragStart = onDragStart,
                onDragEnd = onDragEnd
            )
        }
    }
}