package com.example.canvas.handles

import androidx.compose.foundation.border
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.dp

@Composable
fun BoxScope.Handle(
    modifier: Modifier = Modifier,
    size: Dp = 10.dp,
    borderWidth: Dp = 1.dp,
    alignment: Alignment = Alignment.TopStart,
    offset: DpOffset = DpOffset.Zero,
    shape: Shape = RectangleShape,
    color: Color = Color.Black,
    onDrag: (Offset) -> Unit = { },
    onDragStart: (Offset) -> Unit = { },
    onDragEnd: () -> Unit = { },
) {
    Box(
        modifier = modifier
            .size(size)
            .align(alignment)
            .offset(x = offset.x, y = offset.y)
            .border(
                width = borderWidth,
                color = color,
                shape = shape
            )
            .clip(shape)
            .pointerInput(Unit) {
                detectDragGestures(
                    onDragStart = onDragStart,
                    onDragEnd = onDragEnd,
                ) { change, dragAmount ->
                    change.consume()

                    onDrag(
                        Offset(
                            x = dragAmount.x,
                            y = dragAmount.y
                        )
                    )
                }
            }
    )
}
