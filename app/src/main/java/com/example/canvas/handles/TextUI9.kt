package com.example.canvas.handles

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.TransformOrigin
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import com.example.canvas.composable.BasicBoardItemUI
import com.example.canvas.utils.conditional
import kotlin.math.roundToInt

@Composable
fun TextUI9(
    startOffsetX: Dp = 100.dp,
    startOffsetY: Dp = 100.dp,
) {
    val density = LocalDensity.current
    var offsetX by remember { mutableStateOf(startOffsetX) }
    var offsetY by remember { mutableStateOf(startOffsetY) }

    var scale: Float by remember { mutableStateOf(1f) }
    var size: DpSize by remember { // todo data.size
        mutableStateOf(
            DpSize(
                width = 164.dp,
                height = 164.dp
            )
        )
    }
    val scaleSize: DpSize by remember { // todo inner scaled+zoomed size
        derivedStateOf {
            size.times(scale)
        }
    }
    /////////////////////////////////////////////////////////////////
    BlockableBoardItem(
        modifier = Modifier
            .offset(offsetX, offsetY),
        contentPadding = PaddingValues(top = 8.dp, start = 8.dp, end = 8.dp, bottom = 16.dp),
        blockedCanvas = {
//            drawBlockedCanvas()
        },
    ) {
        /////////////////////////////////////////////////////////////////
        BasicBoardItemUI(
            modifier = Modifier
                .size(size)
                .conditional(
                    condition = scale != 1f,
                    ifTrue = {
                        Modifier.graphicsLayer(
                            scaleX = scale,
                            scaleY = scale,
                            // todo Use transformOrigin if content isn't centered in ResizableBox6
                            transformOrigin = TransformOrigin(0f, 0f)
                        )
                    }
                ),
            boardItemId = "1",
            isSelected = true,
            isEditMode = false,
            isBlocked = false,
            isLocked = false,
            onSelectChange = { },
            onEnableEditMode = { },
            onMove = { },
            onDragEnd = { },
            backgroundCanvas = {
                drawRect(
                    color = Color.Yellow,
                    size = this.size
                )
            },
        ) {
            Column(
                modifier = Modifier
                    .size(size)
                    .padding(16.dp),
                verticalArrangement = Arrangement.SpaceBetween
            ) {
                Box(
                    modifier = Modifier
                        .weight(weight = 0.8f)
                        .background(Color(0xFFBABABA))
                        .fillMaxWidth(),
                ) {
                    Text(
                        modifier = Modifier.fillMaxWidth(), // While in box
                        text = "TextUI9"
                    )
                }

                Text(
                    modifier = Modifier.weight(0.2f),
                    text = "Author",
                )
            }
        }

        ///////////////////////////////////////////////////////////

        ResizableBox8(
            // Box with handles and border. Don't scale it. Set its size.
            modifier = Modifier
                .size(scaleSize) // set scaled size
                .border(width = 1.dp, color = Color.Blue),
            handleOffset = DpOffset(x = (-4).dp, y = (-4).dp), //todo refactor to not need offset
            onScale = { scaleOffset, newScale ->
                scale += newScale

                if (scaleOffset != Offset.Zero) {
                    with(density) {
                        offsetX += (size.width.toPx().roundToInt() * scaleOffset.x).toDp()
                        offsetY += (size.height.toPx().roundToInt() * scaleOffset.y).toDp()
                    }
                }
            },
        )
    }
}

@Composable
fun BlockableBoardItem(
    modifier: Modifier = Modifier,
    contentPadding: PaddingValues = PaddingValues(),
    blockedCanvas: DrawScope.() -> Unit = { },
    content: @Composable BoxScope.() -> Unit = { },
) {
    Column(
        modifier = modifier
    ) {
        Box(
            modifier = Modifier
                .drawWithContent {
                    drawContent()
                    if (false) {
                        blockedCanvas()
                    }
                }
                .padding(contentPadding),
            content = content
        )
    }
}