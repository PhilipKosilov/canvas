package com.example.canvas.handles

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.TransformOrigin
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import kotlin.math.pow
import kotlin.math.round
import kotlin.math.roundToInt

@Composable
fun TextUI8(
    startOffsetX: Dp = 100.dp,
    startOffsetY: Dp = 100.dp,
) {
    val density = LocalDensity.current
    var offsetX by remember { mutableStateOf(startOffsetX) }
    var offsetY by remember { mutableStateOf(startOffsetY) }

    var scale: Float by remember { mutableStateOf(1f) }
    var size: DpSize by remember {
        mutableStateOf(
            DpSize(
                width = 100.dp,
                height = 50.dp
            )
        )
    } // Size in firebase?
    val scaleSize: DpSize by remember(size, scale) { // Inner size?
        derivedStateOf {
            size.times(scale)
        }
    }


    /////////////////////////////////////////////////////////////////
    Box( // Blockable box
        modifier = Modifier
            .offset(offsetX, offsetY)
    ) {
        /////////////////////////////////////////////////////////////////
        ResizableBox8(
            // Box with handles and border. Don't scale it. Set its size.
            modifier = Modifier
                .size(scaleSize) // set scaled size
//                .padding(16.dp) // todo this also scales (causes unevenness)
                .background(Color.LightGray)
                .border(width = 1.dp, color = Color.Blue),
            onScale = { scaleOffset, newScale ->
                scale += newScale

                if (scaleOffset != Offset.Zero) {
//                    offsetX += size.width.times(scaleOffset.x) // okay this works. Now hwat?
//                    offsetY += size.height.times(scaleOffset.y) // should be 0

                    with(density) {
                        // Semi work
                        offsetX += (size.width.toPx().roundToInt() * scaleOffset.x).toDp()
                        offsetY += (size.height.toPx().roundToInt() * scaleOffset.y).toDp()
                    }
                }
            },
        )

        Box(
            // Content box. Scale its content
            modifier = Modifier
                .size(size) // todo UNSCALED size
                .graphicsLayer(
                    scaleX = scale,
                    scaleY = scale,
                    // todo Use transformOrigin if content isn't centered in ResizableBox6
                    transformOrigin = TransformOrigin(0f, 0f)
                ),
            contentAlignment = Alignment.Center,
        ) {
            // Content to scale
            Text(
                modifier = Modifier
                    .padding(16.dp) // todo placing it here works fine.
                    .matchParentSize()
                    .background(Color(0xFFBABABA)),
                text = "TextUI8"
            )
        }
    }
}

@Composable
fun ResizableBox8(
    modifier: Modifier = Modifier,
    contentAlignment: Alignment = Alignment.TopStart,
    scale: Float = 1f,
    minScale: Float = 0.5f, //todo 0.1f
    maxScale: Float = 2f, //todo 10f
    handleOffset: DpOffset = DpOffset.Zero,
    handleSize: Dp = 8.dp,
    onResize: (offset: Offset, size: Size) -> Unit = { _, _ -> },
    onResizeEnd: () -> Unit = { },
    /////
    onScale: ((scaleOffset: Offset, scale: Float) -> Unit)? = null,
    minSize: Size = Size(48f, 48f),
    content: @Composable () -> Unit = { }
) {
    // Actual size, that is updated as the user resizes this box.
    var currentSize by remember { mutableStateOf(Size.Zero) }
    // Size of the box before user starts resizing it.
    var preDragSize = remember { Size.Zero }
    // The horizontal distance between the origin point of touch event and current dragging position.
    var totalDragOffsetX = remember { 0f }

    // necessary to work in lambda
    var currentScale: Float by remember { mutableStateOf(scale) }

    UpdateViewConfiguration(
        size = 25.dp
    ) {
        BoxWithCornerHandles(
            modifier = modifier.onGloballyPositioned { coordinates ->
                currentSize = coordinates.size.toSize()
            },
            contentAlignment = contentAlignment,
            offset = handleOffset,
            handleSize = handleSize,
            onTopStartDrag = { offset ->
                totalDragOffsetX += offset.x

                // New width value including offset, corrected with minimal size constraints.
                val correctedNewWidth =
                    maxOf(minSize.width, preDragSize.width - totalDragOffsetX)

                // New horizontal offset relative to the previous offset coordinate, corrected with minimal size constraints.
                // Rounding to Int prevents minor positional shifts caused by using Float size and coordinates.
                val correctedOffsetX =
                    (currentSize.width - correctedNewWidth).roundToInt().toFloat()

                onResize(
                    offset.copy(x = correctedOffsetX),
                    preDragSize.copy(width = correctedNewWidth)
                )

                if (onScale != null) {
                    val newScale = (correctedNewWidth - currentSize.width) / currentSize.width
                    currentScale += newScale

                    onScale(Offset(x = -newScale, y = -newScale), currentScale)
                }
            },
            onTopEndDrag = { offset ->
                totalDragOffsetX += offset.x

                val correctedNewWidth =
                    maxOf(minSize.width, preDragSize.width + totalDragOffsetX)

                onResize(
                    Offset.Zero,
                    preDragSize.copy(width = correctedNewWidth)
                )

                if (onScale != null) {
                    val newScale = (correctedNewWidth - currentSize.width) / currentSize.width
                    currentScale += newScale

                    onScale(Offset(x = 0f, y = -newScale), currentScale)
                }
            },
            onBottomStartDrag = { offset ->
                totalDragOffsetX += offset.x

                // New width value including offset, corrected with minimal size constraints.
                val correctedNewWidth =
                    maxOf(minSize.width, preDragSize.width - totalDragOffsetX)

                // New horizontal offset relative to the previous offset coordinate, corrected with minimal size constraints.
                // Rounding to Int prevents minor positional shifts caused by using Float size and coordinates.
                val correctedOffsetX =
                    (currentSize.width - correctedNewWidth).roundToInt().toFloat()

                onResize(
                    offset.copy(x = correctedOffsetX),
                    preDragSize.copy(width = correctedNewWidth)
                )

                if (onScale != null) {
                    val newScale = ((correctedNewWidth - currentSize.width) / currentSize.width).roundTo(8)
                    currentScale += newScale

                    onScale(Offset(x = -newScale, y = 0f), currentScale) //todo test rounding
                }
            },
            onBottomEndDrag = { offset ->
                totalDragOffsetX += offset.x

                val correctedNewWidth =
                    maxOf(minSize.width, preDragSize.width + totalDragOffsetX)

                val newScale = (correctedNewWidth - currentSize.width) / currentSize.width

                val changeText = if (offset.x > 0) "INCREASE" else "DECREASE"
                Log.i("kosilov", "ResizableBox8: $changeText offset=${offset}, correctedNewWidth=$correctedNewWidth, currentSize.width=${currentSize.width}, newScale=$newScale")


                onResize(
                    Offset.Zero,
                    preDragSize.copy(width = correctedNewWidth)
                )

                if (onScale != null) {
                    currentScale += newScale
                    onScale(Offset.Zero, currentScale) //todo test rounding
                }
            },
            onDragStart = {
                preDragSize = currentSize
            },
            onDragEnd = {
                totalDragOffsetX = 0f
                onResizeEnd()
            },
            content = content
        )
    }
}

fun Double.roundTo(decimals: Int): Double {
    var multiplier = 10.0.pow(decimals)
    return round(this * multiplier) / multiplier
}

fun Float.roundTo(decimals: Int): Float {
    var multiplier = 10.0f.pow(decimals)
    return round(this * multiplier) / multiplier
}

