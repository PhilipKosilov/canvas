package com.example.canvas.handles

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.requiredSizeIn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.Placeable
import androidx.compose.ui.layout.SubcomposeLayout
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp

/*
@Composable
fun TransformLayout(
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    handleRadius: Dp = 15.dp,
    handlePlacement: HandlePlacement = HandlePlacement.Corners(),
    onDown: (Transform) -> Unit = {},
    onMove: (Transform) -> Unit = {},
    onUp: (Transform) -> Unit = {},
    content: @Composable () -> Unit
) {

    MorphSubcomposeLayout(
        modifier = modifier
            .requiredSizeIn(
                minWidth = handleRadius * 2,
                minHeight = handleRadius * 2
            )
}
 */



@Composable
internal fun TransformSubcomposeLayout(
    modifier: Modifier = Modifier,
    handleRadius: Dp = 15.dp,
    mainContent: @Composable () -> Unit,
    dependentContent: @Composable (IntSize) -> Unit
) {

    val handleRadiusInPx = with(LocalDensity.current) {
        handleRadius.roundToPx()
    }

    SubcomposeLayout(
        modifier = modifier
    ) { constraints ->

        // Subcompose(compose only a section) com.example.canvas.main content and get Placeable
        val mainPlaceables: List<Placeable> = subcompose(SlotsEnum.Main, mainContent)
            .map {
                it.measure(constraints)
            }

        // Get max width and height of com.example.canvas.main component
        val maxSize =
            mainPlaceables.fold(IntSize.Zero) { currentMax: IntSize, placeable: Placeable ->
                IntSize(
                    width = maxOf(currentMax.width, placeable.width),
                    height = maxOf(currentMax.height, placeable.height)
                )
            }

        // Set  sum of content and handle size as total size of this Composable
        val width = maxSize.width + 2 * handleRadiusInPx
        val height = maxSize.height + 2 * handleRadiusInPx


        val dependentPlaceables = subcompose(SlotsEnum.Dependent) {
            dependentContent(maxSize)
        }.map {
            it.measure(constraints)
        }

        layout(width, height) {
            dependentPlaceables.forEach { placeable: Placeable ->
                // place Placeables inside content area by offsetting by handle radius
                placeable.placeRelative(
                    (width - placeable.width) / 2,
                    (height - placeable.height) / 2
                )
            }
        }
    }
}

enum class SlotsEnum { Main, Dependent }

/*
@Composable
fun TransformLayout(
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    handleRadius: Dp = 15.dp,
    handlePlacement: HandlePlacement = HandlePlacement.Corner,
    onDown: (Transform) -> Unit = {},
    onMove: (Transform) -> Unit = {},
    onUp: (Transform) -> Unit = {},
    content: @Composable () -> Unit
) {
    TransformSubcomposeLayout(
        // 🔥 !!! I'm not able to pass modifier from function because if it contains
        // a Modifier.size() it breaks layout() function
        // and because of that i can't set border, zIndex or any other Modifier from
        // user. ZIndex must be set here to work, since it works against siblings
        modifier = Modifier.border(3.dp, Color.Green),
        handleRadius = handleRadius.coerceAtLeast(12.dp),
        mainContent = {
            // 🔥 Modifier from user is only used for measuring size
            Box(
                modifier = modifier,
                contentAlignment = Alignment.Center
            ) {
                content()
            }
        },
        dependentContent = { intSize: IntSize ->

            val dpSize = with(LocalDensity.current) {
                val rawWidth = intSize.width.toDp()
                val rawHeight = intSize.height.toDp()
                DpSize(rawWidth, rawHeight)
            }

            TransformLayoutImpl(
                enabled = enabled,
                handleRadius = handleRadius,
                dpSize = dpSize,
                handlePlacement = handlePlacement,
                onDown = onDown,
                onMove = onMove,
                onUp = onUp,
                content = content
            )
        }
    )
}
 */