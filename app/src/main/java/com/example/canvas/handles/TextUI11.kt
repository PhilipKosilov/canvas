package com.example.canvas.handles

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import com.example.canvas.composable.BasicBoardItemUI
import com.example.canvas.utils.conditional

@Composable
fun TextUI11(
    startOffsetX: Dp = 50.dp,
    startOffsetY: Dp = 100.dp,
    scale: Float = 1f,
    size: DpSize = DpSize(width = 164.dp, height = 164.dp),
) {
    val coordinate: DpOffset by remember(startOffsetX, startOffsetY) {
        mutableStateOf(
            DpOffset(
                x = startOffsetX,
                y = startOffsetY
            )
        )
    }

    Box(
        // BlockableBoardItem
        modifier = Modifier
            .offset(coordinate.x, coordinate.y),
    ) {
        // Inside BlockableBoardItem
        // todo simulating ResizableBox by passing scale + offset
        Box( // ResizableBox substitute
            contentAlignment = Alignment.Center
        ) {
            // Inside ResizableBox
            BasicScalableBoardItemUI(
                boardItemId = "1",
                size = size,
                scale = scale,
            ) {
                // Inside BasicScalableBoardItemUI
                Column(
                    modifier = Modifier
                        .size(size)
                        .background(Color(0xFFFFD966))
                        .padding(16.dp),
                    verticalArrangement = Arrangement.SpaceBetween
                ) {
                    Box(
                        modifier = Modifier
                            .weight(weight = 0.8f)
                            .fillMaxWidth(),
                    ) {
                        Text(
                            modifier = Modifier.fillMaxWidth(), // While in box
                            text = "TextUI11"
                        )
                    }

                    Text(
                        modifier = Modifier.weight(0.2f),
                        text = "Author",
                    )
                }
            }
        }
    }
}

@Composable
fun BasicScalableBoardItemUI(
    boardItemId: String,
    size: DpSize,
    modifier: Modifier = Modifier,
    isSelected: Boolean = false,
    isEditMode: Boolean = false,
    isBlocked: Boolean = false,
    isLocked: Boolean = false,
    scale: Float = 1f,
    onSelectChange: () -> Unit = { },
    onEnableEditMode: () -> Unit = { },
    onMove: (offset: DpOffset) -> Unit = { },
    onDragEnd: () -> Unit = { },
    backgroundCanvas: DrawScope.() -> Unit = { },
    borderCanvas: DrawScope.() -> Unit = { },
    content: @Composable BoxScope.() -> Unit = { },
) {
    val scaledSize: DpSize by remember(size, scale) { mutableStateOf(size.times(scale)) }

    val scaleModifier = Modifier
        .graphicsLayer(
            scaleX = scale,
            scaleY = scale,
        )

    BasicBoardItemUI(
        modifier = modifier
            // Manually set the size of outer composable to scaled size to not mess up the composition.
            .size(scaledSize)
            .drawWithContent {
                drawContent()

                if (isSelected) {
                    borderCanvas()
                }
            },
        boardItemId = boardItemId,
        isSelected = isSelected,
        isEditMode = isEditMode,
        isBlocked = isBlocked,
        isLocked = isLocked,
        onSelectChange = onSelectChange,
        onEnableEditMode = onEnableEditMode,
        onDragEnd = onDragEnd,
        backgroundCanvas = backgroundCanvas,
    ) {
        Box(
            modifier = Modifier
                .requiredSize(size)
                .conditional(
                    condition = scale != 1f,
                    ifTrue = { scaleModifier }
                ),
            content = content
        )
    }
}