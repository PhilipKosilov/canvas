package com.example.canvas.handles

import android.util.Log
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import com.example.canvas.composable.SubComponent
import com.example.canvas.utils.TAG
import com.example.canvas.utils.conditional

/*
* Calculate it size once and then pass it somehow.
* You keep recalculating size of the content, but it won't change.
 */

@Composable
fun TextUI3(

) {
    val density = LocalDensity.current

    var offsetX by remember { mutableStateOf(100.dp) }
    var offsetY by remember { mutableStateOf(100.dp) }

    var size: Size by remember { mutableStateOf(Size.Zero) }

    Box( // global box (in NewBoardContent)
        modifier = Modifier.fillMaxSize()
    ) {
        // ResizableBox2 ResizableBoxAlternative
        ResizableBoxAlternative(
            modifier = Modifier
                .offset(offsetX, offsetY)
                .border(width = 1.dp, color = Color.Magenta),
            onResize = { offset, newSize ->

                size = newSize

                if (offset != Offset.Zero) {
                    offsetX += with(density) {
                        offset.x.toDp()
                    }
                }
            }
        ) {
            Text(
                modifier = Modifier
                    .conditional(
                        condition = size == Size.Zero,
                        ifTrue = {
                            onGloballyPositioned {
                                size = it.size.toSize()
                            }
                        },
                        ifFalse = {
                            with(density) {
                                size(size.toDpSize())
                            }
                        }
                    )
                    .border(width = 1.dp, color = Color.Blue),
                text = "TextUI2"
            )
        }
    }
}

@Composable
fun ResizableBoxAlternative(
    modifier: Modifier = Modifier,
    handleOffset: DpOffset = DpOffset(x = 8.dp, y = 6.dp),
    onResize: (offset: Offset, size: Size) -> Unit = { _, _ -> },
    minSize: Size = Size(48f, 48f),
    content: @Composable () -> Unit = { }
){
    var componentSize by remember { mutableStateOf(Size.Zero) }
    var overOffsetX = remember { 0f }

    Log.i(TAG, "ResizableBoxAlternative: recompose")

    UpdateViewConfiguration(
        size = 20.dp
    ) {
        BoxWithHexHandles(
            modifier = modifier.onGloballyPositioned { coordinates ->
                componentSize = coordinates.size.toSize()
            },
            offset = handleOffset,
            onMiddleStartDrag = { offset ->
                if (componentSize.width - offset.x < minSize.width) { // moving LR (offset > 0)
                    // correction needed
                    overOffsetX += minSize.width - (componentSize.width - offset.x)

                    if (offset.x - overOffsetX > 0) {
                        onResize(
                            Offset(x = (offset.x - overOffsetX), y = 0f),
                            componentSize.copy(width = componentSize.width - (offset.x - overOffsetX))
                        )
                    }
                } else if (overOffsetX > 0) { // moving LL (offset < 0)
                    overOffsetX += offset.x

                    if (overOffsetX < 0) {
                        onResize(
                            Offset(x = overOffsetX, y = 0f),
                            componentSize.copy(width = minSize.width - overOffsetX)
                        )
                        overOffsetX = 0f
                    }
                } else { // dragging LL (offset < 0)
                    // correction not needed
                    onResize(
                        offset,
                        componentSize.copy(width = componentSize.width - offset.x)
                    )
                }
            },
            onMiddleEndDrag = { offset ->
                if (componentSize.width + offset.x < minSize.width) {
                    // we add difference minSize.width - componentSize.width + offset.x
                    overOffsetX += minSize.width - (componentSize.width + offset.x)

                    onResize(
                        Offset.Zero,
                        componentSize.copy(width = minSize.width)
                    )
                } else if (overOffsetX > 0) {
                    // remove correction offset
                    overOffsetX -= offset.x
                    // if removed more than was in correction, move the element
                    if (overOffsetX < 0) {
                        onResize(
                            Offset.Zero,
                            componentSize.copy(width = minSize.width - overOffsetX)
                        )
                        overOffsetX = 0f
                    }
                } else { // dragging RR
                    onResize(
                        Offset.Zero,
                        componentSize.copy(width = componentSize.width + offset.x)
                    )
                }
            },
            onDragEnd = {
                // when stop drag, reset correction offset
                overOffsetX = 0f
            },
            content = content
        )
    }
}




