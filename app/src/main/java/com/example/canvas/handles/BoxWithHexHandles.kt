package com.example.canvas.handles

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.dp

@Composable
fun BoxWithHexHandles(
    modifier: Modifier = Modifier,
    handleSize: Dp = 10.dp,
    offset: DpOffset = DpOffset.Zero,
    onTopStartDrag: (Offset) -> Unit = { },
    onTopEndDrag: (Offset) -> Unit = { },
    onBottomStartDrag: (Offset) -> Unit = { },
    onBottomEndDrag: (Offset) -> Unit = { },
    onMiddleStartDrag: (Offset) -> Unit = { },
    onMiddleEndDrag: (Offset) -> Unit = { },
    onDragStart: (Offset) -> Unit = { },
    onDragEnd: () -> Unit = { },
    content: @Composable () -> Unit = { },
) {
    val offsetX = handleSize + offset.x // + 8.dp
    val offsetY = handleSize + offset.y // + 6.dp

    val middleStart = HandleData(
        offset = DpOffset(x = -offsetX, y = 0.dp),
        alignment = Alignment.CenterStart,
        onDrag = onMiddleStartDrag
    )
    val middleEnd = HandleData(
        offset = DpOffset(x = offsetX, y = 0.dp),
        alignment = Alignment.CenterEnd,
        onDrag = onMiddleEndDrag
    )

    BoxWithCornerHandles(
        modifier = modifier,
        handleSize = handleSize,
        offset = offset,
        onTopStartDrag = onTopStartDrag,
        onTopEndDrag = onTopEndDrag,
        onBottomStartDrag = onBottomStartDrag,
        onBottomEndDrag = onBottomEndDrag,
    ) {
        Box {
            content()

            listOf(
                middleStart, middleEnd,
            ).forEach { data ->
                Handle(
                    size = handleSize,
                    shape = CircleShape,
                    alignment = data.alignment,
                    offset = data.offset,
                    onDrag = data.onDrag,
                    onDragStart = onDragStart,
                    onDragEnd = onDragEnd,
                )
            }
        }
    }
}