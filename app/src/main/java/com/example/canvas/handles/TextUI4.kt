package com.example.canvas.handles

import android.util.Log
import androidx.compose.foundation.border
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.input.pointer.PointerInputChange
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import com.example.canvas.utils.TAG
import com.example.canvas.utils.conditional
import kotlin.math.roundToInt

@Composable
fun TextUI4(
    startOffsetX: Dp = 100.dp,
    startOffsetY: Dp = 100.dp,
) {
    val density = LocalDensity.current

    var offsetX by remember { mutableStateOf(startOffsetX) }
    var offsetY by remember { mutableStateOf(startOffsetY) }

    var size: Size by remember { mutableStateOf(Size.Zero) }

    Box( // global box (in NewBoardContent)
        modifier = Modifier.fillMaxSize()
    ) {
        ResizableBoxTotalOffset(
            modifier = Modifier
                .offset(offsetX, offsetY)
                .border(width = 1.dp, color = Color.Magenta),
            onResize = { offset, newSize ->

                size = newSize

                if (offset != Offset.Zero) {
                    offsetX += with(density) {
                        offset.x.toDp()
                    }
                }
            }
        ) {
            Text(
                modifier = Modifier
                    .conditional(
                        condition = size == Size.Zero,
                        ifTrue = {
                            onGloballyPositioned {
                                size = it.size.toSize()
                            }
                        },
                        ifFalse = {
                            with(density) {
                                size(size.toDpSize())
                            }
                        }
                    )
                    .border(width = 1.dp, color = Color.Blue),
                text = "TextUI2"
            )
        }
    }
}

@Composable
fun ResizableBoxTotalOffset(
    modifier: Modifier = Modifier,
    handleOffset: DpOffset = DpOffset(x = 8.dp, y = 6.dp),
    onResize: (offset: Offset, size: Size) -> Unit = { _, _ -> },
    minSize: Size = Size(48f, 48f),
    content: @Composable () -> Unit = { }
) {
    // 1) Using non mutable state to not update content size in callback while dragging
    var contentSize by remember { mutableStateOf(Size.Zero) }
    var preDragContentSize by remember { mutableStateOf(Size.Zero) }
    var totalOffsetX = remember { 0f }
    Log.i(TAG, "ResizableBoxTotalOffset: recompose")

    Box(
        modifier = modifier,
    ) {
        Box(
            modifier = Modifier.onGloballyPositioned { coordinates ->
//                Log.i(TAG, "ResizableBoxTotalOffset: contentSize=${coordinates.size}")
                contentSize = coordinates.size.toSize()
                if (preDragContentSize == Size.Zero) { // initialize first
                    preDragContentSize = contentSize
                }
            },
        ) {
            content()
        }

        UpdateViewConfiguration(
            size = 20.dp
        ) {
            BoxWithHexHandles(
                offset = handleOffset,
                onMiddleStartDrag = { offset ->
                    totalOffsetX += offset.x

                    val actualWidth =
                        maxOf(minSize.width, preDragContentSize.width - totalOffsetX)
                    val availableOffsetX =  -(actualWidth - contentSize.width).roundToInt().toFloat()

                    // todo calculate what offset can use
                    onResize(
                        offset.copy(x = availableOffsetX),
//                        offset,
                        preDragContentSize.copy(width = actualWidth)
                    )
                },
                onMiddleEndDrag = { offset ->
                    // first calculate what amount of offset you can use
                    totalOffsetX += offset.x
                    val actualWidth =
                        maxOf(minSize.width, preDragContentSize.width + totalOffsetX)

                    onResize(
                        Offset.Zero,
                        preDragContentSize.copy(width = actualWidth)
                    )
                },
                onDragStart = {
                    preDragContentSize = contentSize // update size, when start dragggin
                },
                onDragEnd = {
                    totalOffsetX = 0f
                }
            )
        }
    }
}