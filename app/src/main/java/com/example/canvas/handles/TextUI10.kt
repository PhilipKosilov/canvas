package com.example.canvas.handles

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toOffset
import androidx.compose.ui.unit.toSize
import com.example.canvas.composable.BasicBoardItemUI
import com.example.canvas.utils.conditional
import kotlin.math.roundToInt

@Composable
fun TextUI10(
    startOffsetX: Dp = 50.dp,
    startOffsetY: Dp = 100.dp,
) {
    val density = LocalDensity.current
    // offset will be saved in Firebase
    var offsetX by remember { mutableStateOf(startOffsetX) }
    var offsetY by remember { mutableStateOf(startOffsetY) }

    var scale: Float by remember { mutableStateOf(1f) }
    var size: DpSize by remember { // todo data.size
        mutableStateOf(
            DpSize(
                width = 164.dp,
                height = 164.dp
            )
        )
    }

    /////////////////////////////////////////////////////////////////
    BlockableBoardItem(
        modifier = Modifier
            .offset(offsetX, offsetY),
//            .background(Color.LightGray)
        contentPadding = PaddingValues(top = 8.dp, start = 8.dp, end = 8.dp, bottom = 16.dp),
        blockedCanvas = {
//            drawBlockedCanvas()
        },
    ) {
        InteractiveBoardItemUI(
            modifier = Modifier
                .shadow(
                    elevation = 16.dp,
                    clip = false // Removes default border clipping, when elevation > 0
                ),
            boardItemId = "1",
            scale = scale,
            onScale = { newOffset, newScale ->
                scale = newScale
                Log.i("kosilov", "---3: newScale=$newScale, scale=$scale")

                with(density) {
                    offsetX += newOffset.x.toDp()
                    offsetY += newOffset.y.toDp()
                }
            },
            backgroundCanvas = {
                drawRect(
                    color = Color.Yellow,
                    size = this.size
                )
            },
        ) {
            Column(
                modifier = Modifier
                    .size(size)
                    .padding(16.dp),
                verticalArrangement = Arrangement.SpaceBetween
            ) {
                Box(
                    modifier = Modifier
                        .weight(weight = 0.8f)
                        .background(Color(0xFFBABABA))
                        .fillMaxWidth(),
                ) {
                    Text(
                        modifier = Modifier.fillMaxWidth(), // While in box
                        text = "TextUI10"
                    )
                }

                Text(
                    modifier = Modifier.weight(0.2f),
                    text = "Author",
                )
            }
        }
    }
}

@Composable
fun InteractiveBoardItemUI(
    boardItemId: String,
    modifier: Modifier = Modifier,
    isSelected: Boolean = false,
    isEditMode: Boolean = false,
    isBlocked: Boolean = false,
    isLocked: Boolean = false,
    scale: Float = 1f, //todo add min/max
    onSelectChange: () -> Unit = { },
    onEnableEditMode: () -> Unit = { },
    onMove: (Offset) -> Unit = { },
    onDragEnd: () -> Unit = { },
    onScale: ((offset: Offset, scale: Float) -> Unit)? = null, //todo refactor
    backgroundCanvas: DrawScope.() -> Unit = { },
    borderCanvas: DrawScope.() -> Unit = { },
    content: @Composable BoxScope.() -> Unit = { },
) {
    val density = LocalDensity.current
    val scaleModifier = Modifier
        .graphicsLayer(
            scaleX = scale,
            scaleY = scale,
//            transformOrigin = TransformOrigin(0f, 0f)
        )

    var contentSize by remember { //todo can I calculate?
        mutableStateOf(DpSize.Zero)
    }
    val upscaleSize: DpSize by remember(scale) {
        derivedStateOf {
            contentSize.times(scale) // todo don't use innerScale to force update scale parameter.
        }
    }

    ResizableBox10(
        modifier = Modifier // todo test
            .then( //todo conditional
                if (upscaleSize == DpSize.Zero) {
                    Modifier
                } else {
                    Modifier.size(upscaleSize)
                }
            )
            .drawBehind {
                if (isSelected) {
                    borderCanvas()
                }
            }
            .background(Color.LightGray.copy(alpha = 0.2f)),
        scale = scale, // todo this needed?
        handleOffset = DpOffset(x = (-4).dp, y = (-4).dp), //todo refactor to not need offset
        contentAlignment = Alignment.Center,
        minSize = with(density) {
            DpSize(
                48.dp,
                48.dp
            ).toSize()
        }, // todo move this to InteractiveItem!!!!
        onScale = { scaleOffset, newScale ->
//                Log.i("kosilov", "InteractiveBoardItemUI: onscale innerScale=$innerScale")
//                innerScale = newScale
//                Log.i("kosilov", "InteractiveBoardItemUI: innerOffset=$innerOffset")

            with(density) {
                val offsetDelta = IntOffset(
                    x = contentSize.width.toPx().times(scaleOffset.x).roundToInt(),
                    y = contentSize.height.toPx().times(scaleOffset.y).roundToInt()
                )


                if (onScale != null) {
                    onScale(offsetDelta.toOffset(), newScale)
                }
            }
        },
    ) {
        BasicBoardItemUI(
            // todo size should be reduced to then scale
            // todo this should be scaled -> content and background
            modifier = Modifier
//                .then(modifier) // FIRST WORKS
//                .offset { innerOffset } // todo offset BEFORE scale
                // todo testing
                .then( // todo check
                    if (contentSize == DpSize.Zero) {
                        Modifier
                    } else {
                        Modifier.requiredSize(contentSize)
                    }
                )
                .conditional(
                    condition = scale != 1f,
                    ifTrue = {
                        scaleModifier
                    }
                )
                .then(modifier) // FIRST WORKS
                .onGloballyPositioned {
                    with(density) {
                        if (contentSize == DpSize.Zero) {
                            contentSize = it.size
                                .toSize()
                                .toDpSize()
                        }
                    }
                },
            boardItemId = boardItemId,
            isSelected = isSelected,
            isEditMode = isEditMode,
            isBlocked = isBlocked,
            isLocked = isLocked,
            onSelectChange = onSelectChange,
            onEnableEditMode = onEnableEditMode,
            onMove = onMove,
            onDragEnd = onDragEnd,
            backgroundCanvas = backgroundCanvas,
            content = content,
        )
    }
}


@Composable
fun ResizableBox10(
    modifier: Modifier = Modifier,
    contentAlignment: Alignment = Alignment.TopStart,
    scale: Float = 1f,
    handleOffset: DpOffset = DpOffset.Zero,
    handleSize: Dp = 8.dp,
    onResize: (offset: Offset, size: Size) -> Unit = { _, _ -> },
    onResizeEnd: () -> Unit = { },
    /////
    onScale: ((scaleOffset: Offset, scale: Float) -> Unit)? = null,
    minSize: Size = Size(48f, 48f),
    content: @Composable () -> Unit = { }
) {
    // Actual size, that is updated as the user resizes this box.
    var currentSize by remember { mutableStateOf(Size.Zero) }
    // Size of the box before user starts resizing it.
    var preDragSize = remember { Size.Zero }
    // The horizontal distance between the origin point of touch event and current dragging position.
    var totalDragOffsetX = remember { 0f }
    var totalDragOffsetY = remember { 0f }

    // necessary to work in lambda
    var currentScale: Float by remember { mutableStateOf(scale) }

    UpdateViewConfiguration(
        size = 25.dp
    ) {
        BoxWithCornerHandles(
            modifier = modifier.onGloballyPositioned { coordinates ->
                currentSize = coordinates.size.toSize()
                Log.i("kosilov", "-1: currentSize=$currentSize")
            },
            contentAlignment = contentAlignment,
            offset = handleOffset,
            handleSize = handleSize,
            onTopStartDrag = { offset ->
                totalDragOffsetX += offset.x
                totalDragOffsetY += offset.y

                val correctedNewWidth =
                    maxOf(minSize.width, preDragSize.width - totalDragOffsetX)
                val correctedNewHeight =
                    maxOf(minSize.height, preDragSize.height - totalDragOffsetY)

                val correctedOffsetX =
                    (currentSize.width - correctedNewWidth).roundToInt().toFloat()

                onResize(
                    offset.copy(x = correctedOffsetX),
                    preDragSize.copy(width = correctedNewWidth)
                )

                if (onScale != null) {
                    val newScale =
                        calculateNewScale(correctedNewWidth, correctedNewHeight, currentSize, preDragSize)
                    currentScale += newScale

                    onScale(Offset(x = -newScale, y = -newScale), currentScale)
                }
            },
            onTopEndDrag = { offset ->
                totalDragOffsetX += offset.x
                totalDragOffsetY += offset.y

                val correctedNewWidth =
                    maxOf(minSize.width, preDragSize.width + totalDragOffsetX)
                val correctedNewHeight =
                    maxOf(minSize.height, preDragSize.height - totalDragOffsetY)

                if (onScale != null) {
                    val newScale =
                        calculateNewScale(correctedNewWidth, correctedNewHeight, currentSize, preDragSize)
                    currentScale += newScale

                    onScale(Offset(x = 0f, y = -newScale), currentScale) //todo test rounding
                }
            },
            onBottomStartDrag = { offset ->
                totalDragOffsetX += offset.x
                totalDragOffsetY += offset.y

                val correctedNewWidth =
                    maxOf(minSize.width, preDragSize.width - totalDragOffsetX)
                val correctedNewHeight =
                    maxOf(minSize.height, preDragSize.height + totalDragOffsetY)

                if (onScale != null) {
                    val newScale =
                        calculateNewScale(correctedNewWidth, correctedNewHeight, currentSize, preDragSize)
                    currentScale += newScale

                    onScale(Offset(x = -newScale, y = 0f), currentScale) //todo test rounding
                }
            },
            onBottomEndDrag = { offset ->
                totalDragOffsetX += offset.x
                totalDragOffsetY += offset.y

                val correctedNewWidth =
                    maxOf(minSize.width, preDragSize.width + totalDragOffsetX)
                val correctedNewHeight =
                    maxOf(minSize.height, preDragSize.height + totalDragOffsetY)

                if (onScale != null) {
                    val newScale =
                        calculateNewScale(correctedNewWidth, correctedNewHeight, currentSize, preDragSize)
                    currentScale += newScale

                    onScale(Offset.Zero, currentScale) //todo test rounding
                }
            },
            onDragStart = {
                preDragSize = currentSize
            },
            onDragEnd = {
                totalDragOffsetX = 0f
                totalDragOffsetY = 0f
                onResizeEnd()
            },
            content = content
        )
    }
}

// todo refactor
private fun calculateNewScale(
    correctedNewWidth: Float,
    correctedNewHeight: Float,
    currentSize: Size,
    preDragSize: Size
): Float {
    val scaleX = (correctedNewWidth - currentSize.width) / preDragSize.width
    val scaleY = (correctedNewHeight - currentSize.height) / preDragSize.height
    return (scaleX + scaleY) / 2
}
