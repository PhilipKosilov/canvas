package com.example.canvas.handles

import androidx.compose.ui.geometry.Offset

// First implementation is
sealed interface HandlePlacement {
    object None: HandlePlacement
    class Corners(offset: Offset = Offset.Zero): HandlePlacement
    class CornersWithMiddle(offset: Offset = Offset.Zero): HandlePlacement
}
