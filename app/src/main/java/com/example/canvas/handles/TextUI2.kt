package com.example.canvas.handles

import android.util.Log
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import com.example.canvas.utils.TAG
import com.example.canvas.utils.conditional
import kotlin.math.roundToInt

/**
 * Encapsulate resizing logic in ResizableBox
 */

@Composable
fun TextUI2(

) {
    val density = LocalDensity.current

    var offsetX by remember { mutableStateOf(100.dp) }
    var offsetY by remember { mutableStateOf(100.dp) }

    var size: Size by remember { mutableStateOf(Size.Zero) }

    Box(
        modifier = Modifier
            .offset(offsetX, offsetY)
            .wrapContentSize(align = Alignment.TopStart)
    ) {
        ResizableBox(
            onResize = { offset, newSize ->
                size = newSize

                if (offset != Offset.Zero) {
                    offsetX -= with(density) {
                        offset.x.toDp()
                    }
                    with(density) {
                        Log.i(
                            TAG,
                            "TextUI2: offset.x=${offset.x}, offset.x.toDp()=${offset.x.toDp()}, offsetX=${offsetX.toPx()}, new width=${newSize.width}"
                        )
                        Log.i(TAG, "----------------------------")
                    }
                }
            },
        ) {
            Text(
                modifier = Modifier
                    .conditional(
                        condition = size == Size.Zero,
                        ifTrue = {
                            onGloballyPositioned {
                                with(density) {
                                    Log.i(
                                        TAG,
                                        "TextUI2: Start size=${it.size.width}, start offset=${offsetX.toPx()}"
                                    )
                                }
                                size = it.size.toSize()
                            }
                        },
                        ifFalse = {
                             with(density) {
                                 size(size.toDpSize())
                            }
                        }
                    )
                    .clickable {

                    }
                    .border(width = 1.dp, color = Color.Blue),
                text = "TextUI2"
            )
        }
    }
}

// When move RR convert drag offset to IntOffset before setting
@Composable
fun ResizableBox(
    modifier: Modifier = Modifier,
    handleOffset: DpOffset = DpOffset(x = 8.dp, y = 6.dp),
    onResize: (offset: Offset, size: Size) -> Unit = { _, _ -> },
    minSize: Size = Size(48f, 48f),
    content: @Composable () -> Unit = { },
) {
    var overOffsetX = remember { 0f }
    // will store the difference left after rounding Float to Int
    // Use this to not lose accuracy when dragging RR
    var dragOffsetRoundRemainder = remember { 0f }
    var overOffsetRoundRemainder = remember { 0f }

    var componentSize: Size = remember { minSize }

    Box(
        modifier = modifier,
        contentAlignment = Alignment.Center,
    ) {
        Box(
            modifier = Modifier.onGloballyPositioned { coordinates ->
                componentSize = coordinates.size.toSize()
            }
        ) {
            content()
        }

        UpdateViewConfiguration(
            size = 20.dp
        ) {
            BoxWithHexHandles(
                offset = handleOffset,
                onMiddleStartDrag = { offset ->
                    val intOffsetX = (offset.x + dragOffsetRoundRemainder).roundToInt()
                    dragOffsetRoundRemainder += offset.x - intOffsetX

                    if (componentSize.width - offset.x < minSize.width) { // moving LR (offset > 0)
                        // correction needed
                        overOffsetX += minSize.width - (componentSize.width - offset.x)

                        //////////
                        val intOverOffsetX = (overOffsetX).roundToInt()
                        overOffsetRoundRemainder += overOffsetX - intOverOffsetX

                        if (offset.x - overOffsetX > 0) {
                            onResize(
                                Offset(x = -(intOffsetX - intOverOffsetX).toFloat(), y = 0f),
                                componentSize.copy(width = (componentSize.width - (intOffsetX - intOverOffsetX)).roundToInt().toFloat())
                            )
                        }
                    } else if (overOffsetX > 0) { // moving LL (offset < 0)
                        overOffsetX += offset.x

                        val intOverOffsetX = (overOffsetX).roundToInt()
                        overOffsetRoundRemainder += overOffsetX - intOverOffsetX


                        if (overOffsetX <= 0) {
                            onResize(
                                Offset(x = -intOverOffsetX.toFloat(), y = 0f),
                                componentSize.copy(width = (minSize.width - intOverOffsetX).roundToInt().toFloat())
                            )
                            overOffsetX = 0f
                        }
                    } else { // moving LL (offset < 0)
                        // correction not needed
                        val intOverOffsetX = (overOffsetX).roundToInt()
                        overOffsetRoundRemainder += overOffsetX - intOverOffsetX

                        onResize(
                            Offset(x = -offset.x.roundToInt().toFloat(), y = -offset.y),
//                            -offset,
                            componentSize.copy(width = componentSize.width - intOffsetX)
                        )
                    }
                },
                onMiddleEndDrag = { offset ->
                    if (componentSize.width + offset.x < minSize.width) {
                        // we add difference minSize.width - componentSize.width + offset.x
                        overOffsetX += minSize.width - (componentSize.width + offset.x)

                        onResize(
                            Offset.Zero,
                            componentSize.copy(width = minSize.width)
                        )
                    } else if (overOffsetX > 0) {
                        // remove correction offset
                        overOffsetX -= offset.x
                        // if removed more than was in correction, move the element
                        if (overOffsetX < 0) {
                            onResize(
                                Offset.Zero,
                                componentSize.copy(width = minSize.width - overOffsetX)
                            )
                            overOffsetX = 0f
                        }
                    } else {
                        onResize(
                            Offset.Zero,
                            componentSize.copy(width = componentSize.width + offset.x)
                        )
                    }
                },
                onDragEnd = {
                    // when stop drag, reset correction offset
                    overOffsetX = 0f
                    dragOffsetRoundRemainder = 0f
                    overOffsetRoundRemainder = 0f
                }
            )
        }
    }
}