package com.example.canvas.handles

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import com.example.canvas.utils.conditional
import kotlin.math.roundToInt

// Testing scaling
@Composable
fun TextUI5(
    startOffsetX: Dp = 100.dp,
    startOffsetY: Dp = 100.dp,
) {
    val density = LocalDensity.current
    var offsetX by remember { mutableStateOf(startOffsetX) }
    var offsetY by remember { mutableStateOf(startOffsetY) }

    var scale: Float by remember { mutableStateOf(1f) }
    var size: DpSize by remember { mutableStateOf(DpSize.Zero) }
    val scaleSize: DpSize by remember(size, scale) { derivedStateOf {
        size.times(scale)
    } }

    Box( // Global box (in NewBoardContent)
        modifier = Modifier.fillMaxSize()
    ) {
        ResizableBox5( // Box with handles and border. Don't scale it. Set its size.
            modifier = Modifier
                .offset(offsetX, offsetY)
                .conditional( // Initializes size the first time. Whiteboard doesn't need this.
                    condition = size == DpSize.Zero,
                    ifTrue = {
                        onGloballyPositioned {
                            with(density) {
                                size = it.size
                                    .toSize()
                                    .toDpSize()
                            }
                        }
                    },
                    ifFalse = {
                        size(scaleSize)
                    }
                )
//                .padding(16.dp) // todo this also scales (causes unevenness)
                .background(Color.LightGray)
                .border(width = 1.dp, color = Color.Blue),
            onScale = { offset, newScale ->
                scale += newScale

                if (offset != Offset.Zero) {
                    with(density) {
                        offsetX += offset.x.toDp()
                        offsetY += offset.y.toDp()
                    }
                }
            },
            contentAlignment = Alignment.Center,
        ) {
            Box( // Content box. Scale its content
                modifier = Modifier
                    .graphicsLayer(
                        scaleX = scale,
                        scaleY = scale,
//                        transformOrigin = TransformOrigin(0f, 0f)
                    ),
                contentAlignment = Alignment.Center,
            ) {
                // Content to scale
                Text(
                    modifier = Modifier
                        .background(Color(0xFFBABABA)),
                    text = "TextUI5"
                )
            }
        }
    }
}


@Composable
fun ResizableBox5(
    modifier: Modifier = Modifier,
    contentAlignment: Alignment = Alignment.TopStart,
    handleOffset: DpOffset = DpOffset.Zero,
    handleSize: Dp = 8.dp,
    onResize: (offset: Offset, size: Size) -> Unit = { _, _ -> },
    onResizeEnd: () -> Unit = { },
    /////
    onScale: (offset: Offset, scale: Float) -> Unit = { _, _ -> },
    minSize: Size = Size(48f, 48f),
    content: @Composable () -> Unit = { }
) {
    // Actual size, that is updated as the user resizes this box.
    var currentSize by remember { mutableStateOf(Size.Zero) }
    // Size of the box before user starts resizing it.
    var preDragSize = remember { Size.Zero }
    // The horizontal distance between the origin point of touch event and current dragging position.
    var totalDragOffsetX = remember { 0f }

    UpdateViewConfiguration(
        size = 25.dp
    ) {
        BoxWithCornerHandles(
            modifier = modifier.onGloballyPositioned { coordinates ->
                currentSize = coordinates.size.toSize()
            },
            contentAlignment = contentAlignment,
            offset = handleOffset,
            handleSize = handleSize,
            onTopStartDrag = { offset ->
                totalDragOffsetX += offset.x

                // New width value including offset, corrected with minimal size constraints.
                val correctedNewWidth =
                    maxOf(minSize.width, preDragSize.width - totalDragOffsetX)

                // New horizontal offset relative to the previous offset coordinate, corrected with minimal size constraints.
                // Rounding to Int prevents minor positional shifts caused by using Float size and coordinates.
                val correctedOffsetX =
                    (currentSize.width - correctedNewWidth).roundToInt().toFloat()

                onResize(
                    offset.copy(x = correctedOffsetX),
                    preDragSize.copy(width = correctedNewWidth)
                )
            },
            onBottomStartDrag = { offset ->
                totalDragOffsetX += offset.x

                // New width value including offset, corrected with minimal size constraints.
                val correctedNewWidth =
                    maxOf(minSize.width, preDragSize.width - totalDragOffsetX)

                // New horizontal offset relative to the previous offset coordinate, corrected with minimal size constraints.
                // Rounding to Int prevents minor positional shifts caused by using Float size and coordinates.
                val correctedOffsetX =
                    (currentSize.width - correctedNewWidth).roundToInt().toFloat()

                onResize(
                    offset.copy(x = correctedOffsetX),
                    preDragSize.copy(width = correctedNewWidth)
                )

                val newScale = (correctedNewWidth - currentSize.width) / currentSize.width
                val scaleOffsetX = (correctedOffsetX * newScale) - correctedOffsetX
                Log.i("kosilov", "ResizableBox5: ")
                onScale(
//                    Offset(x = -(scaleOffsetX / 4), y = 0f),
                    Offset(x = -scaleOffsetX, y = 0f),
//                    offset.copy(x = correctedOffsetX),
                    newScale
                )
            },
            onBottomEndDrag = { offset ->
                totalDragOffsetX += offset.x

                val correctedNewWidth =
                    maxOf(minSize.width, preDragSize.width + totalDragOffsetX)

                onResize(
                    Offset.Zero,
                    preDragSize.copy(width = correctedNewWidth)
                )

                onScale(
                    Offset.Zero,
                    (correctedNewWidth - currentSize.width) / currentSize.width
                )
            },
            onDragStart = {
                preDragSize = currentSize
            },
            onDragEnd = {
                totalDragOffsetX = 0f
                onResizeEnd()
            },
            content = content
        )
    }
}

