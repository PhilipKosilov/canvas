package com.example.canvas.handles

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.platform.LocalViewConfiguration
import androidx.compose.ui.platform.ViewConfiguration
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp

// Use this to modify minimal size of clickable element
@Composable
fun UpdateViewConfiguration(
    size: Dp = 48.dp,
    content: @Composable () -> Unit
) {
    fun ViewConfiguration.updateViewConfiguration() = object : ViewConfiguration {
        override val doubleTapMinTimeMillis: Long
            get() = this@updateViewConfiguration.doubleTapMinTimeMillis

        override val doubleTapTimeoutMillis: Long
            get() = this@updateViewConfiguration.doubleTapTimeoutMillis

        override val longPressTimeoutMillis: Long
            get() = this@updateViewConfiguration.longPressTimeoutMillis

        override val touchSlop: Float
            get() = this@updateViewConfiguration.touchSlop

        override val minimumTouchTargetSize: DpSize
            get() = DpSize(size, size)
    }

    CompositionLocalProvider(
        LocalViewConfiguration provides LocalViewConfiguration.current.updateViewConfiguration()
    ) {
        content()
    }
}