package com.example.canvas.handles

import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.platform.LocalViewConfiguration
import androidx.compose.ui.platform.ViewConfiguration
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import com.example.canvas.composable.drawCornerCircle
import com.example.canvas.utils.conditional
import kotlin.math.roundToInt

/**
 * OnDrag return offset from center, not relative offset from press point.
 *
 *
 * Version1:
 * BoxWithConstraints(
 *  dragTopLeft: (Offset) -> Unit
 *  dragTopRight: (Offset) -> Unit
 *  ...
 *  dragBottomRight: (Offset) -> Unit
 * )
 *
 *
 * Version2:
 * BoxWithConstraints(
 *  onDrag: (Offset, HandlePosition) -> Unit
 * )
 *
 *
 * Version3:
 * Pass some kind of HandleBehaviour that will contain logic?
 * + Makes this logic reusable
 * QuadHandles/HexHandles
 * QuadHandlesBehavior? QuadHandlesBehavior.Scale?
 *
 *
 * Version4:
 * Pass Handles. CornerHandles will have 4 callbacks, CornerWidthMiddleHandles will have 6 callbacks
 *
 *
 * Version5:
 * Similar to touchEvent it will implement onResize, onRotate ets.
 * onResize(offsetX, dragAmount) for example
 */


/**
 * Stop drag, when size is 0!!!
 */
@Composable
fun TextUI(
    handlePlacement: HandlePlacement = HandlePlacement.CornersWithMiddle()
) {
    val view = LocalView.current

    // size of the inner text
    var size by remember { mutableStateOf(IntSize.Zero) }
    val density = LocalDensity.current

    var offsetX by remember { mutableStateOf(100.dp) }
    var offsetY by remember { mutableStateOf(100.dp) }

    Box(
        modifier = Modifier
            .offset(offsetX, offsetY)
            .wrapContentSize(align = Alignment.TopStart)
//            .border(width = 1.dp, color = Color.Red)
    ) {
        BoxWithSizeHexHandles(
            onResize = { offset, dragAmount ->
                size = IntSize(
                    width = size.width + dragAmount.x.roundToInt(),
                    height = size.height
                )
                if (offset != Offset.Zero) {
                    offsetX -= with(density) {
                        offset.x.toDp()
                    }
                }
//                Log.i(TAG, "BoxWithHandles: offset = $offset size = $size")
            }
        ) {
            Text(
                modifier = Modifier
                    .conditional(
                        condition = size == IntSize.Zero,
                        ifTrue = {
                            onGloballyPositioned {
                                size = it.size
                            }
                        },
                        ifFalse = {
                            size(size.pxToDpSize(density))
                        }
                    )
                    .clickable {

                    }
                    .border(width = 1.dp, color = Color.Blue),
                text = "TextUI"
            )
        }
    }
}


// Send just offset and new size, since inner logic is handling resize
@Composable
fun BoxWithSizeHexHandles(
    handleOffset: DpOffset = DpOffset(x = 8.dp, y = 6.dp),
    onResize: (offset: Offset, dragAmount: Offset) -> Unit = { _, _ -> },
    minSize: Size = Size(48f, 48f),
    content: @Composable () -> Unit = { }
) {
    val density = LocalDensity.current
    var totalOffset by remember {
        mutableStateOf(Offset.Zero)
    }

    var componentSize by remember {
        with(density) {
            mutableStateOf(minSize.toDpSize())
        }
    }

    Box(
        contentAlignment = Alignment.Center,
    ) {
        Box(
            modifier = Modifier.onGloballyPositioned { coordinates ->
                componentSize = coordinates.size.pxToDpSize(density)
            }
        ) {
            content()
        }

        UpdateViewConfiguration(
            size = 20.dp
        ) {
            // Get what handle is dragged
            BoxWithHexHandles(
                offset = handleOffset,
                onMiddleStartDrag = { offset ->
                    // for now only width matter
                    totalOffset -= offset
                    with(density) {
                        if (componentSize.toSize().width - offset.x > minSize.width) {
                            onResize(-offset, -offset)
                        }
                    }
                },
                onMiddleEndDrag = { offset ->
                    totalOffset += offset
                    with(density) {
                        if (componentSize.toSize().width + totalOffset.x > minSize.width) {
                            onResize(Offset.Zero, offset)
                        }
                        val min = maxOf(minSize.width, componentSize.toSize().width + totalOffset.x)
//                        Log.i("hanax", "BoxWithSizeHexHandles: minSize=${min}")

                    }
                },
                onDragEnd = {
                    // when stop drag, reset total offset
                    totalOffset = Offset.Zero
                }
            )
        }
    }
}


fun IntSize.pxToDpSize(localDensity: Density): DpSize = with(localDensity) {
    DpSize(
        width = this@pxToDpSize.width.toDp(),
        height = this@pxToDpSize.height.toDp()
    )
}