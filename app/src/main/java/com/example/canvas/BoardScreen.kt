package com.example.canvas

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.canvas.composable.CircleShape
import com.example.canvas.composable.SquareShape
import com.example.canvas.composable.StickyNote
import com.example.canvas.data.BoardItemModel
import com.example.canvas.data.BoardItemType
import com.example.canvas.data.ShapeType
import com.example.canvas.data.toStickyData


@Composable
fun BoardScreen(
    viewModel: BoardViewModel = hiltViewModel()
) {
    Box(
        modifier = Modifier.fillMaxSize()
    ) {
        viewModel.elements.forEach { item ->
            BoardItem(
                model = item,
                onSelect = viewModel::onSelect, // todo: temp
                onEditMode = viewModel::onEditMode,
                onMove = viewModel::onMove
            )
        }
    }
}

@Composable
fun BoardItem(
    model: BoardItemModel,
    onSelect: (Int) -> Unit = { },
    onEditMode: (Int) -> Unit = { },
    onMove: (Int, Offset) -> Unit = { _, _ -> },
) {
    when (model.type) {
        BoardItemType.STICKY -> {
            StickyNote(
                data = model,
                onSelect = onSelect,
                onEditMode = onEditMode,
                onMove = onMove
            )
        }

        BoardItemType.SHAPE -> {
            when (model.shapeType) {
                ShapeType.SQUARE -> {
                    SquareShape(
                        data = model,
                        onSelect = onSelect,
                        onEditMode = onEditMode,
                        onMove = onMove
                    )
                }

                ShapeType.CIRCLE -> {
                    CircleShape(
                        data = model,
                        onSelect = onSelect,
                        onEditMode = onEditMode,
                        onMove = onMove
                    )
                }

                else -> { } // nothing for now
            }
        }

        BoardItemType.IMAGE -> { }

        BoardItemType.TEXT -> { }

        else -> { } // type is nullable for now
    }
}