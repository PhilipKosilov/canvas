package com.example.canvas

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.geometry.Offset
import com.example.canvas.data.BoardItemModel
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Should be DI singleton
 * Holds data on all items in the board: their positions, are they selected etc.
 *
 * Adds items to the Board. Has nothing to do with DB
 *
 * Deselect function, when click on Board?
 *
 * Use its own ids: When adding element, set it's UI-id that is just number of current elements (1, 2, 3)
 * 0 id will be the board itself. Use it to deselect elements, move board ets?
 */
@Singleton
class BoardManager @Inject constructor() {
    private val _items: SnapshotStateList<BoardItemModel> = mutableStateListOf()
    val items: List<BoardItemModel> = _items

    fun addItem(item: BoardItemModel) { // todo BoardItem, BoardUIData??
        _items.add(item)
    }

    fun selectItem(id: Int) {
        // Deselect other element, if selected
        val previouslySelected = _items.firstOrNull { it.isSelected && id != it.id }
        if (previouslySelected != null) {
            val index = _items.indexOf(previouslySelected)
            _items[index] = previouslySelected.copy(isSelected = false)
        }

        // Change selection state of clicked element
        val currentIndex = _items.indexOfFirst { id == it.id }
        _items[currentIndex] =
            _items[currentIndex].copy(isSelected = !_items[currentIndex].isSelected)
    }

    fun changeText(newText: String, ) {

    }


    fun changeEditMode(id: Int) {
        // for now, check if other is in edit mode
        // because edit mode is not linked with isSelected state
        val previouslyEdited = _items.firstOrNull { it.isSelected && id != it.id }
        if (previouslyEdited != null) {
            val index = _items.indexOf(previouslyEdited)
            _items[index] = previouslyEdited.copy(isEditMode = false)
        }


        val currentIndex = _items.indexOfFirst { id == it.id }
        _items[currentIndex] =
            _items[currentIndex].copy(isEditMode = !_items[currentIndex].isEditMode)
    }

    // Can't pass here an Offset, because drag captures current position
    // Have to pass an offset and calculate current position here
    fun move(id: Int, offset: Offset) {
        val index = _items.indexOfFirst { id == it.id }

        val currentPos = _items[index].topLeft
        _items[index] = _items[index].copy(
            topLeft = Offset(
                x = currentPos.x + offset.x,
                y = currentPos.y + offset.y
            )
        )
    }
}