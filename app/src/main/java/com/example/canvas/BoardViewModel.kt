package com.example.canvas

import androidx.compose.ui.geometry.Offset
import androidx.lifecycle.ViewModel
import com.example.canvas.data.ShapeModel
import com.example.canvas.data.ShapeType
import com.example.canvas.data.StickyModel
import com.example.canvas.data.toBoardItemModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class BoardViewModel @Inject constructor(
    private val boardManager: BoardManager
) : ViewModel() {
    val elements = boardManager.items


    /**
     * If currently selected item is saved in a var, you have to manage that var
     * When item will be deleted. -> Better search in list for selected item
     *
     * BoardItem, BoardObject ?
     *
     * When DOUBLE CLICK, both select and go to editmode
     *
     * First save element in DB, then get it [id] from DB
     */

    // todo make addItem function instead
    init {
        val circle = ShapeModel(
            id = 1,
            type = ShapeType.CIRCLE,
            topLeft = Offset(x = 100f, y = 100f)
        )

        val square = ShapeModel(
            id = 2,
            type = ShapeType.SQUARE,
            topLeft = Offset(x = 300f, y = 100f)
        )

        val sticky = StickyModel(
            id = 3,
            author = "sandeep",
            topLeft =  Offset(x = 500f, y = 500f)
        )

        boardManager.addItem(circle.toBoardItemModel())
        boardManager.addItem(square.toBoardItemModel())
        boardManager.addItem(sticky.toBoardItemModel())
    }

    // todo select
    fun onSelect(id: Int) {
        boardManager.selectItem(id)
    }

    fun onEditMode(id: Int) {
        boardManager.changeEditMode(id)
    }

    fun onMove(id: Int, offset: Offset) {
        boardManager.move(id, offset)
    }
}