package com.example.canvas

import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.basicMarquee
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.safeDrawingPadding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.view.WindowCompat
import com.example.canvas.composable.MarqueeText
import com.example.canvas.ui.theme.CanvasTheme
import dagger.hilt.android.AndroidEntryPoint

/**
 * TODO: Select only one element at a time
 * Use selection manager
 */

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)

        setContent {
            CanvasTheme {
                Surface(
                    modifier = Modifier
                        .safeDrawingPadding()
                        .fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
//                    BoardScreen()

                    Box( // Global box (in NewBoardContent)
                        modifier = Modifier.fillMaxSize()
                    ) {
                        Test()
                    }
                }
            }
        }
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
private fun Test() {
//    TextTest()
    TempExample()
//    PopupPlacementTest()
}


/**
 * Popup is just a popup without overlay and onDismiss?
 */
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun TempExample() {


   Box(
       modifier = Modifier.fillMaxSize(),
       contentAlignment = Alignment.Center
   ) {
       MarqueeText(
           modifier = Modifier
               .background(Color.Red.copy(alpha = 0.05f))
               .widthIn(max = 150.dp),
           text = "Very long text with marquee effect with many many words",
           initialIterations = 0,
//           text = "Very long text with asd",
           maxLines = 1,
//           overflow = TextOverflow.Ellipsis,
//           fontSize = 36.sp,
           velocity = 90.dp,
           onMarqueeStart = {
               Log.i("kosilov", "TempExample: onMarqueeStart")
           },
           onMarqueeEnd = {
               Log.i("kosilov", "TempExample: onMarqueeEnd")
           }
       )
   }
}


@RequiresApi(Build.VERSION_CODES.O)
@Preview
@Composable
fun Preview() {
    Surface(
        modifier = Modifier.fillMaxSize()
    ) {
        TempExample()
    }
}