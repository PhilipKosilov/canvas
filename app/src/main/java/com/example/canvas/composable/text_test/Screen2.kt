package com.example.canvas.composable.text_test

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.TextMeasurer
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.rememberTextMeasurer
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize

/**
 * Description:
 *
 * - Static test of how Compose wrapping text.
 * - Contains 2 versions: Green at 100% of default text size, Blue at 265% of default text size.
 *
 * - 1 (10%): Uses some fixed max width in dp. Scale it and font size by same 265%. Uses widthIn modifier.
 * - 2 (20%): Calculates max width as 8 'W' characters that should scale with font size. Then calculates actual Size of the text with max width constraint. Uses size modifier.
 * - 3 (30%): Calculates max width as 8 'W' characters that should scale with font size. Passes it to widthIn modifier.
 */

@Composable
fun TextScreen2() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(24.dp)
    ) {
        // HEADER
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally),
            text = "Screen 2"
        )

        Content()
    }
}

@OptIn(ExperimentalTextApi::class)
@Composable
private fun Content() {
    val density = LocalDensity.current
    val textMeasurer = rememberTextMeasurer()
    val defaultTextStyle = LocalTextStyle.current
    val textToMeasure = "1234567890".repeat(2)

    /////////////////////////////////////////
    //   Default text size
    /////////////////////////////////////////
    val autoMaxWidth = 110.dp
    val autoMaxWidthNumChar = with(density) {
        measureSizeDp(
            measurer = textMeasurer,
            text = "W".repeat(8),
            style = defaultTextStyle,
        ).width
    }
    val currentSizeLimitByNumChar = with(density) {
        measureSizeDp(
            measurer = textMeasurer,
            text = textToMeasure,
            style = defaultTextStyle,
            constraints = Constraints(maxWidth = with(density) { autoMaxWidthNumChar.roundToPx() })
        )
    }


    /////////////////////////////////////////
    //   Scaled text size
    /////////////////////////////////////////
    val scaleFactor = 2.65f
    val scaledTextStyle = defaultTextStyle.copy(
        fontSize = defaultTextStyle.fontSize.times(scaleFactor),
        lineHeight = defaultTextStyle.lineHeight.times(scaleFactor),
    )

    val scaledAutoMaxWidth = 110.dp.times(scaleFactor)
    val scaledAutoMaxWidthNumChar = with(density) {
        measureSizeDp(
            measurer = textMeasurer,
            text = "W".repeat(8),
            style = scaledTextStyle,
        ).width
    }

    val scaledSizeLimitByNumChars = with(density) {
        measureSizeDp(
            measurer = textMeasurer,
            text = textToMeasure, //todo test value
            style = scaledTextStyle, //todo change after testing
            constraints = Constraints(maxWidth = with(density) { scaledAutoMaxWidthNumChar.roundToPx() })
        )
    }

    /////////////////////////////////////////
    //   UI
    /////////////////////////////////////////
    Column(
        modifier = Modifier
            .fillMaxSize()
    ) {
        // DEFAULT TEXT SIZE
        // autoMaxWidth is default value
        Text(
            modifier = Modifier
                .widthIn(max = autoMaxWidth)
                .background(Color.Green.copy(alpha = 0.1f)),
            text = textToMeasure,
            style = defaultTextStyle
        )

        // currentTextSize is provided by measure text with autoMaxWidth constraint
        Text(
            modifier = Modifier
                .size(currentSizeLimitByNumChar)
                .background(Color.Green.copy(alpha = 0.2f)),
            text = textToMeasure,
            style = defaultTextStyle
        )

        // autoMax20Char is measured text with current style and N of chars
        Text(
            modifier = Modifier
                .widthIn(max = autoMaxWidthNumChar)
                .background(Color.Green.copy(alpha = 0.3f)),
            text = textToMeasure,
            style = defaultTextStyle
        )

        // SCALED FONT SIZE x2.65
        Text(
            modifier = Modifier
                .widthIn(max = scaledAutoMaxWidth)
                .background(Color.Blue.copy(alpha = 0.1f)),
            text = textToMeasure,
            style = scaledTextStyle
        )

        Text(
            modifier = Modifier
                .size(scaledSizeLimitByNumChars)
                .background(Color.Blue.copy(alpha = 0.2f)),
            text = textToMeasure,
            style = scaledTextStyle
        )

        Text(
            modifier = Modifier
                .widthIn(max = scaledAutoMaxWidthNumChar)
                .background(Color.Blue.copy(alpha = 0.3f)),
            text = textToMeasure,
            style = scaledTextStyle
        )
    }
}

@OptIn(ExperimentalTextApi::class)
private fun Density.measureSizeDp(
    measurer: TextMeasurer,
    text: String = "",
    style: TextStyle = TextStyle.Default,
    constraints: Constraints = Constraints(),
): DpSize {
    val innerText = AnnotatedString(text = text)
    val textSize = measurer.measure(
        text = innerText,
        style = style,
        density = this,
        softWrap = true,
        constraints = constraints
    ).size.toSize()

    return textSize.toDpSize()
}