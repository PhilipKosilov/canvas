package com.example.canvas.composable

import androidx.compose.foundation.gestures.awaitEachGesture
import androidx.compose.foundation.gestures.awaitFirstDown
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.pointerInput
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch

//todo doesn't really work
@Composable
fun ClickOutsideDetector(
    onDismiss: () -> Unit,
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit
) {
    Box(modifier = Modifier
        .fillMaxSize()
    ) {
        // Transparent overlay to detect clicks outside the content
        Box(
            modifier = Modifier
                .matchParentSize()
                .pointerInput(Unit) {
                    coroutineScope {
                        awaitEachGesture {
                            val down = awaitFirstDown()
                            down.consume()

                            launch {
                                onDismiss()
                            }
                        }
                    }
                }
        )
    }

    // Your actual content
    Box(modifier = modifier) {
        content()
    }
}