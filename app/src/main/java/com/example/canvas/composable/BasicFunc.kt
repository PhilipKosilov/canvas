package com.example.canvas.composable

import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import kotlin.math.roundToInt


@Composable
fun BasicFunc(
    text: String = "",
    textStyle: TextStyle,
    position: Offset,
    boxSize: Float,
    isSelected: Boolean = false,
    isEditMode: Boolean = false,
    onSelect: () -> Unit = { },
    onEditMode: () -> Unit = { }, // onChangeEditMode
    shape: DrawScope.() -> Unit = { },
) {
    var offsetX by remember { mutableStateOf(position.x) }
    var offsetY by remember { mutableStateOf(position.y) }

    var textFieldValue by remember {
        mutableStateOf(TextFieldValue(text, TextRange(text.length)))
    }

    // To check, when IME keyboard is open/closed
    val isKeyboardVisible by keyboardAsState()

    // To request focus in BasicTextField, when clicking on Box
    val focusRequester = remember { FocusRequester() }
    // To clear focus, when closing keyboard
    val focusManager = LocalFocusManager.current

    LaunchedEffect(isEditMode) {
        if (isEditMode) {
            focusRequester.requestFocus()
        }
    }

    // When closing IME keyboard during editing
    LaunchedEffect(isKeyboardVisible) {
        if (isEditMode && !isKeyboardVisible) {
            onEditMode()
            focusManager.clearFocus()
        }
    }

    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .size(boxSize.dp)
            .offset { IntOffset(offsetX.roundToInt(), offsetY.roundToInt()) }
            .pointerInput(Unit) {
                detectDragGestures { change, dragAmount ->
                    change.consume()
                    offsetX += dragAmount.x
                    offsetY += dragAmount.y
                }
            }
            .pointerInput(Unit) {
                detectTapGestures(
                    onTap = {
                        // if click when editing, stop editing
                        if (isEditMode) {
                            onEditMode()
                        } else {
                            onSelect()
                        }
                    },
                    onDoubleTap = {
                        onEditMode()
                    }
                )
            }
            .drawBehind {
                shape()
                // border drawing moved to Shape, because borders could be different
            },
    ) {
        if (isEditMode) {
            BasicTextField(
                modifier = Modifier
//                    .background(Color.LightGray) // temp background
                    .focusRequester(focusRequester),
                value = textFieldValue,
                singleLine = true,
                onValueChange = {
                    textFieldValue = it
                },
                textStyle = textStyle.copy(textAlign = TextAlign.Center) // this is how align text
                // Doesn't work, if change focus here
            )

        } else {
            Text(
                modifier = Modifier.focusRequester(focusRequester),
                text = textFieldValue.text,
                textAlign = TextAlign.Center,
                style = textStyle,
                maxLines = 1
            )
        }
    }
}