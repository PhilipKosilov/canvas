package com.example.canvas.composable

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun ToggleButtonGroup(
    selectedIndex: Int,
    states: List<String>,
    modifier: Modifier = Modifier,
    shape: Shape = RoundedCornerShape(24.dp),
    onItemSelection: (selectedItemIndex: Int) -> Unit = {},
) {
    Row(
        modifier = modifier
//            .padding(8.dp)
            .clip(shape = shape)
//            .background(color = MaterialTheme.colorScheme.primary)
            .border(
                width = 1.dp,
                color = Color.LightGray,
                shape = shape
            )
            .width(IntrinsicSize.Max)
    ) {
        states.forEachIndexed { index, text->
            val horizontalPaddings = if (index == states.lastIndex) {
                Modifier.padding(end = 4.dp)
            } else {
                Modifier.padding(start = 4.dp)
            }

            Text(
                text = text,
                color = if (selectedIndex == index) {
                    Color.White
//                    Color.Gray
                } else {
                    Color.Gray
//                    Color.White
                },
                style = LocalTextStyle.current.copy(
                    fontSize = 14.sp,
                    fontWeight = if (selectedIndex == index) {
                        FontWeight.SemiBold
                    } else {
                        FontWeight.Normal
                    }
                ),
                modifier = Modifier
                    .weight(1f)
                    .padding(vertical = 4.dp)
                    .then(horizontalPaddings)
                    .clip(shape = shape)
                    .clickable {
                        onItemSelection(index)
                    }
                    .background(
                        if (selectedIndex == index) {
                            MaterialTheme.colorScheme.primary
//                            Color.White
                        } else {
                            Color.Transparent
                        }
                    )
                    .padding(8.dp),
                textAlign = TextAlign.Center
            )
        }
    }
}

@Preview
@Composable
private fun Preview() {
    ToggleButtonGroup(
        selectedIndex = 0,
        states = listOf("First", "Second")
    )
}