package com.example.canvas.composable.text_test

import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.awaitEachGesture
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.PointerInputChange
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.launch

/**
 * Example of Scaffold with bottom sheet and snackbar.
 */

@Composable
fun TextScreen15() {
    Content()
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun Content() {
    val snackBarHostState = remember { SnackbarHostState() }
    val scope = rememberCoroutineScope()
    val sheetState = rememberModalBottomSheetState(skipPartiallyExpanded = false)
    var openBottomSheet by remember { mutableStateOf(false) }


    Scaffold(
        containerColor = Color.Green.copy(alpha = 0.2f),
        topBar = {
            TopAppBar(
                title = { Text(text = "Screen 15") },
                colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                    containerColor = Color.Red.copy(alpha = 0.2f)
                )
            )
        },
        snackbarHost = {
            SnackbarHost(hostState = snackBarHostState)
        },
        bottomBar = {
            BottomAppBar(
                modifier = Modifier
                    .clickable {

                    }
                    .padding(32.dp)
                ,
                containerColor = Color.Magenta.copy(alpha = 0.2f),
            ) {
                Text(
                    modifier = Modifier
                        .fillMaxSize()
                        .consumeAllPointerEvents(),
                    text = "Bottom bar"
                )
            }
        }
    ) { paddingValues ->
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(paddingValues)
                .clickable {

                },
            contentAlignment = Alignment.Center,
        ) {
            Column {
                Button(
                    onClick = {
                        openBottomSheet = true
                    }
                ) {
                    Text("Open bottom sheet")
                }

                Button(
                    onClick = {
                        scope.launch {
                            snackBarHostState.showSnackbar(
                                message = "Test",
                                duration = SnackbarDuration.Long,
                            )
                        }
                    }
                ) {
                    Text("Show snack")
                }
            }
        }

        if (openBottomSheet) {
            ModalBottomSheet(
                sheetState = sheetState,
                onDismissRequest = { openBottomSheet = false },
                dragHandle = null,

                ) {
                Column(
                    modifier = Modifier
                        .padding(24.dp)
                        .height(300.dp),
                ) {
                    Text("text1")
                    Text("text2")
                    Text("text3")
                    Text("text4")

                    Button(
                        onClick = {
                            scope.launch {
                                snackBarHostState.showSnackbar(
                                    message = "Test",
                                    duration = SnackbarDuration.Long,
                                )
                            }
                        }
                    ) {
                        Text("Show snack")
                    }
                }
            }
        }
    }
}

fun Modifier.consumeAllPointerEvents(): Modifier =
    pointerInput(Unit) {
        awaitEachGesture {
            while (true) {
                val event = awaitPointerEvent()
                event.changes.forEach { pointerInputChange: PointerInputChange ->
                    pointerInputChange.consume()
                }
            }
        }
    }