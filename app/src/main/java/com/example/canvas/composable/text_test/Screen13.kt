package com.example.canvas.composable.text_test

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandHorizontally
import androidx.compose.animation.shrinkHorizontally
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

/**
 * Demo of animations and transitions.
 */

@Composable
fun TextScreen13() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(24.dp)
    ) {
        // HEADER
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally),
            text = "Screen 13"
        )

//        VisibilityAnimation()
        ContentAnimation()
    }
}

@Composable
private fun ContentAnimation() {
    var isOpen by remember { mutableStateOf(false) }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Yellow.copy(alpha = 0.1f)),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Button(onClick = { isOpen = !isOpen }) {
            Text(text = "Switch")
        }


    }
}

@Composable
private fun VisibilityAnimation() {
    val animationTime = 300

    var isOpen by remember { mutableStateOf(false) }


    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Yellow.copy(alpha = 0.1f)),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Button(onClick = { isOpen = !isOpen }) {
            Text(text = "Open")
        }
        // Use animate content to swap two Composables.
        AnimatedVisibility(
            visible = isOpen,
            enter = slideInVertically {
                // Slide in from 40 dp from the top.
                100
            } + expandHorizontally(
                expandFrom = Alignment.CenterHorizontally
            ),
            exit = slideOutVertically { 100 } + shrinkHorizontally()
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(20.dp)
                    .background(Color.Blue.copy(alpha = 0.5f))
            )
        }
    }
}