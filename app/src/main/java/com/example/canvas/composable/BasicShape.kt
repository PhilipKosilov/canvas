package com.example.canvas.composable

import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import com.example.canvas.data.BoardItemModel


@Composable
fun BasicShape(
    data: BoardItemModel,
    onSelect: (Int) -> Unit = { },
    onEditMode: (Int) -> Unit = { },
    onMove: (Int, Offset) -> Unit = { _, _ -> },
    backgroundCanvas: DrawScope.() -> Unit = { },
    borderCanvas: DrawScope.() -> Unit = { },
) {
    val focusRequester = remember { FocusRequester() }

    var textFieldValue by remember {
        mutableStateOf(TextFieldValue(data.text!!, TextRange(data.text.length)))
    }

    BasicEditableBoardItem(
        position = data.topLeft,
        size = data.size, // square has equal sizes
        isSelected = data.isSelected,
        isEditMode = data.isEditMode,
        focusRequester = focusRequester, // this is important
        onSelect = {
            onSelect(data.id)
        },
        onEditMode = {
            onEditMode(data.id)
        },
        onMove = { offset ->
            onMove(data.id, offset)
        },
        backgroundCanvas = backgroundCanvas,
        borderCanvas = borderCanvas,
        content = {
            if (data.isEditMode) {
                BasicTextField(
                    modifier = Modifier
//                    .background(Color.LightGray) // temp background
                        .focusRequester(focusRequester),
                    value = textFieldValue,
                    singleLine = true,
                    onValueChange = {
                        textFieldValue = it
                    },
                    textStyle = data.textStyle?.copy(textAlign = TextAlign.Center)!! // this is how align text
                )
            } else {
                Text(
                    modifier = Modifier.focusRequester(focusRequester),
                    text = textFieldValue.text,
                    textAlign = TextAlign.Center,
                    style = data.textStyle!!,
                    maxLines = 1
                )
            }
        }
    )
}