package com.example.canvas.composable.text_test

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.Divider
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalFontFamilyResolver
import androidx.compose.ui.text.Paragraph
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.canvas.handles.pxToDpSize

/**
 * Demo of Paragraph line breaking and line width calculations.
 *
 * Lines ending in white space ignore it when maxWidth limit is set. So actual line width can be greater that maxWidth.
 */

@Composable
fun TextScreen11() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(24.dp)
    ) {
        // HEADER
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally),
            text = "Screen 11"
        )

        Content()
    }
}

@Composable
private fun Content() {
    val localDensity = LocalDensity.current
    val fontFamilyResolver = LocalFontFamilyResolver.current

    with(localDensity) {
        val textStyleLocal = LocalTextStyle.current
        var fontSize by remember { mutableStateOf(60.sp) }
        val textStyle by remember(fontSize) {
            mutableStateOf(
                textStyleLocal.copy(fontSize = fontSize, lineHeight = textStyleLocal.lineHeight * (fontSize.value / 24f))
            )
        }

//        val textToDisplay = "Text"
        val textToDisplay = "This is text"
        val inputFieldSize = DpSize(250.dp, 200.dp)
//    val inputFieldSize = DpSize(100.dp, 100.dp)

        val fitTextInParagraph: (fontSize: TextUnit) -> Paragraph = { fontSize ->
            makeAParagraph(
                text = textToDisplay,
                style = textStyle,
                curFontSize = fontSize,
                inputFieldSize = inputFieldSize,
                localDensity = localDensity,
                fontFamilyResolver = fontFamilyResolver,
            )
        }
        val inlineText = fitTextInParagraph(textStyle.fontSize)
        val maxLineWidth = inlineText.maxLineWidth().toDp()

        Row {
            Button(
                onClick = {
                    fontSize = (fontSize.value + 2f).sp
                }
            ) {
                Text("+")
            }

            Button(
                onClick = {
                    fontSize = (fontSize.value - 2f).sp
                }
            ) {
                Text("-")
            }
        }

        Box {
            Text(
                modifier = Modifier
                    .background(Color.Blue.copy(alpha = 0.1f))
                    .size(width = inputFieldSize.width, height = inputFieldSize.height)
                    .onSizeChanged {
                        val inlineSize = "${inlineText.width.toDp()} x ${inlineText.height.toDp()}"
                        val sizeDp = it.pxToDpSize(localDensity)

                        Log.i(
                            "kosilov",
                            "Text: sizeDp=$sizeDp, inlineSize=$inlineSize, lines=${inlineText.lineCount}, maxLineWidth=$maxLineWidth"
                        )
                    },
                text = textToDisplay,
                style = textStyle,
            )

            Box(
                modifier = Modifier
                    .size(width = maxLineWidth, height = inlineText.height.toDp())
                    .background(Color.Green.copy(alpha = 0.2f))
            )
        }
        Divider(color = Color.Transparent, thickness = 20.dp)

        Text(
            text = "BLUE - Shape's size. Width is passes as Constraint(maxWidth)"
        )

        Divider(color = Color.Transparent, thickness = 10.dp)

        Text(
            text = "GREEN - maximum line width"
        )
    }
}

private fun Paragraph.maxLineWidth(): Float {
    return List(lineCount) { index ->
        val lineEnd = getLineEnd(index)
        Log.i("kosilov", "maxLineWidth: index=$index, lineEnd=$lineEnd")
        getLineWidth(index)
    }.max()
}

private fun makeAParagraph(
    text: String,
    style: TextStyle,
    curFontSize: TextUnit, // can be different from style.FontSize
    inputFieldSize: DpSize,
    localDensity: Density,
    fontFamilyResolver: FontFamily.Resolver,
): Paragraph {
    return with(localDensity) {
        Paragraph(
            text = text,
            style = style.copy(fontSize = curFontSize),
            constraints = Constraints(maxWidth = inputFieldSize.width.roundToPx()),
//        constraints = Constraints(minWidth = inputFieldMaxWidth), // Exception not supported???
//        constraints = Constraints(), //if no constraint, width is Int.MAX
            density = localDensity,
            fontFamilyResolver = fontFamilyResolver,
        )
    }
}