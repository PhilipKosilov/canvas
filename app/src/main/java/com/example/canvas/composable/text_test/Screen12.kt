package com.example.canvas.composable.text_test

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.Divider
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalFontFamilyResolver
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.Paragraph
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.LineBreak
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

/**
 * Demo of different strategies for Paragraph line breaking (Default, Heading, Paragraph).
 */

@Composable
fun TextScreen12() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(24.dp)
    ) {
        // HEADER
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally),
            text = "Screen 12"
        )

        Content()
//        Content2()
    }
}

@OptIn(ExperimentalTextApi::class)
@Composable
private fun Content() {
    val textStyleLocal = LocalTextStyle.current
//    var fontSize by remember { mutableStateOf(24.sp) }
    var fontSize by remember { mutableStateOf(48.sp) }

    val textToDisplay = "This is text"
//    val textToDisplay = "Alternative text"
//    val textToDisplay = "Alternative"
//    val textToDisplay = "This is 12345678912345678912456789134567"
    val inputFieldSize = DpSize(144.dp, 124.dp)

    Row {
        Button(
            onClick = {
                fontSize = (fontSize.value + 2f).sp
            }
        ) {
            Text("+")
        }

        Button(
            onClick = {
                fontSize = (fontSize.value - 2f).sp
            }
        ) {
            Text("-")
        }
    }

    /*
        strategy = Strategy.Simple,
        strictness = Strictness.Normal,
        wordBreak = WordBreak.Default
     */
    TextItem(
        text = textToDisplay,
        style = textStyleLocal,
        fontSize = fontSize,
        size = inputFieldSize
    )

    Divider(color = Color.Transparent, thickness = 20.dp)

    /*
        strategy = Strategy.Balanced,
        strictness = Strictness.Loose,
        wordBreak = WordBreak.Phrase
     */
    TextItem(
        text = textToDisplay,
        style = textStyleLocal.copy(lineBreak = LineBreak.Heading),
        fontSize = fontSize,
        size = inputFieldSize
    )

    Divider(color = Color.Transparent, thickness = 20.dp)

    /*
        strategy = Strategy.HighQuality,
        strictness = Strictness.Strict,
        wordBreak = WordBreak.Default
     */
    TextItem(
        text = textToDisplay,
        style = textStyleLocal.copy(lineBreak = LineBreak.Paragraph),
        fontSize = fontSize,
        size = inputFieldSize
    )

    Divider(color = Color.Transparent, thickness = 20.dp)

    TextItem(
        text = textToDisplay,
        style = textStyleLocal.copy(
            lineBreak = LineBreak.Simple.copy(
                strategy = LineBreak.Strategy.Balanced,
//                strictness = LineBreak.Strictness.Default,
//                wordBreak = LineBreak.WordBreak.Default
            )
        ),
        fontSize = fontSize,
        size = inputFieldSize
    )
}

@Composable
private fun Content2() {
    val textStyleLocal = LocalTextStyle.current
//    var fontSize by remember { mutableStateOf(24.sp) }
    var fontSize by remember { mutableStateOf(48.sp) }

    val textToDisplay = ""
//    val textToDisplay = "This is text"
//    val textToDisplay = "Alternative text"
//    val textToDisplay = "Alternative"
//    val textToDisplay = "This is 12345678912345678912456789134567"
    val inputFieldSize = DpSize(144.dp, 124.dp)

    Row {
        Button(
            onClick = {
                fontSize = (fontSize.value + 2f).sp
            }
        ) {
            Text("+")
        }

        Button(
            onClick = {
                fontSize = (fontSize.value - 2f).sp
            }
        ) {
            Text("-")
        }
    }

    TextItem(
        text = textToDisplay,
        style = textStyleLocal,
        fontSize = fontSize,
        size = inputFieldSize
    )

    Divider(color = Color.Transparent, thickness = 20.dp)

    TextItem2(
        text = textToDisplay,
        style = textStyleLocal,
        fontSize = fontSize,
        size = inputFieldSize
    )
}

@Composable
private fun TextItem(
     text: String,
     style: TextStyle,
     fontSize: TextUnit,
     size: DpSize,
) {
    val localDensity = LocalDensity.current
    val fontFamilyResolver = LocalFontFamilyResolver.current
    val textStyle = style.copy(fontSize = fontSize, lineHeight = style.lineHeight * (fontSize.value / 24f))

    with(localDensity) {
        val fitTextInParagraph: (fontSize: TextUnit) -> Paragraph = { fontSize ->
            makeAParagraph(
                text = text,
                style = textStyle,
                curFontSize = fontSize,
                inputFieldSize = size,
                localDensity = localDensity,
                fontFamilyResolver = fontFamilyResolver,
            )
        }
        val inlineText = fitTextInParagraph(fontSize)
        val maxLineWidth = inlineText.maxLineWidth().toDp()
        Log.i("kosilov", "TextItem: maxLineWidth=${maxLineWidth}, inlineText.height=${inlineText.height.toDp()}")

        Box {
            Text(
                modifier = Modifier
                    .background(Color.Blue.copy(alpha = 0.1f))
                    .size(width = size.width, height = size.height)
                ,
                text = text,
                style = textStyle,
            )

            Box(
                modifier = Modifier
                    .size(width = maxLineWidth, height = inlineText.height.toDp())
                    .background(Color.Green.copy(alpha = 0.2f))
            )
        }
    }
}

@Composable
private fun TextItem2(
    text: String,
    style: TextStyle,
    fontSize: TextUnit,
    size: DpSize,
) {
    val localDensity = LocalDensity.current
    val fontFamilyResolver = LocalFontFamilyResolver.current
    val textStyle = style.copy(fontSize = fontSize, lineHeight = style.lineHeight * (fontSize.value / 24f))

    with(localDensity) {
        val fitTextInParagraphWithText: (textToFit: String, fontSize: TextUnit) -> Paragraph =
            { textToFit, fontSize ->
                makeAParagraph(
                    text = textToFit,
                    style = textStyle,
                    curFontSize = fontSize,
                    inputFieldSize = size,
                    localDensity = localDensity,
                    fontFamilyResolver = fontFamilyResolver,
                )
            }

        val actualSize = getTextSize(
            text = text,
            fontSize = fontSize,
            fitTextInParagraph = fitTextInParagraphWithText
        ).toDpSize()

        Box {
            Text(
                modifier = Modifier
                    .background(Color.Blue.copy(alpha = 0.1f))
                    .size(width = size.width, height = size.height),
                text = text,
                style = textStyle,
            )

            Box(
                modifier = Modifier
                    .size(width = actualSize.width, height = actualSize.height)
                    .background(Color.Green.copy(alpha = 0.2f))
            )
        }
    }
}

private fun Paragraph.maxLineWidth(): Float {
    return List(lineCount) { index ->
        getLineWidth(index)
    }.max()
}

private fun makeAParagraph(
    text: String,
    style: TextStyle,
    curFontSize: TextUnit, // can be different from style.FontSize
    inputFieldSize: DpSize,
    localDensity: Density,
    fontFamilyResolver: FontFamily.Resolver,
): Paragraph {
    return with(localDensity) {
        Paragraph(
            text = text,
            style = style.copy(fontSize = curFontSize),
            constraints = Constraints(maxWidth = inputFieldSize.width.roundToPx()),
//        constraints = Constraints(minWidth = inputFieldMaxWidth), // Exception not supported???
//        constraints = Constraints(), //if no constraint, width is Int.MAX
            density = localDensity,
            fontFamilyResolver = fontFamilyResolver,
        )
    }
}

private fun getTextSize(
    text: String,
    fontSize: TextUnit,
    fitTextInParagraph: (textToFit: String, fontSize: TextUnit) -> Paragraph,
) : Size {
    return with(fitTextInParagraph(text, fontSize)) {

        val width = List(lineCount) { index ->
            val endIndex = getLineEnd(index)

            // if ends on white space, don't include it in width
            if (text[endIndex-1] == ' ') {
                val startIndex = getLineStart(index)
                val lineText = text.substring(startIndex, endIndex).trimEnd()
                fitTextInParagraph(lineText, fontSize).getLineWidth(0)
            } else {
                getLineWidth(index)
            }
        }.max()

        Size(width = width, height = height)
    }
}