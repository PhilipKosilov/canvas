package com.example.canvas.composable.text_test

import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import com.example.canvas.composable.autosize.AutoSizeText

/**
 * Description:
 *
 * - Uses AutoSizeText that supposedly should adapt font size to fit inside a given size.
 * - Uses some fixed max width in dp which is then modified by another value in infinite transition.
 */

@Composable
fun TextScreen3() {
    Column(
        modifier = Modifier
            .fillMaxSize()
    ) {
        // HEADER
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally),
            text = "Screen 3"
        )

        Content()
    }
}

@Composable
private fun Content() {
    val density = LocalDensity.current
    val defaultTextSize = DpSize(120.dp, 100.dp)

    val infiniteTransition = rememberInfiniteTransition()
    val sizeChange by infiniteTransition.animateFloat(
        initialValue = with(density) { 0.dp.toPx() },
        targetValue = with(density) { 50.dp.toPx() },
        animationSpec = infiniteRepeatable(
            animation = tween(
                durationMillis = 3000,
                easing = LinearEasing
            ),
            repeatMode = RepeatMode.Reverse
        )
    )

    val textSize by remember(sizeChange) {
        mutableStateOf(defaultTextSize.plus(DpSize(sizeChange.dp, sizeChange.dp)))
    }

    Surface(modifier = Modifier
        .fillMaxSize()
        .padding(32.dp)
    ) {
        Box {
            AutoSizeText(
                modifier = Modifier
                    .size(textSize)
                    .background(Color.Blue.copy(alpha = 0.1f)),
                text = "Testingtest"
            )
        }
    }
}
