package com.example.canvas.composable

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Popup
import com.example.canvas.R

/**
 * Popup is placed on top of parent.
 * DropdownMenu is placed near its parent (either top or bottom).
 */
@Composable
fun PopupPlacementTest() {
    var isTopLeft by remember { mutableStateOf(false) }
    var isTopCenter by remember { mutableStateOf(false) }
    var isTopRight by remember { mutableStateOf(false) }
    var isCenterLeft by remember { mutableStateOf(false) }
    var isCenter by remember { mutableStateOf(false) }
    var isCenterRight by remember { mutableStateOf(false) }
    var isBottomLeft by remember { mutableStateOf(false) }
    var isBottomCenter by remember { mutableStateOf(false) }
    var isBottomRight by remember { mutableStateOf(false) }

    Box(
        modifier = Modifier.fillMaxSize()
    ) {
        TestItem(
            text = "Top left",
            isVisible = isTopLeft,
            alignment = Alignment.TopStart,
            onClick = { isTopLeft = true },
            onDismiss = { isTopLeft = false }
        )

        TestItem(
            text = "Top center",
            isVisible = isTopCenter,
            alignment = Alignment.TopCenter,
            onClick = { isTopCenter = true },
            onDismiss = { isTopCenter = false }
        )

        TestItem(
            text = "Top right",
            isVisible = isTopRight,
            alignment = Alignment.TopEnd,
            onClick = { isTopRight = true },
            onDismiss = { isTopRight = false }
        )

        TestItem(
            text = "Center left",
            isVisible = isCenterLeft,
            alignment = Alignment.CenterStart,
            onClick = { isCenterLeft = true },
            onDismiss = { isCenterLeft = false }
        )

        TestItem(
            text = "Center",
            isVisible = isCenter,
            alignment = Alignment.Center,
            onClick = { isCenter = true },
            onDismiss = { isCenter = false }
        )

        TestItem(
            text = "Center right",
            isVisible = isCenterRight,
            alignment = Alignment.CenterEnd,
            onClick = { isCenterRight = true },
            onDismiss = { isCenterRight = false }
        )

        TestItem(
            text = "Bottom left",
            isVisible = isBottomLeft,
            alignment = Alignment.BottomStart,
            onClick = { isBottomLeft = true },
            onDismiss = { isBottomLeft = false }
        )

        TestItem(
            text = "Bottom center",
            isVisible = isBottomCenter,
            alignment = Alignment.BottomCenter,
            onClick = { isBottomCenter = true },
            onDismiss = { isBottomCenter = false }
        )

        TestItem(
            text = "Bottom right",
            isVisible = isBottomRight,
            alignment = Alignment.BottomEnd,
            onClick = { isBottomRight = true },
            onDismiss = { isBottomRight = false }
        )
    }
}

@Composable
private fun BoxScope.TestItem(
    text: String,
    isVisible: Boolean,
    alignment: Alignment,
    onClick: () -> Unit,
    onDismiss: () -> Unit,
) {
    Box(
        modifier = Modifier.align(alignment)
    ) {
        Icon(
            modifier = Modifier.clickable(onClick = onClick).border(1.dp, Color.Black),
            painter = painterResource(id = R.drawable.ic_lightbulb),
            contentDescription = null
        )

        //todo animating MyDropdownColumn doesn't work, because it's added to a separate parent.
        // Can't add animated content INSIDE DropDown, because DropDown is invisible by default
        // and making it visible will create overlay

        PopupV0(text, isVisible, onDismiss) //DropdownMenu
        PopupV2(text, isVisible, onDismiss) //Popup
        PopupV2_5(text, isVisible, onDismiss) //UltimatePopup
    }
}

/**
 *
 */
@Composable
private fun PopupV2_5(
    text: String,
    isVisible: Boolean,
    onDismiss: () -> Unit,
) {
    var alignment by remember { mutableStateOf(Alignment.TopStart) }

    UltimatePopup(
        expanded = isVisible,
        alignment = alignment,
        animationDirection = Alignment.BottomCenter,
//        minimalMargin = 12.dp,
        offset = DpOffset(12.dp, 12.dp),
        onDismissRequest = onDismiss,
    ) {
        Column(
            modifier = Modifier
                .background(Color.Green.copy(alpha = 0.2f))
                .padding(24.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Text(
                text = text,
                modifier = Modifier
            )

            listOf(
                Alignment.TopStart to "TopStart",
                Alignment.TopCenter to "TopCenter",
                Alignment.TopEnd to "TopEnd",
                Alignment.CenterStart to "CenterStart",
                Alignment.Center to "Center",
                Alignment.CenterEnd to "CenterEnd",
                Alignment.BottomStart to "BottomStart",
                Alignment.BottomCenter to "BottomCenter",
                Alignment.BottomEnd to "BottomEnd",
            ).forEach {
                Text(
                    text = it.second,
                    modifier = Modifier.clickable { alignment = it.first }
                )
            }
        }
    }
}

/**
 * Has alignment. Placed on top of the anchor (no padding). No/default animation.
 */
@Composable
private fun PopupV2(
    text: String,
    isVisible: Boolean,
    onDismiss: () -> Unit,
) {
    var alignment by remember {
        mutableStateOf(Alignment.TopStart)
    }

    if (isVisible) {
        Popup(
            alignment = alignment,
            onDismissRequest = onDismiss,
        ) {
            Column(
                modifier = Modifier
                    .background(Color.Green.copy(alpha = 0.2f))
                    .padding(24.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Text(
                    text = text,
                    modifier = Modifier
                )

                listOf(
                    Alignment.TopStart to "TopStart",
                    Alignment.TopCenter to "TopCenter",
                    Alignment.TopEnd to "TopEnd",
                    Alignment.CenterStart to "CenterStart",
                    Alignment.Center to "Center",
                    Alignment.CenterEnd to "CenterEnd",
                    Alignment.BottomStart to "BottomStart",
                    Alignment.BottomCenter to "BottomCenter",
                    Alignment.BottomEnd to "BottomEnd",
                ).forEach {
                    Text(
                        text = it.second,
                        modifier = Modifier.clickable { alignment = it.first }
                    )
                }
            }
        }
    }
}

/**
 * No alignment. Placed at the bottom of the anchor. Big padding from bottom of the screen.
 * Default animation.
 */
@Composable
private fun PopupV0(
    text: String,
    isVisible: Boolean,
    onDismiss: () -> Unit,
) {
    DropdownMenu(expanded = isVisible, onDismissRequest = onDismiss) {
        Text(
            text = text,
            modifier = Modifier
                .background(Color.Green.copy(alpha = 0.2f))
                .clickable {}
                .padding(24.dp)
        )
    }
}