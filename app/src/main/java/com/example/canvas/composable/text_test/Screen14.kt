package com.example.canvas.composable.text_test

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import kotlin.math.pow
import kotlin.math.roundToInt
import kotlin.math.sqrt

/**
 * Example of interactive line implementation based on Shapes and Modifier.clip.
 */

@Composable
fun TextScreen14() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(24.dp)
    ) {
        // HEADER
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally),
            text = "Screen 14"
        )

        Content()
    }
}

@Composable
private fun Content() {
    val density = LocalDensity.current
    var counter by remember { mutableStateOf(1) }

    // 1D line position. Line draws between start and end.
    // But as user drags the points on that line you don't just move item.
    // To move calculate the change of the "Top left" coordinate in the 2D line.
    // To resize calculate the difference between "Top left" and the "Bottom right" coordinates.
    var lineStart by remember { mutableStateOf(Offset(0f, 0f)) }
    var lineEnd by remember { mutableStateOf(Offset(200f, 200f)) }

    // 2D line coordinates, that include line thickness (3 default)
    val lineCoordinates by remember {
        derivedStateOf {
            calculateLine2D(lineStart, lineEnd)
        }
    }
    // 2D line coordinates, that is thicker to specify clickable area around the line.
    val clipLineCoordinates by remember {
        derivedStateOf {
            calculateLine2D(lineStart, lineEnd, thickness = 10f)
        }
    }

    // lineShape will be displayed. clipOutline is bigger to clip clickable area around the lineShape.
    val lineShape = createShape(lineCoordinates)
    val clipOutline = createShape(clipLineCoordinates)

    // TopLeft coordinate (2D) of the line regardless of 1D line position (originally lineStart is topLeft)
    val topLeftCoordinate by remember {
        derivedStateOf {
            calculateTopLeftCoordinate(clipLineCoordinates)!! //todo for now !! to simplify
        }
    }

    // TopLeft coordinate (2D) of the line regardless of 1D line position (originally lineEnd is bottomRight)
    val bottomRightCoordinate by remember {
        derivedStateOf {
            calculateBottomRightCoordinate(clipLineCoordinates)!!
        }
    }

    // Total size of the 2D line including thickness.
    val lineSize by remember {
        with(density) {
            derivedStateOf {
                DpSize(
                    width = (bottomRightCoordinate.x - topLeftCoordinate.x).toDp(),
                    height = (bottomRightCoordinate.y - topLeftCoordinate.y).toDp(),
                )
            }
        }
    }

    var offset by remember { // Global line offset like data.coordinate
        mutableStateOf(DpOffset.Zero)
    }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "Clicked:$counter"
        )

        Box( // Main content
            modifier = Modifier.fillMaxSize()
        ) {
            Box( //Resize box (contains line and handles)
                modifier = Modifier
                    .offset(offset.x, offset.y)
            ) {
                Box( // LINE
                    modifier = Modifier
                        .size(lineSize)
                        .background(Color.Black, shape = lineShape)
                        .background(Color.Green.copy(alpha = 0.2f), shape = clipOutline)
                        // clip before pointerInputs && offset
                        .clip(shape = clipOutline)
                        //Offset the clickable area to follow topLeft coordinate of clipping line.
                        .offset {
                            IntOffset(
                                x = topLeftCoordinate.x.roundToInt(),
                                y = topLeftCoordinate.y.roundToInt(),
                            )
                        }
                        .processDragModifier {
                            with(density) {
                                offset += DpOffset(
                                    x = it.x.toDp(),
                                    y = it.y.toDp()
                                )
                            }
                        }
                        .clickable {
                            counter++
                            Log.i("kosilov", "TempExample: clicked")
                        }
                )


                Box( // start handle
                    modifier = Modifier
                        .offset {
                            IntOffset(
                                x = lineStart.x.roundToInt() - 4.dp.roundToPx(),
                                y = lineStart.y.roundToInt() - 4.dp.roundToPx(),
                            )
                        }
                        .size(8.dp, 8.dp)
                        .clip(RectangleShape)
                        .background(Color.Cyan)
                        .processDragModifier {
                            lineStart += it
                        },
                )

                Box( // end handle
                    modifier = Modifier
                        .offset {
                            IntOffset(
                                x = lineEnd.x.roundToInt() - 4.dp.roundToPx(),
                                y = lineEnd.y.roundToInt() - 4.dp.roundToPx(),
                            )
                        }
                        .size(8.dp, 8.dp)
                        .background(Color.Cyan)
                        .processDragModifier {
                            lineEnd += it
                        },
                )
            }

            Box( // Zigzag line
                Modifier
                    .align(Alignment.BottomCenter)
                    .size(300.dp, 300.dp)
                    .background(color = Color.Blue, shape = LineShapeTop)
                    .clip(LineShapeTop)
                    .fillMaxSize()
                    .clickable { counter++ }
            )
        }
    }
}

private fun Modifier.processDragModifier(func: (dragAmount: Offset) -> Unit) =
    pointerInput(Unit) {
        detectDragGestures { change, dragAmount ->
            change.consume()

            func(dragAmount)
        }
    }

private fun calculateTopLeftCoordinate(coordinates: List<Offset>): Offset? {
    return when {
        coordinates.isEmpty() -> null

        else -> Offset(
            x = coordinates.minOf { it.x },
            y = coordinates.minOf { it.y },
        )
    }
}

private fun calculateBottomRightCoordinate(coordinates: List<Offset>): Offset? {
    return when {
        coordinates.isEmpty() -> null

        else -> Offset(
            x = coordinates.maxOf { it.x },
            y = coordinates.maxOf { it.y },
        )
    }
}

private fun createShape(coordinates: List<Offset>): Shape {
    return object : Shape {
        override fun createOutline(
            size: Size,
            layoutDirection: LayoutDirection,
            density: Density
        ): Outline {
            val path = Path().apply {
                coordinates.forEachIndexed { index, coordinate ->
                    when (index) {
                        0 -> moveTo(coordinate.x, coordinate.y)
                        else -> lineTo(coordinate.x, coordinate.y)
                    }
                }
                close()
            }

            return Outline.Generic(path)
        }
    }
}


private fun calculateLine2D(lineStart: Offset, lineEnd: Offset, thickness: Float = 3f): List<Offset> {
    // Calculate the direction vector of the line
    val dx = lineEnd.x - lineStart.x
    val dy = lineEnd.y - lineStart.y

    // Calculate half thickness
    val halfThickness = thickness / 2f

    // Calculate perpendicular vectors and normalize them
    val magnitude = sqrt(dx.pow(2) + dy.pow(2))
    val perpendicularX = -dy * halfThickness / magnitude
    val perpendicularY = dx * halfThickness / magnitude

    // Calculate the four vertices of the rectangle
    val coordinate1 = Offset(
        x = lineStart.x + halfThickness * perpendicularX,
        y = lineStart.y + halfThickness * perpendicularY
    )
    val coordinate2 = Offset(
        x = lineEnd.x + halfThickness * perpendicularX,
        y = lineEnd.y + halfThickness * perpendicularY
    )
    val coordinate3 = Offset(
        x = lineEnd.x - halfThickness * perpendicularX,
        y = lineEnd.y - halfThickness * perpendicularY
    )
    val coordinate4 = Offset(
        x = lineStart.x - halfThickness * perpendicularX,
        y = lineStart.y - halfThickness * perpendicularY
    )

    return listOf(coordinate1, coordinate2, coordinate3, coordinate4)
}


////////////////////////////////////
// Zigzag line
////////////////////////////////////
object LineShapeTop : Shape {
    private const val strokeWidth = 16.0f
    override fun createOutline(size: Size, layoutDirection: LayoutDirection, density: Density): Outline {
        val path = Path().apply {
            moveTo(0f, 0f)
            lineTo(size.width/2 + strokeWidth, 0f)
            lineTo(size.width/2 + strokeWidth, size.height- strokeWidth)
            lineTo(size.width, size.height- strokeWidth)

            lineTo(size.width, size.height)
            lineTo(size.width/2, size.height)
            lineTo(size.width/2, strokeWidth)
            lineTo(0f, strokeWidth)
            close()
        }
        return Outline.Generic(path)
    }
}