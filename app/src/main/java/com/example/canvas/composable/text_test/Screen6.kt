package com.example.canvas.composable.text_test

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material3.Button
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.TransformOrigin
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp

/**
 * Description:
 *
 * - Scaling text using combination of Modifier.scale and font size.
 *
 * - 1. Increase font size directly.
 * - 2. Calculating delta between default and current font size, then using scale
 */

@Composable
fun TextScreen6() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(24.dp)
    ) {
        // HEADER
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally),
            text = "Screen 6"
        )

        Content()
    }
}

@Composable
private fun Content() {
    val defaultTextStyle = LocalTextStyle.current
    val textToDisplay = "1234567890"
    val maxWidth by remember {
        mutableStateOf(65.dp)
    }
    // scale factor to increase font size by.
    var scale by remember { mutableStateOf(1f) }

    // V1 uses scaled font size directly.
    val bigTextStyle by remember(scale) {
        mutableStateOf(
            defaultTextStyle.copy(
                fontSize = defaultTextStyle.fontSize.times(scale),
                lineHeight = defaultTextStyle.lineHeight.times(scale),
            )
        )
    }

    // V2 calculates font scale difference between current and default size internally.
    // Text receives default font size and than scales with Modifier.graphicsLayer.
    val fontScale by remember(defaultTextStyle, bigTextStyle) {
        mutableStateOf(bigTextStyle.fontSize.value / defaultTextStyle.fontSize.value)
    }

    Row {
        Button(
            onClick = {
                scale *= 1.11f
            }
        ) {
            Text("+")
        }

        Button(
            onClick = {
                scale /= 1.11f
            }
        ) {
            Text("-")
        }
    }
    // Original text size
    Text(
        modifier = Modifier
            .widthIn(max = maxWidth)
            .background(Color.Green.copy(alpha = 0.1f)),
        text = textToDisplay,
        style = defaultTextStyle
    )

    // V1. Scales with font size
    Text(
        modifier = Modifier
            .widthIn(max = maxWidth.times(fontScale)) // As font size changes, need to correct maxWidth.
            .background(Color.Green.copy(alpha = 0.2f)),
        text = textToDisplay,
        style = bigTextStyle
    )

    // V2. Scales with graphicsLayer
    Text(
        modifier = Modifier
            .widthIn(max = maxWidth) // maxWidth for default font size stays unchanged.
            .graphicsLayer {
                scaleX = fontScale
                scaleY = fontScale
                transformOrigin = TransformOrigin(0f, 0f)
            }
            .background(Color.Green.copy(alpha = 0.3f)),
        text = textToDisplay,
        style = defaultTextStyle
    )
}
