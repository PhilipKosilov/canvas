package com.example.canvas.composable

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.canvas.data.BoardItemModel

@Composable
fun StickyNote(
    data: BoardItemModel,
    onSelect: (Int) -> Unit = { },
    onEditMode: (Int) -> Unit = { },
    onMove: (Int, Offset) -> Unit = { _, _ -> }
) {
    val focusRequester = remember { FocusRequester() }

    var textFieldValue by remember {
        mutableStateOf(TextFieldValue(data.text!!, TextRange(data.text.length)))
    }

    BasicEditableBoardItem(
        position = data.topLeft,
        size = data.size,
        isSelected = data.isSelected,
        isEditMode = data.isEditMode,
        focusRequester = focusRequester, // this is important
        onSelect = {
            onSelect(data.id)
        },
        onEditMode = {
            onEditMode(data.id)
        },
        onMove = { offset ->
            onMove(data.id, offset)
        },
        backgroundCanvas = {
            drawRect(
                color = data.color!!, //todo temp
                size = size
            )
        },
        borderCanvas = {
            drawBorder()
        },
        content = {
            Column(
                modifier = Modifier
                    .size(data.size.maxDimension.dp)
                    .padding(16.dp),
                verticalArrangement = Arrangement.SpaceBetween
            ) {
                if (data.isEditMode) {
                    BasicTextField(
                        modifier = Modifier
                            .focusRequester(focusRequester),
                        value = textFieldValue,
                        singleLine = true,
                        onValueChange = {
                            textFieldValue = it
                        },
                        decorationBox = { innerTextField ->
                            Row {
                                if (textFieldValue.text.isEmpty()) {
                                    Text(
                                        text = "Add text",
                                        style = data.textStyle!!,
                                    )
                                }
                                innerTextField()  //<-- Add this
                            }
                        },
                        textStyle = data.textStyle!! // in sticky, text is from START with some padding
                    )
                } else {
                    Text(
                        text = textFieldValue.text.ifEmpty {
                            "Add text"
                        },
                        textAlign = TextAlign.Center,
                        style = data.textStyle!!,
                        maxLines = 1 // more than 1 line for sticky
                    )
                }

                Text(
                    text = data.author!!,
                    textAlign = TextAlign.Center,
                    style = data.textStyle.copy(fontSize = 8.sp), // todo different style for author
                )
            }
        }
    )
}