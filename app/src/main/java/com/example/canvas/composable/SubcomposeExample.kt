package com.example.canvas.composable

import android.util.Log
import android.util.Size
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.SubcomposeLayout
import androidx.compose.ui.unit.IntSize
import com.example.canvas.handles.SlotsEnum
import com.example.canvas.utils.TAG

@Composable
fun SubcomposeExample() {
    Box( // com.example.canvas.main Box (Root)
        modifier = Modifier.fillMaxSize()
    ) {
        SubComponent(
            mainContent = {
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = "MainContent"
                )
            },
            dependentContent = { size ->
                Log.i(TAG, "Dependent size: $size")
                Text("Dependent Content")
            }
        )
    }
}

@Composable
fun SubComponent(
    modifier: Modifier = Modifier,
    mainContent: @Composable () -> Unit,
    dependentContent: @Composable (IntSize) -> Unit
) {
    SubcomposeLayout(
        modifier = modifier
    ) { constraints ->
        val mainPlaceables = subcompose(SlotsEnum.Main, mainContent).map {
            it.measure(constraints)
        }

        val maxSize = mainPlaceables.fold(IntSize.Zero) { currentMax, placeable ->
            IntSize(
                width = maxOf(currentMax.width, placeable.width),
                height = maxOf(currentMax.height, placeable.height)
            )
        }

        layout(maxSize.width, maxSize.height) {
            mainPlaceables.forEach { it.placeRelative(0, 0) }

            subcompose(SlotsEnum.Dependent) {
                dependentContent(maxSize)
            }.forEach {
                it.measure(constraints).placeRelative(0, 0)
            }
        }
    }
}