package com.example.canvas.composable

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.unit.dp

fun DrawScope.drawBorder() {
    drawRect(
        color = Color.Black, //blue
        size = size,
        style = Stroke(
            width = 1.dp.toPx()
        )
    )

    drawCornerHandles()
}

fun DrawScope.drawCornerHandles() {
    val circleRadius = 3.dp.toPx()
    val offsetX = circleRadius + 8.dp.toPx()
    val offsetY = circleRadius + 6.dp.toPx()

    val topLeft = Offset(x = -offsetX, y = -offsetY)
    val topRight = Offset(x = size.width + offsetX, y = -offsetY)
    val middleLeft = Offset(x = -offsetX, y = size.height / 2)
    val middleRight = Offset(x = size.width + offsetX, y = size.height / 2)
    val bottomLeft = Offset(x = -offsetX, y = size.height + offsetY)
    val bottomRight = Offset(x = size.width + offsetX, y = size.height + offsetY)

    listOf(
        topLeft, topRight,
        middleLeft, middleRight,
        bottomLeft, bottomRight,
    ).forEach {
        drawCornerCircle(circleRadius, it)
    }
}

fun DrawScope.drawCornerCircle(
    radius: Float,
    center: Offset,
) {
    drawCircle(
        color = Color.White,// gray
        radius = radius,
        center = center
    )

    drawCircle(
        color = Color(0xFF333333),
        radius = radius,
        center = center,
        style = Stroke(
            width = 1.dp.toPx()
        )
    )
}
