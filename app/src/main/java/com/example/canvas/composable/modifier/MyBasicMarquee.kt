package com.example.canvas.composable.modifier

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.StartOffset
import androidx.compose.animation.core.TweenSpec
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.repeatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.DefaultMarqueeDelayMillis
import androidx.compose.foundation.DefaultMarqueeIterations
import androidx.compose.foundation.DefaultMarqueeSpacing
import androidx.compose.foundation.DefaultMarqueeVelocity
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.MarqueeAnimationMode
import androidx.compose.foundation.MarqueeAnimationMode.Companion.Immediately
import androidx.compose.foundation.MarqueeAnimationMode.Companion.WhileFocused
import androidx.compose.foundation.MarqueeSpacing
import androidx.compose.foundation.focusable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Modifier
import androidx.compose.ui.MotionDurationScale
import androidx.compose.ui.focus.FocusEventModifierNode
import androidx.compose.ui.focus.FocusState
import androidx.compose.ui.graphics.drawscope.ContentDrawScope
import androidx.compose.ui.graphics.drawscope.clipRect
import androidx.compose.ui.graphics.drawscope.translate
import androidx.compose.ui.layout.IntrinsicMeasurable
import androidx.compose.ui.layout.IntrinsicMeasureScope
import androidx.compose.ui.layout.LayoutCoordinates
import androidx.compose.ui.layout.Measurable
import androidx.compose.ui.layout.MeasureResult
import androidx.compose.ui.layout.MeasureScope
import androidx.compose.ui.node.DrawModifierNode
import androidx.compose.ui.node.LayoutModifierNode
import androidx.compose.ui.node.ModifierNodeElement
import androidx.compose.ui.node.requireDensity
import androidx.compose.ui.node.requireLayoutDirection
import androidx.compose.ui.platform.InspectorInfo
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.constrainWidth
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.math.absoluteValue
import kotlin.math.ceil
import kotlin.math.roundToInt
import kotlin.math.sign

/**
 * Applies an animated marquee effect to the modified content if it's too wide to fit in the
 * available space. This modifier has no effect if the content fits in the max constraints. The
 * content will be measured with unbounded width.
 *
 * When the animation is running, it will restart from the initial state any time:
 *  - any of the parameters to this modifier change, or
 *  - the content or container size change.
 *
 * The animation only affects the drawing of the content, not its position. The offset returned by
 * the [LayoutCoordinates] of anything inside the marquee is undefined relative to anything outside
 * the marquee, and may not match its drawn position on screen. This modifier also does not
 * currently support content that accepts position-based input such as pointer events.
 *
 * To only animate when the composable is focused, specify [animationMode] and make the composable
 * focusable.
 *
 * This modifier does not add any visual effects aside from scrolling, but you can add your own by
 * placing modifiers before this one.
 *
 * @param iterations The number of times to repeat the animation. `Int.MAX_VALUE` will repeat
 * forever, and 0 will disable animation.
 * @param animationMode Whether the marquee should start animating [Immediately] or only
 * [WhileFocused]. In [WhileFocused] mode, the modified node or the content must be made
 * [focusable]. Note that the [initialDelayMillis] is part of the animation, so this parameter
 * determines when that initial delay starts counting down, not when the content starts to actually
 * scroll.
 * @param delayMillis The duration to wait before starting each subsequent iteration, in millis.
 * @param initialDelayMillis The duration to wait before starting the first iteration of the
 * animation, in millis. By default, there will be no initial delay if [animationMode] is
 * [WhileFocused], otherwise the initial delay will be [delayMillis].
 * @param spacing A [MarqueeSpacing] that specifies how much space to leave at the end of the
 * content before showing the beginning again.
 * @param velocity The speed of the animation in dps / second.
 * @param onMarqueeStart Callback that is executed when the marquee animation starts.
 * @param onMarqueeEnd Callback that is executed when the marquee animation ends.
 */
@ExperimentalFoundationApi
fun Modifier.myBasicMarquee(
    iterations: Int = DefaultMarqueeIterations,
    animationMode: MarqueeAnimationMode = Immediately,
    delayMillis: Int = DefaultMarqueeDelayMillis,
    initialDelayMillis: Int = if (animationMode == Immediately) delayMillis else 0,
    spacing: MarqueeSpacing = DefaultMarqueeSpacing,
    velocity: Dp = DefaultMarqueeVelocity,
    onMarqueeStart: () -> Unit = {},
    onMarqueeEnd: () -> Unit = {},
): Modifier = this then MarqueeModifierElement(
    iterations = iterations,
    animationMode = animationMode,
    delayMillis = delayMillis,
    initialDelayMillis = initialDelayMillis,
    spacing = spacing,
    velocity = velocity,
    onMarqueeStart = onMarqueeStart,
    onMarqueeEnd = onMarqueeEnd,
)

@OptIn(ExperimentalFoundationApi::class)
private data class MarqueeModifierElement constructor(
    private val iterations: Int,
    private val animationMode: MarqueeAnimationMode,
    private val delayMillis: Int,
    private val initialDelayMillis: Int,
    private val spacing: MarqueeSpacing,
    private val velocity: Dp,
    private val onMarqueeStart: () -> Unit = {},
    private val onMarqueeEnd: () -> Unit = {},
) : ModifierNodeElement<MarqueeModifierNode>() {
    override fun InspectorInfo.inspectableProperties() {
        name = "basicMarquee"
        properties["iterations"] = iterations
        properties["animationMode"] = animationMode
        properties["delayMillis"] = delayMillis
        properties["initialDelayMillis"] = initialDelayMillis
        properties["spacing"] = spacing
        properties["velocity"] = velocity
    }

    override fun create(): MarqueeModifierNode =
        MarqueeModifierNode(
            iterations = iterations,
            animationMode = animationMode,
            delayMillis = delayMillis,
            initialDelayMillis = initialDelayMillis,
            spacing = spacing,
            velocity = velocity,
            onAnimationStart = onMarqueeStart,
            onAnimationEnd = onMarqueeEnd,
        )

    override fun update(node: MarqueeModifierNode) {
        node.update(
            iterations = iterations,
            animationMode = animationMode,
            delayMillis = delayMillis,
            initialDelayMillis = initialDelayMillis,
            spacing = spacing,
            velocity = velocity,
        )
    }
}

@OptIn(ExperimentalFoundationApi::class)
private class MarqueeModifierNode constructor(
    private var iterations: Int,
    animationMode: MarqueeAnimationMode,
    private var delayMillis: Int,
    private var initialDelayMillis: Int,
    spacing: MarqueeSpacing,
    private var velocity: Dp,
    private val onAnimationStart: () -> Unit = {},
    private val onAnimationEnd: () -> Unit = {},
) : Modifier.Node(),
    LayoutModifierNode,
    DrawModifierNode,
    FocusEventModifierNode {

    private var contentWidth by mutableIntStateOf(0)
    private var containerWidth by mutableIntStateOf(0)
    private var hasFocus by mutableStateOf(false)
    var spacing: MarqueeSpacing by mutableStateOf(spacing)
    var animationMode: MarqueeAnimationMode by mutableStateOf(animationMode)

    private val offset = Animatable(0f)
    private val direction
        get() = sign(velocity.value) * when (requireLayoutDirection()) {
            LayoutDirection.Ltr -> 1
            LayoutDirection.Rtl -> -1
        }
    private val spacingPx by derivedStateOf {
        with(spacing) {
            requireDensity().calculateSpacing(contentWidth, containerWidth)
        }
    }

    override fun onAttach() {
        restartAnimation()
    }

    fun update(
        iterations: Int,
        animationMode: MarqueeAnimationMode,
        delayMillis: Int,
        initialDelayMillis: Int,
        spacing: MarqueeSpacing,
        velocity: Dp,
    ) {
        this.spacing = spacing
        this.animationMode = animationMode

        if (
            this.iterations != iterations ||
            this.delayMillis != delayMillis ||
            this.initialDelayMillis != initialDelayMillis ||
            this.velocity != velocity
        ) {
            this.iterations = iterations
            this.delayMillis = delayMillis
            this.initialDelayMillis = initialDelayMillis
            this.velocity = velocity
            restartAnimation()
        }
    }

    override fun onFocusEvent(focusState: FocusState) {
        hasFocus = focusState.hasFocus
    }

    override fun MeasureScope.measure(
        measurable: Measurable,
        constraints: Constraints
    ): MeasureResult {
        val childConstraints = constraints.copy(maxWidth = Constraints.Infinity)
        val placeable = measurable.measure(childConstraints)
        containerWidth = constraints.constrainWidth(placeable.width)
        contentWidth = placeable.width
        return layout(containerWidth, placeable.height) {
            // Placing the marquee content in a layer means we don't invalidate the parent draw
            // scope on every animation frame.
            placeable.placeWithLayer(x = (-offset.value * direction).roundToInt(), y = 0)
        }
    }

    // Override intrinsic calculations to avoid setting state (see b/278729564).

    /** Always returns zero since the marquee has no minimum width. */
    override fun IntrinsicMeasureScope.minIntrinsicWidth(
        measurable: IntrinsicMeasurable,
        height: Int
    ): Int = 0

    override fun IntrinsicMeasureScope.maxIntrinsicWidth(
        measurable: IntrinsicMeasurable,
        height: Int
    ): Int = measurable.maxIntrinsicWidth(height)

    /** Ignores width since marquee contents are always measured with infinite width. */
    override fun IntrinsicMeasureScope.minIntrinsicHeight(
        measurable: IntrinsicMeasurable,
        width: Int
    ): Int = measurable.minIntrinsicHeight(Constraints.Infinity)

    /** Ignores width since marquee contents are always measured with infinite width. */
    override fun IntrinsicMeasureScope.maxIntrinsicHeight(
        measurable: IntrinsicMeasurable,
        width: Int
    ): Int = measurable.maxIntrinsicHeight(Constraints.Infinity)

    override fun ContentDrawScope.draw() {
        val clipOffset = offset.value * direction
        val firstCopyVisible = when (direction) {
            1f -> offset.value < contentWidth
            else -> offset.value < containerWidth
        }
        val secondCopyVisible = when (direction) {
            1f -> offset.value > (contentWidth + spacingPx) - containerWidth
            else -> offset.value > spacingPx
        }
        val secondCopyOffset = when (direction) {
            1f -> contentWidth + spacingPx
            else -> -contentWidth - spacingPx
        }.toFloat()

        clipRect(left = clipOffset, right = clipOffset + containerWidth) {
            if (firstCopyVisible) {
                this@draw.drawContent()
            }
            if (secondCopyVisible) {
                translate(left = secondCopyOffset) {
                    this@draw.drawContent()
                }
            }
        }
    }

    private fun restartAnimation() {
        if (isAttached) {
            coroutineScope.launch {
                runAnimation()
            }
        }
    }

    @OptIn(ExperimentalFoundationApi::class)
    private suspend fun runAnimation() {
        if (iterations <= 0) {
            // No animation.
            return
        }
        onAnimationStart()

        // Marquee animations should not be affected by motion accessibility settings.
        // Wrap the entire flow instead of just the animation calls so kotlin doesn't have to create
        // an extra CoroutineContext every time the flow emits.
        withContext(FixedMotionDurationScale) {
            snapshotFlow {
                // Don't animate if content fits. (Because coroutines, the int will get boxed
                // anyway.)
                if (contentWidth <= containerWidth) return@snapshotFlow null
                if (animationMode == WhileFocused && !hasFocus) return@snapshotFlow null
                (contentWidth + spacingPx).toFloat()
            }.collectLatest { contentWithSpacingWidth ->
                // Don't animate when the content fits.
                if (contentWithSpacingWidth == null) return@collectLatest

                val spec = createMarqueeAnimationSpec(
                    iterations,
                    contentWithSpacingWidth,
                    initialDelayMillis,
                    delayMillis,
                    velocity,
                    requireDensity()
                )

                offset.snapTo(0f)
                try {
                    offset.animateTo(contentWithSpacingWidth, spec)
                } finally {
                    onAnimationEnd()
                    offset.snapTo(0f)
                }
            }
        }
    }
}

private fun createMarqueeAnimationSpec(
    iterations: Int,
    targetValue: Float,
    initialDelayMillis: Int,
    delayMillis: Int,
    velocity: Dp,
    density: Density
): AnimationSpec<Float> {
    val pxPerSec = with(density) { velocity.toPx() }
    val singleSpec = velocityBasedTween(
        velocity = pxPerSec.absoluteValue,
        targetValue = targetValue,
        delayMillis = delayMillis
    )
    // Need to cancel out the non-initial delay.
    val startOffset = StartOffset(-delayMillis + initialDelayMillis)
    return if (iterations == Int.MAX_VALUE) {
        infiniteRepeatable(singleSpec, initialStartOffset = startOffset)
    } else {
        repeatable(iterations, singleSpec, initialStartOffset = startOffset)
    }
}

private fun velocityBasedTween(
    velocity: Float,
    targetValue: Float,
    delayMillis: Int
): TweenSpec<Float> {
    val pxPerMilli = velocity / 1000f
    return tween(
        durationMillis = ceil(targetValue / pxPerMilli).toInt(),
        easing = LinearEasing,
        delayMillis = delayMillis
    )
}

private object FixedMotionDurationScale : MotionDurationScale {
    override val scaleFactor: Float
        get() = 1f
}