package com.example.canvas.composable

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.DefaultMarqueeDelayMillis
import androidx.compose.foundation.DefaultMarqueeIterations
import androidx.compose.foundation.DefaultMarqueeSpacing
import androidx.compose.foundation.DefaultMarqueeVelocity
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.MarqueeAnimationMode
import androidx.compose.foundation.MarqueeAnimationMode.Companion.Immediately
import androidx.compose.foundation.MarqueeAnimationMode.Companion.WhileFocused
import androidx.compose.foundation.MarqueeSpacing
import androidx.compose.foundation.focusable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.CompositingStrategy
import androidx.compose.ui.graphics.drawscope.ContentDrawScope
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.takeOrElse
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.Paragraph
import androidx.compose.ui.text.TextLayoutResult
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.rememberTextMeasurer
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import com.example.canvas.composable.modifier.clickableWithNoIndication
import com.example.canvas.composable.modifier.myBasicMarquee

private const val MILLIS_600: Int = 600
private val fadedEdgeWidth = 8.dp

/**
 * Text element with marquee effect functionality.
 *
 * If Text element can fit in its available space without overflowing, it will be a regular [Text].
 * But if it overflows, then a custom Text with marquee effect will be used instead.
 *
 * The implementation of custom Text with marquee is based on current Experimental implementation by
 * Compose. But it adds [onMarqueeStart] and [onMarqueeEnd] callbacks for more controlled marquee.
 *
 * It also changes marquee logic making it more on-demand. Instead of set number of animation iterations,
 * it uses [initialIterations] to set the number of iterations to run as the element enters the
 * composition. But provides an under the hood implementation to run marquee again by clicking on the
 * Text element, after animation finishes.
 *
 *
 * @param text the text to be displayed.
 * @param modifier the [Modifier] to be applied to this layout node.
 * @param color [Color] to apply to the text. If [Color.Unspecified], and [style] has no color set,
 * this will be [LocalContentColor].
 * @param fontSize the size of glyphs to use when painting the text. See [TextStyle.fontSize].
 * @param fontStyle the typeface variant to use when drawing the letters (e.g., italic).
 * See [TextStyle.fontStyle].
 * @param fontWeight the typeface thickness to use when painting the text (e.g., [FontWeight.Bold]).
 * @param fontFamily the font family to be used when rendering the text. See [TextStyle.fontFamily].
 * @param letterSpacing the amount of space to add between each letter.
 * See [TextStyle.letterSpacing].
 * @param textDecoration the decorations to paint on the text (e.g., an underline).
 * See [TextStyle.textDecoration].
 * @param textAlign the alignment of the text within the lines of the paragraph.
 * See [TextStyle.textAlign].
 * @param lineHeight line height for the [Paragraph] in [TextUnit] unit, e.g. SP or EM.
 * See [TextStyle.lineHeight].
 * @param overflow how visual overflow should be handled.
 * @param softWrap whether the text should break at soft line breaks. If false, the glyphs in the
 * text will be positioned as if there was unlimited horizontal space. If [softWrap] is false,
 * [overflow] and TextAlign may have unexpected effects.
 * @param maxLines An optional maximum number of lines for the text to span, wrapping if
 * necessary. If the text exceeds the given number of lines, it will be truncated according to
 * [overflow] and [softWrap]. It is required that 1 <= [minLines] <= [maxLines].
 * @param minLines The minimum height in terms of minimum number of visible lines. It is required
 * that 1 <= [minLines] <= [maxLines].
 * @param onTextLayout callback that is executed when a new text layout is calculated. A
 * [TextLayoutResult] object that callback provides contains paragraph information, size of the
 * text, baselines and other details. The callback can be used to add additional decoration or
 * functionality to the text. For example, to draw selection around the text.
 * @param style style configuration for the text such as color, font, line height etc.
 * @param initialIterations the number of times to repeat the animation as the element enters the
 * composition. `Int.MAX_VALUE` will repeat forever, and 0 will disable animation.
 * @param animationMode whether the marquee should start animating [Immediately] or only
 * [WhileFocused]. In [WhileFocused] mode, the modified node or the content must be made
 * [focusable]. Note that the [initialDelayMillis] is part of the animation, so this parameter
 * determines when that initial delay starts counting down, not when the content starts to actually
 * scroll.
 * @param delayMillis the duration to wait before starting each subsequent iteration, in millis.
 * @param initialDelayMillis the duration to wait before starting the first iteration of the
 * animation, in millis. By default, there will be no initial delay if [animationMode] is
 * [WhileFocused], otherwise the initial delay will be [delayMillis].
 * @param spacing a [MarqueeSpacing] that specifies how much space to leave at the end of the
 * content before showing the beginning again.
 * @param velocity the speed of the animation in dps / second.
 * @param onMarqueeStart callback that is executed when the marquee animation starts.
 * @param onMarqueeEnd callback that is executed when the marquee animation ends.
 */
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun MarqueeText(
    text: String,
    modifier: Modifier = Modifier,
    color: Color = Color.Unspecified,
    fontSize: TextUnit = TextUnit.Unspecified,
    fontStyle: FontStyle? = null,
    fontWeight: FontWeight? = null,
    fontFamily: FontFamily? = null,
    letterSpacing: TextUnit = TextUnit.Unspecified,
    textDecoration: TextDecoration? = null,
    textAlign: TextAlign? = null,
    lineHeight: TextUnit = TextUnit.Unspecified,
    overflow: TextOverflow = TextOverflow.Clip,
    softWrap: Boolean = true,
    maxLines: Int = Int.MAX_VALUE,
    minLines: Int = 1,
    onTextLayout: (TextLayoutResult) -> Unit = {},
    style: TextStyle = LocalTextStyle.current,
    initialIterations: Int = 0,
    animationMode: MarqueeAnimationMode = Immediately,
    delayMillis: Int = DefaultMarqueeDelayMillis,
    initialDelayMillis: Int = if (animationMode == Immediately) delayMillis else 0,
    spacing: MarqueeSpacing = DefaultMarqueeSpacing,
    velocity: Dp = DefaultMarqueeVelocity,
    onMarqueeStart: () -> Unit = {},
    onMarqueeEnd: () -> Unit = {},
) {
    val density = LocalDensity.current
    val textMeasurer = rememberTextMeasurer()

    val textColor = color.takeOrElse {
        style.color.takeOrElse {
            LocalContentColor.current
        }
    }

    val mergedStyle = style.merge(
        TextStyle(
            color = textColor,
            fontSize = fontSize,
            fontWeight = fontWeight,
            textAlign = textAlign,
            lineHeight = lineHeight,
            fontFamily = fontFamily,
            textDecoration = textDecoration,
            fontStyle = fontStyle,
            letterSpacing = letterSpacing
        )
    )

    BoxWithConstraints(modifier = modifier) {
        val didOverflow = remember(text) {
            textMeasurer.measure(
                text = text,
                style = mergedStyle,
                overflow = overflow,
                maxLines = maxLines,
                density = density,
                constraints = constraints
            ).hasVisualOverflow
        }

        if (didOverflow) {
            TextWithMarquee(
                text = text,
                overflow = overflow,
                softWrap = softWrap,
                maxLines = maxLines,
                minLines = minLines,
                onTextLayout = onTextLayout,
                style = mergedStyle,
                iterations = initialIterations,
                animationMode = animationMode,
                delayMillis = delayMillis,
                initialDelayMillis = initialDelayMillis,
                spacing = spacing,
                velocity = velocity,
                onMarqueeStart = onMarqueeStart,
                onMarqueeEnd = onMarqueeEnd,
            )
        } else {
            Text(
                text = text,
                overflow = overflow,
                softWrap = softWrap,
                maxLines = maxLines,
                minLines = minLines,
                onTextLayout = onTextLayout,
                style = mergedStyle,
            )
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun TextWithMarquee(
    text: String,
    modifier: Modifier = Modifier,
    overflow: TextOverflow = TextOverflow.Clip,
    softWrap: Boolean = true,
    maxLines: Int = Int.MAX_VALUE,
    minLines: Int = 1,
    onTextLayout: (TextLayoutResult) -> Unit = {},
    style: TextStyle = LocalTextStyle.current,
    iterations: Int = DefaultMarqueeIterations,
    animationMode: MarqueeAnimationMode = Immediately,
    delayMillis: Int = DefaultMarqueeDelayMillis,
    initialDelayMillis: Int = if (animationMode == Immediately) delayMillis else 0,
    spacing: MarqueeSpacing = DefaultMarqueeSpacing,
    velocity: Dp = DefaultMarqueeVelocity,
    onMarqueeStart: () -> Unit = {},
    onMarqueeEnd: () -> Unit = {},
) {
    // Changing iteration value in myBasicMarquee makes the animation to rerun.
    var repeatMarquee by remember {
        mutableIntStateOf(iterations)
    }

    Box(
        modifier = modifier
            // Rendering to an offscreen buffer is required to get the faded edges' alpha to be
            // applied only to the text, and not whatever is drawn below this composable (e.g. the
            // window).
            .graphicsLayer { compositingStrategy = CompositingStrategy.Offscreen }
            .drawFadedEdges(edge = if (repeatMarquee > 0) FadedEdge.BOTH else FadedEdge.END),
    ) {
        Text(
            text = text,
            modifier = Modifier
                .myBasicMarquee(
                    iterations = repeatMarquee,
                    animationMode = animationMode,
                    delayMillis = delayMillis,
                    initialDelayMillis = initialDelayMillis,
                    spacing = spacing,
                    velocity = velocity,
                    onMarqueeStart = onMarqueeStart,
                    onMarqueeEnd = {
                        // Resetting Marquee to allow animation to run again.
                        repeatMarquee = 0
                        onMarqueeEnd()
                    }
                )
                .clickableWithNoIndication {
                    repeatMarquee = 1
                },
            overflow = overflow,
            softWrap = softWrap,
            maxLines = maxLines,
            minLines = minLines,
            onTextLayout = onTextLayout,
            style = style,
        )
    }
}

/**
 * Draw faded edges.
 *
 * Fading is achieved by creating a gradient "masking layer" and blending it using [BlendMode.DstOut].
 * In short this blending mode will REDUCE the opacity of the destination layer by the opacity value
 * of the making layer ([Color.Black] will make the destination fully transparent, while [Color.Transparent]
 * will have no effect whatsoever.)
 *
 * @param startEdge differentiates between start and end edge.
 * @param alpha defines the opacity of the fadedEdge. Alpha of `1f` means that the faded edge is VISIBLE
 * and the content will be FADED, while alpha of `0f` means that the faded edge is INVISIBLE and will
 * not affect the content.
 */
private fun ContentDrawScope.drawFadedEdge(startEdge: Boolean, alpha: Float = 1f) {
    val edgeWidthPx = fadedEdgeWidth.toPx()

    drawRect(
        topLeft = Offset(if (startEdge) 0f else size.width - edgeWidthPx, 0f),
        size = Size(edgeWidthPx, size.height),
        brush = Brush.horizontalGradient(
            colors = listOf(Color.Black.copy(alpha = alpha), Color.Transparent),
            startX = if (startEdge) 0f else size.width,
            endX = if (startEdge) edgeWidthPx else size.width - edgeWidthPx
        ),
        blendMode = BlendMode.DstOut
    )
}

/**
 * Draw horizontal edges in the parent composable that fade the content of the composable.
 *
 * Implementation treats [FadedEdge.START] and [FadedEdge.END] differently. While [FadedEdge.END] is
 * displayed instantly, the appearance and disappearance of [FadedEdge.START] is animated (fadeIn,
 * fadeOut). For animation to take place, [edge] parameter has to CHANGE after the modifier was applied
 * to the composable.
 *
 * @param edge defines the position of the edges.
 */
fun Modifier.drawFadedEdges(
    edge: FadedEdge = FadedEdge.END
): Modifier = composed {
    // Alpha value will make start edge be completely visible or invisible depending on edge parameter.
    // After the edge changes, alpha value will be gradually "animated" to its new value.
    val alpha by animateFloatAsState(
        targetValue = if (edge.contains(FadedEdge.START)) 1f else 0f,
        label = "Faded Edges Alpha",
        animationSpec = tween(MILLIS_600),
    )

    drawWithContent {
        drawContent()
        drawFadedEdge(startEdge = true, alpha = alpha)
        if (edge.contains(FadedEdge.END)) drawFadedEdge(startEdge = false)
    }
}

enum class FadedEdge {
    NONE, START, END, BOTH;

    fun contains(edge: FadedEdge): Boolean {
        return when (edge) {
            NONE -> this == NONE
            START -> this == START || this == BOTH
            END -> this == END || this == BOTH
            BOTH -> this == BOTH
        }
    }
}