package com.example.canvas.composable

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.onPlaced
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.canvas.R

@Composable
fun DropDownText() {
    val density = LocalDensity.current

    val states = listOf(
        "User",
        "Administrator",
    )
    var selectedOption by remember {
        mutableIntStateOf(0)
    }

    var isEnabled by remember {
        mutableStateOf(false)
    }

    var parentWidth by remember {
        mutableIntStateOf(0)
    }

    var offsetX by remember {
        mutableStateOf(0.dp)
    }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .onPlaced {
                parentWidth = it.size.width
            }
            .clickable {
                isEnabled = !isEnabled
            },
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Text(
            style = LocalTextStyle.current.copy(
                fontSize = 18.sp,
                fontWeight = FontWeight.SemiBold,
            ),
            text = "Role: "
        )

        Row(
            horizontalArrangement = Arrangement.End,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Text(text = states[selectedOption])
            Icon(
                painter = painterResource(id = R.drawable.ic_arrow_down_24),
                contentDescription = null
            )
        }
    }

    DropdownMenu(
        modifier = Modifier.onPlaced {
            val popUpWidthPx = parentWidth - it.size.width

            offsetX = with(density) {
                popUpWidthPx.toDp()
            }
        },
        offset = DpOffset(offsetX, 0.dp),
        expanded = isEnabled,
        onDismissRequest = { isEnabled = false }
    ) {
        states.forEachIndexed { index, state ->
            DropdownMenuItem(
                text = {
                    Text(text = state)
                },
                onClick = {
                    selectedOption = index
                    isEnabled = false
                }
            )
        }
    }
}