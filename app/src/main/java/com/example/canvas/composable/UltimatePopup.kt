package com.example.canvas.composable

import androidx.compose.animation.core.MutableTransitionState
import androidx.compose.animation.core.Transition
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.tween
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.asPaddingValues
import androidx.compose.foundation.layout.statusBars
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.BiasAlignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.GraphicsLayerScope
import androidx.compose.ui.graphics.TransformOrigin
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.IntRect
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Popup
import androidx.compose.ui.window.PopupPositionProvider
import androidx.compose.ui.window.PopupProperties
import kotlin.math.roundToInt

/**
 * Custom [Popup] with animation.
 *
 * Popup changes its positioning depending on the available space, always trying to be
 * fully visible. It will try to align to its parent taking the available space and [alignment] into
 * consideration. [Alignment.TopStart] will try to align TopStart of the popup to the BottomStart of the
 * parent and vice versa. [Alignment.Center] centers popup on top of the parent.
 *
 * [offset] also applies relative to the parent. It means that horizontal offset applies only when
 * [alignment] is [Alignment.CenterStart] (will be subtracted) or [Alignment.CenterEnd] (will be added).
 * Vertical offset is applied for all Top and Bottom alignment to the direction from the parent.
 *
 *
 * @param expanded whether the menu is expanded or not
 * @param onDismissRequest Executes when the user clicks outside of the popup
 * @param alignment The alignment relative to the parent
 * @param offset An offset from the original aligned position of the popup. Offset respects the
 * Ltr/Rtl context and [alignment]
 * @param minimalMargin the minimal horizontal/vertical distance from the screen corner to the
 * popup
 * @param properties [PopupProperties] for further customization of this popup's behavior
 * @param animationDuration duration of the expansion animation in milliseconds
 * @param animationDirection direction of the expansion animation. [Alignment.TopStart] will animate
 * the popup from TopStart to the BottomEnd and so on.
 * @param content The content to be displayed inside the popup.
 */
@Composable
fun UltimatePopup(
    expanded: Boolean,
    onDismissRequest: () -> Unit,
    modifier: Modifier = Modifier,
    alignment: Alignment = Alignment.TopStart,
    offset: DpOffset = DpOffset(0.dp, 0.dp),
    minimalMargin: Dp = 0.dp,
    properties: PopupProperties = PopupProperties(focusable = true),
    animationDuration: Int = 220,
    animationDirection: Alignment = Alignment.TopStart,
    content: @Composable BoxScope.() -> Unit
) {
    val density = LocalDensity.current
    val statusBarHeight = WindowInsets.statusBars.asPaddingValues().calculateTopPadding()
    val expandedState = remember { MutableTransitionState(false) }
    expandedState.targetState = expanded

    val popupPositionProvider = remember(alignment, offset) {
        AlignmentOffsetPositionProvider(
            alignment = alignment,
            offset = offset,
            statusBarHeight = statusBarHeight,
            minimalMargin = minimalMargin,
            density = density,
        )
    }

    if (expandedState.currentState || expandedState.targetState || !expandedState.isIdle) {
        Popup(
            popupPositionProvider = popupPositionProvider,
            onDismissRequest = onDismissRequest,
            properties = properties,
        ) {
            UltimatePopupContent(
                expandedStates = expandedState,
                modifier = modifier,
                animationDurationMillis = animationDuration,
                animationDirection = animationDirection,
                content = content
            )
        }
    }
}

@Composable
fun UltimatePopupContent(
    expandedStates: MutableTransitionState<Boolean>,
    modifier: Modifier = Modifier,
    animationDurationMillis: Int = 220,

    animationDirection: Alignment = Alignment.TopStart,
    content: @Composable BoxScope.() -> Unit
) {
    // Menu open/close animation.
    val transition = updateTransition(expandedStates, "Popup")
    val transitionAnimation = defaultTransitionAnimation(
        transition,
        animationDurationMillis,
        animationDirection
    )

    Box(
        modifier = Modifier
            .graphicsLayer {
                transitionAnimation()
            }
    ) {
        Box(
            modifier = modifier.width(IntrinsicSize.Max),
            content = content
        )
    }
}

/**
 * ExpandIn/shrinkOut with default TransformOrigin (TopStart) and fadeIn/fadeOut.
 */
@Composable
private fun defaultTransitionAnimation(
    transition: Transition<Boolean>,
    animationDurationMillis: Int = 220,
    animationDirection: Alignment = Alignment.TopStart,
): GraphicsLayerScope.() -> Unit {
    // Scale animation.
    val scale by transition.animateFloat(
        transitionSpec = {
            if (false isTransitioningTo true) {
                // Dismissed to expanded.
                tween(animationDurationMillis)
            } else {
                // Expanded to dismissed.
                tween(animationDurationMillis)
            }
        },
        label = "Popup Scale"
    ) {
        if (it) {
            // Popup is expanded.
            1f
        } else {
            // Popup is dismissed.
            0f
        }
    }

    // Alpha animation.
    val alpha by transition.animateFloat(
        transitionSpec = {
            if (false isTransitioningTo true) {
                // Dismissed to expanded.
                tween(animationDurationMillis)
            } else {
                // Expanded to dismissed.
                tween(animationDurationMillis)
            }
        },
        label = "Popup Alpha"
    ) {
        if (it) {
            // Popup is expanded.
            1f
        } else {
            // Popup is dismissed.
            0f
        }
    }

    // Helper function for applying animations to graphics layer.
    return fun GraphicsLayerScope.() {
        scaleX = scale
        scaleY = scale
        this.alpha = alpha
        this.transformOrigin = animationDirection.toTransformOrigin()
    }
}

private class AlignmentOffsetPositionProvider(
    val alignment: Alignment,
    val offset: DpOffset,
    val statusBarHeight: Dp,
    val minimalMargin: Dp,
    val density: Density,
) : PopupPositionProvider {
    override fun calculatePosition(
        anchorBounds: IntRect,
        windowSize: IntSize,
        layoutDirection: LayoutDirection,
        popupContentSize: IntSize
    ): IntOffset {
        // TODO: Decide which is the best way to round to result without reimplementing Alignment.align
        var popupPosition = IntOffset(0, 0)

        //todo: Version1. Trying to get horizontal/vertical bias values from Alignment
//        val biasAlignment = (alignment as? BiasAlignment) ?: BiasAlignment(0f, 0f)

        //todo Version2. Align so that offset doesn't apply unless to prevent covering anchor (covers in Center)
        val biasAlignment = with(alignment.toBiasAlignment()) {
            BiasAlignment(
                horizontalBias = if (verticalBias != 0f) 0f else horizontalBias,
                verticalBias = verticalBias,
            )
        }

        //todo Version3. Ver2 but also correct offset for when there is not enough space left?
//        val biasAlignment = with(alignment as? BiasAlignment ?: BiasAlignment(0f, 0f)) {
//            BiasAlignment(
//                horizontalBias = if (verticalBias != 0f) 0f else horizontalBias,
//                verticalBias = verticalBias,
//            )
//        }

        //todo myParentOffset will attempt to place item on top/bottom, right/left from parent (anchor)
        // account for positioning. If not enough place. change alignment?
        val alignmentOffset = IntOffset(
            x = (anchorBounds.width * (biasAlignment.horizontalBias)).roundToInt(),
            y = (anchorBounds.height * (biasAlignment.verticalBias)).roundToInt()
        )

        // Get the aligned point inside the parent
        val parentAlignmentPoint = alignment.align(
            IntSize.Zero,
            anchorBounds.size,
            layoutDirection
        )
        // Get the aligned point inside the child
        val relativePopupPos = alignment.align(
            IntSize.Zero,
            popupContentSize,
            layoutDirection
        )

        // Add the position of the parent
        popupPosition += IntOffset(anchorBounds.left, anchorBounds.top)

        // Add the distance between the parent's top left corner and the alignment point
        popupPosition += parentAlignmentPoint

        // Subtract the distance between the children's top left corner and the alignment point
        popupPosition -= relativePopupPos

        // Apply the offset to align popup position relative to the parent
        popupPosition -= alignmentOffset

        // The content offset relative to parent specified using the offset parameter.
        // Is affected by alignment.
        val contentOffset = with(density) {
            IntOffset(
                x = (offset.x * (-biasAlignment.horizontalBias)).roundToPx(),
                y = (offset.y * (-biasAlignment.verticalBias)).roundToPx(),
            )
        }

        // Add the user offset
        val resolvedOffset = IntOffset(
            contentOffset.x * (if (layoutDirection == LayoutDirection.Ltr) 1 else -1),
            contentOffset.y
        )
        popupPosition += resolvedOffset

        return with(density) {
            popupPosition.withMinimalMargin(
                margin = minimalMargin.roundToPx(),
                contentSize = popupContentSize,
                windowSize = windowSize,
                statusBarHeight = statusBarHeight.roundToPx(),
            )
        }
    }
}

/**
 * Adds minimal margin between the popup and the corner of the screen.
 *
 * Important:
 * 1. [windowSize] is the size of the working area (without status bar). But the offset will be applied
 * relative to the whole screen (including status bar). Adding [statusBarHeight] to the y coordinate
 * aligns the y offset origin to the bottom of the status bar.
 */
private fun IntOffset.withMinimalMargin(
    margin: Int,
    contentSize: IntSize,
    windowSize: IntSize,
    statusBarHeight: Int,
): IntOffset {
    // Leftmost x coordinate with margin.
    val minStart = margin
    // Rightmost x coordinate with margin.
    val maxEnd = windowSize.width - margin - contentSize.width
    // Uppermost y coordinate with margin.
    val minTop = margin + statusBarHeight
    // Bottom-most y coordinate with margin.
    val maxBottom = windowSize.height + statusBarHeight - margin - contentSize.height

    return IntOffset(
        x = x.coerceIn(minStart, maxEnd),
        y = y.coerceIn(minTop, maxBottom)
    )
}

/**
 * Converts Alignment to BiasAlignment.
 */
private fun Alignment.toBiasAlignment(): BiasAlignment {
    return this as? BiasAlignment ?: BiasAlignment(horizontalBias = 0f, verticalBias = 0f)
}

/**
 * Converts [Alignment] to [TransformOrigin].
 *
 * The main difference between them is that Alignment uses biases -1 for start/top, 0 for center and
 * 1 will for end/bottom while TransformOrigin uses pivotFraction of 0 for start/top, 0.5 for center
 * and 1 for end/bottom.
 */
private fun Alignment.toTransformOrigin(): TransformOrigin {
    return with(this.toBiasAlignment()) {
        TransformOrigin(
            pivotFractionX = 0.5f * horizontalBias + 0.5f,
            pivotFractionY = 0.5f * verticalBias + 0.5f,
        )
    }
}