package com.example.canvas.composable.text_test

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material3.Button
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.TextMeasurer
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.rememberTextMeasurer
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize

/**
 * Description:
 *
 * - Buttons scale font size by 11% on click.
 * - Character 't' is used as unit of measure, that is scaled with font size. Text should wrap after 11 't' character long.
 *
 * - 1 (Green): Calculates its size using measureSizeDp with the size of 11 't' as max width constraint. Uses size modifier.
 * - 2 (Blue): Uses some fixed max width in dp (equal to exactly the size of initial text). Buttons scale both font size and that max width by the same factor. Uses widthIn modifier with max value.
 * - 3 (Magenta): Max width always equals to the width of first 6 chars with current font size. Uses widthIn modifier with max value.
 */

@Composable
fun TextScreen1() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(24.dp)
    ) {
        // HEADER
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally),
            text = "Screen 1"
        )

        Content()
    }
}

@OptIn(ExperimentalTextApi::class)
@Composable
private fun Content() {
    val density = LocalDensity.current
    val textMeasurer = rememberTextMeasurer()
    val defaultTextStyle = LocalTextStyle.current
    var localTextStyle by remember {
        mutableStateOf(defaultTextStyle)
    }

    val textToMeasure = "Teeeeeeeeee"
    val scaleFactor = 1.11f
    ////////////////////////////////////////////////////////////////////////////////
    // V1 Increase Font size by a factor, calculate size in # of chars in that font size
    ///////////////////////////////////////////////////////////////////////////////
    val textMax = "t".repeat(11)

    val maxSize = remember(localTextStyle.fontSize) {
        with(density) {
            measureSizeDp(
                measurer = textMeasurer,
                text = textMax,
                style = localTextStyle,
            )
        }
    }

    val currentTextSize by remember(maxSize, localTextStyle) {
        with(density) {
            mutableStateOf(
                measureSizeDp(
                    measurer = textMeasurer,
                    text = textToMeasure,
                    style = localTextStyle,
                    constraints = Constraints(maxWidth = with(density) { maxSize.width.roundToPx() })
                )
            )
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    // V2 Use predetermined max width. When scaling, scale maxWidth by that same factor.
    ///////////////////////////////////////////////////////////////////////////////
    var autoMaxWidth by remember {
        mutableStateOf(62.18182.dp)
    }


    ////////////////////////////////////////////////////////////////////////////////
    // V3 Set max width to be as large as first # of chars of this font size.
    ///////////////////////////////////////////////////////////////////////////////
    val altMaxWidth = remember(localTextStyle) {
        with(density) {
            measureSizeDp(
                measurer = textMeasurer,
                text = textToMeasure.substring(0, 6),
                style = localTextStyle,
            ).width
        }
    }

    Column(
        Modifier
            .fillMaxSize()
    ) {
        Row {
            Button(
                onClick = {
                    // Scaling font by 30%
                    val newFontSize = localTextStyle.fontSize.times(scaleFactor)
                    // todo crucial to scale line height as well as fontSize
                    val newLineHeight = localTextStyle.lineHeight.times(scaleFactor)

                    localTextStyle =
                        localTextStyle.copy(fontSize = newFontSize, lineHeight = newLineHeight)

                    // V2
                    autoMaxWidth *= scaleFactor
                }
            ) {
                Text("+")
            }

            Button(
                onClick = {
                    // Scaling font by 30%
                    val newFontSize = localTextStyle.fontSize.div(scaleFactor)
                    // todo crucial to scale line height as well as fontSize
                    val newLineHeight = localTextStyle.lineHeight.div(scaleFactor)

                    localTextStyle =
                        localTextStyle.copy(fontSize = newFontSize, lineHeight = newLineHeight)

                    autoMaxWidth /= scaleFactor
                }
            ) {
                Text("-")
            }
        }

        // Maximum size is 11 't' characters in current font size.
        Text(
            modifier = Modifier
                .background(Color.Red.copy(alpha = 0.1f)),
            text = textMax,
            style = localTextStyle
        )

        Text(
            modifier = Modifier
                .size(currentTextSize) // V1 (# of char in this font size)
                .background(Color.Green.copy(alpha = 0.1f)),
            text = textToMeasure,
            style = localTextStyle
        )
        Text(
            modifier = Modifier
                .widthIn(max = autoMaxWidth) // V2 (fixed dp that is scaled with factor)
                .background(Color.Blue.copy(alpha = 0.1f)),
            text = textToMeasure,
            style = localTextStyle
        )
        Text(
            modifier = Modifier
                .widthIn(max = altMaxWidth) // V3 (fixed # of chars)
                .background(Color.Magenta.copy(alpha = 0.1f)),
            text = textToMeasure,
            style = localTextStyle
        )
    }
}

@OptIn(ExperimentalTextApi::class)
private fun Density.measureSizeDp(
    measurer: TextMeasurer,
    text: String = "",
    style: TextStyle = TextStyle.Default,
    constraints: Constraints = Constraints(),
): DpSize {
    val innerText = AnnotatedString(text = text)
    val textSize = measurer.measure(
        text = innerText,
        style = style,
        density = this,
        softWrap = true,
        constraints = constraints
    ).size.toSize()

    return textSize.toDpSize()
}