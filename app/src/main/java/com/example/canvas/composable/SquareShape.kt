package com.example.canvas.composable

import androidx.compose.runtime.Composable
import androidx.compose.ui.geometry.Offset
import com.example.canvas.data.BoardItemModel


@Composable
fun SquareShape(
    data: BoardItemModel,
    onSelect: (Int) -> Unit = { },
    onEditMode: (Int) -> Unit = { },
    onMove: (Int, Offset) -> Unit = { _, _ -> }
) {
    BasicShape(
        data = data,
        onSelect = {
            onSelect(data.id)
        },
        onEditMode = {
            onEditMode(data.id)
        },
        onMove = onMove,
        backgroundCanvas = {
            drawRect(
                color = data.color!!,
                size = size
            )
        },
        borderCanvas = {
            drawBorder()
        }
    )
}