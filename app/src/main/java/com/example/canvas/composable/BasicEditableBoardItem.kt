package com.example.canvas.composable

import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import kotlin.math.roundToInt

@Composable
fun BasicEditableBoardItem(
    position: Offset,
    size: Size,
    isSelected: Boolean = false,
    isEditMode: Boolean = false,
    onSelect: () -> Unit = { }, // todo onSelectChange
    onEditMode: () -> Unit = { }, // todo onEditModeChange
    onMove: (Offset) -> Unit = { },
    focusRequester: FocusRequester = FocusRequester(),
    backgroundCanvas: DrawScope.() -> Unit = { },
    borderCanvas: DrawScope.() -> Unit = { },
    content: @Composable BoxScope.() -> Unit = { }
) {
    // To check, when IME keyboard is open/closed
    val isKeyboardVisible by keyboardAsState()

    // To clear focus, when closing keyboard
    val focusManager = LocalFocusManager.current

    LaunchedEffect(isEditMode) {
        if (isEditMode) {
            focusRequester.requestFocus()
        }
    }

    // When closing IME keyboard during editing
    LaunchedEffect(isKeyboardVisible) {
        if (isEditMode && !isKeyboardVisible) {
            onEditMode()
            focusManager.clearFocus()
        }
    }

    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .size(size.toDpSize())
            .offset { IntOffset(position.x.roundToInt(), position.y.roundToInt()) }
            .pointerInput(Unit) {
                detectDragGestures { change, dragAmount ->
                    change.consume()

                    onMove(
                        Offset(
                            x = dragAmount.x,
                            y = dragAmount.y
                        )
                    )
                }
            }
            // providing key will restart the block with updated value!!
            .pointerInput(isSelected) {
                detectTapGestures(
                    onTap = {
                        if (!isSelected) { // todo always false. Not recompose with changed?
                            onSelect()
                        } else {
                            onEditMode()
                        }
                    }
                )
            }
            .drawBehind {
                backgroundCanvas()

                if (isSelected) {
                    borderCanvas()
                }
            }
    ) {
        content()
    }
}

private fun Size.toDpSize() = DpSize(
    width = width.dp,
    height = height.dp
)