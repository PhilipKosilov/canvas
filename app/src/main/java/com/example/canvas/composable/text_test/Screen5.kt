package com.example.canvas.composable.text_test

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.TextLayoutResult
import androidx.compose.ui.text.TextMeasurer
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.rememberTextMeasurer
import androidx.compose.ui.text.style.LineBreak
import androidx.compose.ui.text.style.TextGeometricTransform
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize

/**
 * Description:
 *
 * - Things to remember:
 *
 * 1. Text scales linearly with font: increasing font increases text.
 * 2. MaxWidth should depend on font size and only on font size.
 *
 * Notes:
 * - Styles has TextGeometricTransform(scaleX = 2f) - scales only horizontally (no scaleY option)
 */

@Composable
fun TextScreen5() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(24.dp)
    ) {
        // HEADER
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally),
            text = "Screen 5"
        )

        Content()
    }
}

@OptIn(ExperimentalTextApi::class)
@Composable
private fun Content() {
    val density = LocalDensity.current
    val measurer = rememberTextMeasurer()
    val defaultTextStyle = LocalTextStyle.current
    val textToDisplay = "1234567890"
    val textToMeasure = "t".repeat(11)

    val measuredResult = with(density) {
        measureSizeDp(
            measurer = measurer,
            text = textToMeasure,
            style = defaultTextStyle,
            constraints = Constraints(
                maxWidth = with(density) { 60.dp.roundToPx() }
            )
        )
    }

    Log.i("kosilov", "1: measuredResult.size=${measuredResult.size}")
    Log.i("kosilov", "2: measuredResult.lineCount=${measuredResult.lineCount}")
    Log.i("kosilov", "3: measuredResult.didOverflowWidth=${measuredResult.didOverflowWidth}")
    Log.i("kosilov", "4: measuredResult.didOverflowHeight=${measuredResult.didOverflowHeight}")
    Log.i("kosilov", "5: measuredResult.hasVisualOverflow=${measuredResult.hasVisualOverflow}")
    Log.i("kosilov", "6: measuredResult.firstBaseline=${measuredResult.firstBaseline}")
    Log.i("kosilov", "7: measuredResult.lastBaseline=${measuredResult.lastBaseline}")
    Log.i("kosilov", "8: measuredResult.layoutInput=${measuredResult.layoutInput}")
    Log.i("kosilov", "9: measuredResult.getLineLeft=${measuredResult.getLineLeft(0)}")
    Log.i("kosilov", "10: measuredResult.getLineRight=${measuredResult.getLineRight(0)}")
    Log.i("kosilov", "11: measuredResult.multiParagraph.maxIntrinsicWidth=${measuredResult.multiParagraph.maxIntrinsicWidth}")
    Log.i("kosilov", "12: measuredResult.multiParagraph.minIntrinsicWidth=${measuredResult.multiParagraph.intrinsics.minIntrinsicWidth}")

    Text(
        modifier = Modifier.background(Color.Green.copy(alpha = 0.1f)),
        text = textToDisplay,
        style = defaultTextStyle
    )

    Text(
        modifier = Modifier
            .widthIn(max = 60.dp)
            .background(Color.Green.copy(alpha = 0.2f)),
        text = textToDisplay,
        style = defaultTextStyle
    )

    Text(
        modifier = Modifier
            .widthIn(max = 60.dp)
            .background(Color.Green.copy(alpha = 0.3f)),
        text = textToDisplay,
        style = defaultTextStyle.copy(lineBreak = LineBreak.Paragraph)
    )

    Text(
        modifier = Modifier
            .widthIn(max = 60.dp)
            .background(Color.Green.copy(alpha = 0.4f)),
        text = textToDisplay,
        style = defaultTextStyle.copy(lineBreak = LineBreak.Heading)
    )
}

@OptIn(ExperimentalTextApi::class)
private fun Density.measureSizeDp(
    measurer: TextMeasurer,
    text: String = "",
    style: TextStyle = TextStyle.Default,
    constraints: Constraints = Constraints(),
): TextLayoutResult {
    return measurer.measure(
        text = AnnotatedString(text),
        style = style,
        density = this,
        softWrap = true,
        constraints = constraints
    )
}