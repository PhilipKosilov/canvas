package com.example.canvas.composable.text_test

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

/**
 * Testing how width changes when scale is done with font itself.
 */

@Composable
fun TextScreen10() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(24.dp)
    ) {
        // HEADER
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally),
            text = "Screen 10"
        )

        Content()
    }
}

@Composable
private fun Content() {
    var text by remember {
        mutableStateOf("")
    }
    Box(

    ) {
        Row {
            Row {
                BasicTextField(
                    modifier = Modifier
//            .clickable { }
                        .background(Color.LightGray),
                    value = text,
                    onValueChange = {
                        text = it
                    }
                )
            }
        }
    }

}