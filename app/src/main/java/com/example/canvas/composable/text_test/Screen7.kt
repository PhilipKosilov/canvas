package com.example.canvas.composable.text_test

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.round
import kotlin.random.Random

/**
 * Testing lambdas capturing closure while drag.
 */

@Composable
fun TextScreen7() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(24.dp)
    ) {
        // HEADER
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally),
            text = "Screen 7"
        )

        Content()
    }
}

@Composable
private fun Content() {
    ParentComposable()
}

@Composable
private fun ParentComposable() {
    // Analog of external data.
    var data by remember {
        mutableStateOf(Offset(165f, 165f))
    }

    var resetKey by remember {
        mutableStateOf(Random.nextInt())
    }

    // Simulating change of external data.
    Button(
        modifier = Modifier.offset(150.dp),
        onClick = {
            data = Offset(165f, 165f)
            resetKey = Random.nextInt()
        }
    ) {
        Text("Reset")
    }

    ChildComposable(data = data, resetKey = resetKey) { newData ->
        data = newData
    }
}

@Composable
private fun ChildComposable(
    data: Offset,
    resetKey: Any,
    onDataChanged: (Offset) -> Unit = {}
) {
    val localPosition = remember(data) {
        Log.i("kosilov", "_____QuickTest: recalculate position to: $data")
        mutableStateOf(data)
    }

    Log.i("kosilov", "QuickTest: in compose position=$localPosition")

    Box(
        modifier = Modifier
            .offset { data.round() }
            .size(50.dp)
            .background(Color.Green)
            .pointerInput(Unit) { // 1. No reset key. localPosition is captured
//            .pointerInput(data) { // 2. Resetting lambda as soon as some data changed.
//            .pointerInput(resetKey) { // 3. Resetting lambda properly.
                detectDragGestures { change, dragAmount ->
                    change.consume()

                    // Any local calculations, not passed in data.
                    localPosition.value = localPosition.value.plus(dragAmount)
                    // Passing data back to ParentComposable.
                    onDataChanged(localPosition.value)

                    Log.i("kosilov", "QuickTest: in lambda position=$localPosition")
                }
            }
    )
}