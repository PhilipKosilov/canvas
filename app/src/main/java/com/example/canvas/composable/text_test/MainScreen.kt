package com.example.canvas.composable.text_test

import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.canvas.R

sealed class Screens(val route: String, val composable: @Composable () -> Unit) {
    object Screen1 : Screens(route = "screen1", composable = { TextScreen1() })
    object Screen2 : Screens(route = "screen2", composable = { TextScreen2() })
    object Screen3 : Screens(route = "screen3", composable = { TextScreen3() })
    object Screen4 : Screens(route = "screen4", composable = { TextScreen4() })
    object Screen5 : Screens(route = "screen5", composable = { TextScreen5() })
    object Screen6 : Screens(route = "screen6", composable = { TextScreen6() })
    object Screen7 : Screens(route = "screen7", composable = { TextScreen7() })
    object Screen8 : Screens(route = "screen8", composable = { TextScreen8() })
    object Screen9 : Screens(route = "screen9", composable = { TextScreen9() })
    object Screen10 : Screens(route = "screen10", composable = { TextScreen10() })
    object Screen11 : Screens(route = "screen11", composable = { TextScreen11() })
    object Screen12 : Screens(route = "screen12", composable = { TextScreen12() })
    object Screen13 : Screens(route = "screen13", composable = { TextScreen13() })
    object Screen14 : Screens(route = "screen14", composable = { TextScreen14() })
    object Screen15 : Screens(route = "screen15", composable = { TextScreen15() })
}

private val bottomNavigationScreens = listOf(
    Screens.Screen1,
    Screens.Screen2,
    Screens.Screen3,
    Screens.Screen4,
    Screens.Screen5,
    Screens.Screen6,
    Screens.Screen7,
    Screens.Screen8,
    Screens.Screen9,
    Screens.Screen10,
    Screens.Screen11,
    Screens.Screen12,
    Screens.Screen13,
    Screens.Screen14,
    Screens.Screen15,
)


@Composable
fun TextTest() {
    TextTestScaffold()
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun TextTestScaffold() {
    val navController = rememberNavController()

    Scaffold(
        modifier = Modifier
            // Background is white by default
            .navigationBarsPadding(),
        bottomBar = {
            NavigationBar(
                containerColor = MaterialTheme.colorScheme.background,
                tonalElevation = 0.dp
            ) {
                val navBackStackEntry by navController.currentBackStackEntryAsState()

                navBackStackEntry?.destination?.let { currentDestination ->
                    val currentDestinationIndex =
                        bottomNavigationScreens.indexOfFirst { it.route == currentDestination.route }

                    val previousDestinationIndex =
                        (currentDestinationIndex - 1 + bottomNavigationScreens.size) % bottomNavigationScreens.size
                    val nextDestinationIndex =
                        (currentDestinationIndex + 1) % bottomNavigationScreens.size


                    val previousDestination = bottomNavigationScreens[previousDestinationIndex]
                    val nextDestination = bottomNavigationScreens[nextDestinationIndex]

                    // Navigate back
                    NavigationBarItem(
                        selected = false,
                        icon = {
                            Icon(
                                painter = painterResource(id = R.drawable.arrow_back_ios_24),
                                contentDescription = null
                            )
                        },
                        onClick = {
                            navController.navigate(previousDestination.route) {
                                popUpTo(navController.graph.findStartDestination().id) {
                                    saveState = true
                                }
                                launchSingleTop = true
                                restoreState = true
                            }
                        },
                    )

                    // Navigate forward
                    NavigationBarItem(
                        selected = false,
                        icon = {
                            Icon(
                                painter = painterResource(id = R.drawable.arrow_forward_ios_24),
                                contentDescription = null
                            )
                        },
                        onClick = {
                            navController.navigate(nextDestination.route) {
                                popUpTo(navController.graph.findStartDestination().id) {
                                    saveState = true
                                }
                                launchSingleTop = true
                                restoreState = true
                            }
                        },
                    )
                }
            }
        }
    ) { innerPadding ->
        NavHost(
            navController,
            startDestination = Screens.Screen1.route,
            Modifier.padding(bottom = innerPadding.calculateBottomPadding())
        ) {
            bottomNavigationScreens.forEach { screen ->
                composable(screen.route) {
                    screen.composable()
                }
            }
        }
    }
}