package com.example.canvas.composable

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.unit.dp

// Temp selected border
fun DrawScope.drawBorder(
    topLeft: Offset = Offset(0f, 0f)
) {
    drawRect(
        color = Color.Blue, // temp
        topLeft = topLeft,
        size = size,
        style = Stroke(
            width = 1.dp.toPx()
        )
    )
}