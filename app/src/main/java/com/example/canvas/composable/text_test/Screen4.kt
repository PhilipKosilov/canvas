package com.example.canvas.composable.text_test

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.TextMeasurer
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.rememberTextMeasurer
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.unit.toSize

/**
 * Description:
 *
 * - Simple test to see how text is sized without any constraints and wrapping. Does it scale
 * linearly?
 * - Answer: NO.
 *
 * - The width difference for "1234567890":
 * - 16.sp -> 17.sp = 10.px.
 * - 17.sp -> 18.sp = 20.px.
 * - 20.sp -> 21.sp = 10.px.
 *
 * - The width difference for 11 't' chars:
 * - 16.sp -> 17.sp = 10.px.
 * - 17.sp -> 18.sp = 11.px.
 * - 20.sp -> 21.sp = 10.px.
 */

@Composable
fun TextScreen4() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(24.dp)
    ) {
        // HEADER
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally),
            text = "Screen 4"
        )

        Content()
    }
}

@OptIn(ExperimentalTextApi::class)
@Composable
private fun Content() {
    val density = LocalDensity.current
    val measurer = rememberTextMeasurer()
    val defaultTextStyle = LocalTextStyle.current
    val textStyles = listOf(
        defaultTextStyle.copy(fontSize = 16.sp),
        defaultTextStyle.copy(fontSize = 17.sp),
        defaultTextStyle.copy(fontSize = 18.sp),
        defaultTextStyle.copy(fontSize = 20.sp),
        defaultTextStyle.copy(fontSize = 21.sp),
        defaultTextStyle.copy(fontSize = 32.sp),
    )
    val textToDisplay = "1234567890"
    val textToMeasure = "t".repeat(11)

    val measuredTextSizesOfT = textStyles.map { style ->
        with(density) {
            measureSizeDp(
                measurer = measurer,
                text = textToMeasure,
                style = style
            ).toSize()
        }
    }

    textStyles.forEachIndexed { index, textStyle ->
        Text(
            modifier = Modifier
                .onSizeChanged {
                    Log.i("kosilov", "Text${index+1}(${textStyle.fontSize}): size=${it.toSize()}, measured 11t size=${measuredTextSizesOfT[index]}")
                }
                .background(Color.Green.copy(alpha = 0.1f * (index+1))),
            text = textToDisplay,
            style = textStyle
        )
    }
}

@OptIn(ExperimentalTextApi::class)
private fun Density.measureSizeDp(
    measurer: TextMeasurer,
    text: String = "",
    style: TextStyle = TextStyle.Default,
    constraints: Constraints = Constraints(),
): DpSize {
    val innerText = AnnotatedString(text = text)
    val textSize = measurer.measure(
        text = innerText,
        style = style,
        density = this,
        softWrap = true,
        constraints = constraints
    ).size.toSize()

    return textSize.toDpSize()
}