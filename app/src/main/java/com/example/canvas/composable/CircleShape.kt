package com.example.canvas.composable

import androidx.compose.runtime.Composable
import androidx.compose.ui.geometry.Offset
import com.example.canvas.data.BoardItemModel

@Composable
fun CircleShape(
    data: BoardItemModel,
    onSelect: (Int) -> Unit = { },
    onEditMode: (Int) -> Unit = { },
    onMove: (Int, Offset) -> Unit = { _, _ -> }
) {
    BasicShape(
        data = data,
        onSelect = {
            onSelect(data.id)
        },
        onEditMode = {
            onEditMode(data.id)
        },
        onMove = onMove,
        backgroundCanvas = {
            drawCircle(
                color = data.color!!,
                radius = size.maxDimension / 2f
            )
        },
        borderCanvas = {
            drawBorder()
        }
    )
}