package com.example.canvas.composable.text_test

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalFontFamilyResolver
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.Paragraph
import androidx.compose.ui.text.TextLayoutResult
import androidx.compose.ui.text.TextMeasurer
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.rememberTextMeasurer
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import com.example.canvas.handles.BasicScalableBoardItemUI
import com.example.canvas.handles.pxToDpSize

/**
 * Testing different ways of calculating text size to check if they differ from actual text size
 * calculated during composition.
 *
 * Calculate size with:
 * 1. TextMeasurer
 * 2. Paragraph
 * 3. Modifier.onSizeChanged
 */

@Composable
fun TextScreen8() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(24.dp)
    ) {
        // HEADER
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally),
            text = "Screen 8"
        )

        Content()
    }
}

@OptIn(ExperimentalTextApi::class)
@Composable
private fun Content() {
    val density = LocalDensity.current
    val measurer = rememberTextMeasurer()
    val defaultTextStyle = LocalTextStyle.current
    val textToDisplay = "1234567890"

    // DEFAULT SIZE
    val measuredResult = with(density) {
        measureSizeDp(
            measurer = measurer,
            text = textToDisplay,
            style = defaultTextStyle,
        )
    }
    val paragraphResult = calculateParagraph(
        text = textToDisplay,
        style = defaultTextStyle
    )
    val paragraphSize = Size(paragraphResult.maxIntrinsicWidth, paragraphResult.height)


    // SCALED SIZE (DIRECTLY)
    // scale factor to increase font size by.
    var scale by remember { mutableStateOf(1f) }
    val scaleTextStyle by remember(scale) {
        val style = defaultTextStyle.copy(
            fontSize = defaultTextStyle.fontSize.times(scale),
        )
        mutableStateOf(style)
    }
    val measuredScaledResult = with(density) {
        measureSizeDp(
            measurer = measurer,
            text = textToDisplay,
            style = scaleTextStyle,
        )
    }
    val fontScale by remember(scaleTextStyle.fontSize) {
        mutableStateOf(scaleTextStyle.fontSize.value / defaultTextStyle.fontSize.value)
    }


    //////////
    // BUTTONS
    //////////
    Row {
        Button(
            onClick = {
                scale += 0.11f
            }
        ) {
            Text("+")
        }

        Button(
            onClick = {
                scale -= 0.11f
            }
        ) {
            Text("-")
        }
    }

    //////////
    // TEXT
    //////////

    Text(
        modifier = Modifier
            .onSizeChanged {
                Log.i(
                    "kosilov",
                    "Content(default): measuredSize=${measuredResult.size}, paragraphSize=${paragraphSize} composeSize=${it}"
                )
            }
            .background(Color.Green.copy(alpha = 0.1f)),
        text = textToDisplay,
        style = defaultTextStyle,
    )

    /**
     * Scaling font size doesn't increase size linearly:
     *
     * Default size = 265 x 59
     * Scale font +11%:
     *
     * Result size = 285 x 66 !
     * Default*Scale = (265 x 59) * 1.11 = 294 x 65
     */
    Text(
        modifier = Modifier
            .onSizeChanged {
                Log.i(
                    "kosilov",
                    "Content(scale): measuredSize=${measuredScaledResult.size}, composeSize=${it}"
                )
            }
            .background(Color.Green.copy(alpha = 0.2f)),
        text = textToDisplay,
        style = scaleTextStyle,
    )

    // Scales LINEARLY: defaultSize * scale == composeSize after graphicsScale.
    BasicScalableBoardItemUI(
        modifier = Modifier
            .onSizeChanged {
                Log.i(
                    "kosilov",
                    "Content(graphics): measuredSize=${measuredResult.size.toSize().times(scale)}, composeSize=${it}, scale=$scale, fontScale=$fontScale"
                )
            }
            .background(Color.Green.copy(alpha = 0.3f)),
        boardItemId = "1",
        size = measuredResult.size.pxToDpSize(density),
        scale = scale
    ) {
        Text(
            text = textToDisplay,
            style = defaultTextStyle,
        )
    }
}


@OptIn(ExperimentalTextApi::class)
private fun Density.measureSizeDp(
    measurer: TextMeasurer,
    text: String = "",
    style: TextStyle = TextStyle.Default,
    constraints: Constraints = Constraints(),
): TextLayoutResult {
    return measurer.measure(
        text = AnnotatedString(text),
        style = style,
        density = this,
        softWrap = true,
        constraints = constraints
    )
}

@Composable
private fun calculateParagraph(
    text: String = "",
    style: TextStyle = TextStyle.Default,
    constraints: Constraints = Constraints(),
): Paragraph {
    return Paragraph(
        text = text,
        style = style,
        density = LocalDensity.current,
        fontFamilyResolver = LocalFontFamilyResolver.current,
        constraints = constraints,
    )
}