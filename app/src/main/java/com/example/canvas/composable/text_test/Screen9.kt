package com.example.canvas.composable.text_test

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalFontFamilyResolver
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.Paragraph
import androidx.compose.ui.text.TextLayoutResult
import androidx.compose.ui.text.TextMeasurer
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.rememberTextMeasurer
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import com.example.canvas.handles.BasicScalableBoardItemUI
import com.example.canvas.handles.pxToDpSize
import kotlin.math.roundToInt

/**
 * Testing how width changes when scale is done with font itself.
 *
 * Notice how two text start of similar width, but as we increase font size, their size starts to differ.
 * Each character is scaled differently.
 *
 * Example scaling font for texts:
 * 1. scale: 1    | lllllllpp = 141 x 59 | iiiiiiioo = 141 x 59 (equal)
 * 2. scale: 1.11 | lllllllpp = 152 x 66 | iiiiiiioo = 152 x 66 (equal)
 * 2. scale: 1.44 | lllllllpp = 189 x 85 | iiiiiiioo = 191 x 85 (not equal)
 * 2. scale: 1.66 | lllllllpp = 222 x 98 | iiiiiiioo = 224 x 98 (not equal)
 */

@Composable
fun TextScreen9() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(24.dp)
    ) {
        // HEADER
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally),
            text = "Screen 9"
        )

        Content()
    }
}

@OptIn(ExperimentalTextApi::class)
@Composable
private fun Content() {
    val density = LocalDensity.current
    val measurer = rememberTextMeasurer()
    val defaultTextStyle = LocalTextStyle.current
    val texts = listOf(
        "1234567890", "Text", "lllllllpp", "iiiiiiioo"
    )

    var scale by remember { mutableStateOf(1f) }
    val scaleTextStyle by remember(scale) {
        val style = defaultTextStyle.copy(
            fontSize = defaultTextStyle.fontSize.times(scale),
        )
        mutableStateOf(style)
    }


    //////////
    // BUTTONS
    //////////
    Row {
        Button(
            onClick = {
                scale += 0.11f
            }
        ) {
            Text("+")
        }

        Button(
            onClick = {
                scale -= 0.11f
            }
        ) {
            Text("-")
        }
    }

    //////////
    // TEXTS
    //////////
    texts.forEachIndexed { index, text ->
        val position = index + 1
        val measuredText = with(density) {
            measureSizeDp(
                measurer = measurer,
                text = text,
                style = defaultTextStyle,
            ).size.toSize()
        }

        Text(
            modifier = Modifier
                .onSizeChanged {
                    Log.i(
                        "kosilov",
                        "Content(text$position): composeSize=${it} scale=$scale, measuredText$position=$measuredText, scaled=${measuredText.times(scale)}"
                    )
                }
                .background(Color.Green.copy(alpha = position * 0.1f)),
            text = text,
            style = scaleTextStyle,
        )
    }
}


@OptIn(ExperimentalTextApi::class)
private fun Density.measureSizeDp(
    measurer: TextMeasurer,
    text: String = "",
    style: TextStyle = TextStyle.Default,
    constraints: Constraints = Constraints(),
): TextLayoutResult {
    return measurer.measure(
        text = AnnotatedString(text),
        style = style,
        density = this,
        softWrap = true,
        constraints = constraints
    )
}
