package com.example.canvas.composable

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.PathEffect
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.unit.dp

@Composable
fun OutlinedBox() {
    Box(
        modifier = Modifier
            .offset(100.dp)
            .size(50.dp)
            .drawBehind {
                val pathEffect = PathEffect.dashPathEffect(floatArrayOf(10f, 10f), 0f)

                drawRect(
                    color = Color.Red,
                    topLeft = Offset(0f, 0f),
                    size = size,
                    style = Stroke(
                        width = 2f,
                        pathEffect = pathEffect
                    )
                )
            },
    )
}