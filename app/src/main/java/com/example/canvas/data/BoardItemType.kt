package com.example.canvas.data

enum class BoardItemType {
    STICKY, TEXT, SHAPE, IMAGE
}