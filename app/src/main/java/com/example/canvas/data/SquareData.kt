package com.example.canvas.data

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.sp

data class SquareData(
    val id: Int,
    var text: String = "",
    val topLeft: Offset = Offset(0f, 0f),
    val size: Size = Size(100f, 100f), // default square is 100x100
    val color: Color = Color(0xFFFFD966),
    val textStyle: TextStyle = TextStyle(
        color = Color.White,
        fontSize = 16.sp
    )
)

fun SquareData.toBoardItemModel(): BoardItemModel = BoardItemModel(
    id = id,
    topLeft = topLeft,
    size = size,
    color = color,
    text = text,
    textStyle = textStyle,
)