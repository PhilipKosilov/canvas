package com.example.canvas.data

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.sp

data class CircleData(
    val id: Int,
    var text: String = "",
    val topLeft: Offset = Offset(0f, 0f),
    val size: Size = Size(100f, 100f), // default shape is 100x100
    val color: Color = Color(0xFFFF6584),
    val textStyle: TextStyle = TextStyle(
        color = Color.White,
        fontSize = 16.sp
    )
)

fun CircleData.toBoardItemModel(): BoardItemModel = BoardItemModel(
    id = id,
    topLeft = topLeft,
    size = size,
    shapeType = ShapeType.CIRCLE,
    color = color,
    text = text,
    textStyle = textStyle,
)



