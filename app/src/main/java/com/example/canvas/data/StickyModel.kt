package com.example.canvas.data

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.sp

data class StickyModel(
    val id: Int,
    val topLeft: Offset = Offset(0f, 0f),
    val size: Size = Size(164f, 164f), // default sticky is 164x164
    val color: Color = Color(0xFFFFD966),
    val author: String = "",
    var text: String = "",
    val textStyle: TextStyle = TextStyle(
        color = Color.Black, //color is not Black
        fontSize = 16.sp
    )
)

fun StickyModel.toBoardItemModel(): BoardItemModel = BoardItemModel(
    id = id,
    topLeft = topLeft,
    size = size,
    type = BoardItemType.STICKY,
    color = color,
    author = author,
    text = text,
    textStyle = textStyle,
)

// Need info, if selected or not
fun BoardItemModel.toStickyData(): StickyModel = StickyModel(
    id = id,
    topLeft = topLeft,
    size = size,
    color = color ?: Color(0xFFFFD966),
    author = author ?: "",
    text = text ?: "",
    textStyle = textStyle ?: TextStyle(
        color = Color.Black, //color is not Black
        fontSize = 16.sp
    )
)