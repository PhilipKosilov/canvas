package com.example.canvas.data

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.sp


/**
 * Can one data class represent all Board items?
 */
data class ShapeModel(
    val id: Int,
    val topLeft: Offset = Offset(x = 0f, y = 0f),
    val size: Size = Size(100f, 100f), // default square is 100x100 //todo DpSize??
    val type: ShapeType,
    val color: Color = Color(0xFFFF6584),
    var text: String = "",
    val textStyle: TextStyle = TextStyle(
        color = Color.White,
        fontSize = 16.sp
    )
)

fun ShapeModel.toBoardItemModel(): BoardItemModel = BoardItemModel(
    id = id,
    topLeft = topLeft,
    size = size,
    type = BoardItemType.SHAPE,
    shapeType = type,
    color = color,
    text = text,
    textStyle = textStyle,
)