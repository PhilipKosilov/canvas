package com.example.canvas.data

data class BoardItemUIModel(
    val model: BoardItemModel,
    var isSelected: Boolean = false,
    var isEditMode: Boolean = false
)

/**
 * Can I open it here?
 * What about different elements having different data parameters?
 */