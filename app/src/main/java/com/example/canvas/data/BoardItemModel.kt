package com.example.canvas.data

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle

//interface BoardItemModel {
//    val id: Int
//}


/**
 * Represents all data, that items might need
 */
data class BoardItemModel(
    val id: Int,
    val size: Size,
    val topLeft: Offset,
    val type: BoardItemType? = null,
    val shapeType: ShapeType? = null,
    val text: String? = null,
    val color: Color? = null,
    val textStyle: TextStyle? = null,
    val author: String? = null,
    val isSelected: Boolean = false,
    val isEditMode: Boolean = false
)


