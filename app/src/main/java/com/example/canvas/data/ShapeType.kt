package com.example.canvas.data

enum class ShapeType {
    SQUARE, CIRCLE
}