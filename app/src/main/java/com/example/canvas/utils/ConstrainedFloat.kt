package com.example.canvas.utils

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.geometry.toRect

class ConstrainedFloat(initialValue: Float = 0f, val minValue: Float = 0f) {

    var value = initialValue
        private set

    var excess: Float = 0f
        private set

    init {
        require(initialValue >= minValue) { "Initial value ($initialValue) must be greater than or equal to the specified minimal value ($minValue)" }
    }

    operator fun plus(amount: Float) {
        if (amount > 0) add(amount)
        else subtract(amount)
    }

    operator fun minus(amount: Float) {
        if (amount > 0) subtract(amount)
        else add(amount)
    }

    private fun add(amount: Float) {
        // 1. If excess > amount, add 0. Else add what is left after subtracting the excess from a given amount.
        value += maxOf(0f, amount - excess)
        // 2. If excess > 0, decrease `excess` by 'amount' until it reaches 0.
        excess = maxOf(0f, excess - amount)
    }

    private fun subtract(amount: Float) {
        // 1. Increase excess, if amount > currentValue - minValue (which is how much you can subtract from currentValue up to minValue).
        excess += maxOf(0f, amount - (value - minValue))
        // 2. Subtract what 'amount' you can from currentValue, until it reaches minValue.
        value = maxOf(minValue, value - amount)
    }

    override fun toString(): String {
        return "${super.toString()}: Value=$value, Minimal value=$minValue, Excess=$excess"
    }
}

class ConstrainedInt2(initialValue: Int, val minValue: Int) {
    var currentValue = initialValue
        private set

    var excess: Int = 0
        private set

    init {
        require(currentValue >= minValue) { "Initial value must be greater than or equal to the specified minimal value" }
    }

    fun process(value: Int) : ConstrainedResult<Int> {
        val use = maxOf(minValue-currentValue, value - excess)
        val extra = use - value

        currentValue += use
        excess += extra

        return ConstrainedResult(use, extra)
    }
}

class ConstrainedSize(initialSize: Size = Size.Zero, val minSize: Size = Size.Zero) {
    var value = initialSize
        private set

    var excess: Offset = Offset.Zero
        private set

    init {
        require(initialSize.width >= minSize.width && initialSize.height >= minSize.height) { "Initial value ($initialSize) must be greater than or equal to the specified minimal value ($minSize)" }
    }

    fun process(change: Offset): ConstrainedResult<Offset> {
        val used = Offset(
            x = maxOf(minSize.width - value.width, change.x - excess.x),
            y = maxOf(minSize.height - value.height, change.y - excess.y)
        )

        val extra = Offset(
            x = used.x - change.x,
            y = used.y - change.y
        )

        value = value.copy(
            width = value.width + used.x,
            height = value.height + used.y
        )

        excess += extra

        return ConstrainedResult(used, extra)
    }

    override fun toString(): String {
        return "${super.toString()}: Value=$value, Minimal value=$minSize, Excess=$excess"
    }
}

data class ConstrainedResult<T>(val used: T, val excess: T)