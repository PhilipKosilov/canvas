package com.example.canvas.utils

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalFontFamilyResolver
import androidx.compose.ui.text.Paragraph
import androidx.compose.ui.text.ParagraphIntrinsics
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.font.createFontFamilyResolver
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

const val TAG = "ricecake"

fun Modifier.remembered(modifier: () -> Modifier): Modifier = composed {
    val rememberedModifier = remember {
        modifier()
    }
    then(rememberedModifier)
}

fun Modifier.conditional(
    condition: Boolean,
    ifTrue: Modifier.() -> Modifier,
    ifFalse: (Modifier.() -> Modifier)? = null
): Modifier {
    return if (condition) {
        then(ifTrue(Modifier))
    } else if (ifFalse != null) {
        then(ifFalse(Modifier))
    } else {
        this
    }
}

fun Modifier.onSizeActuallyChanged(onSizeChanged: (IntSize) -> Unit): Modifier = composed {
    var size by remember { mutableStateOf(IntSize(Int.MIN_VALUE, Int.MIN_VALUE)) }

    onSizeChanged {
        if (it != size) {
            size = it
            onSizeChanged(it)
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun Test(

) {
    // Remember modifier
    TextField(
        modifier = Modifier.remembered { // this remembers onSizeChanged and prevents calls every recomposition
            Modifier.onSizeChanged {// this calls every recomposition

            }
        },
        value = "",
        onValueChange = {},

        )
}


@Composable
fun Temp() {
    var shrunkFontSize = 72.sp
    val calculateIntrinsics = @Composable { // no size constraints
        ParagraphIntrinsics(
            text = "inputValue",
            style = TextStyle(
                fontSize = shrunkFontSize,
                fontWeight = FontWeight.SemiBold,
                lineHeight = 80.sp,
                textAlign = TextAlign.Center
            ),
            density = LocalDensity.current,
            fontFamilyResolver = createFontFamilyResolver(LocalContext.current)
        )
    }

    var intrinsics = calculateIntrinsics()

    val maxWidth = 100.dp
    val calculateParagraph = @Composable {
        Paragraph( // something like this
            text = "value",
            style = TextStyle(fontSize = 16.sp),
            density = LocalDensity.current,
            fontFamilyResolver = LocalFontFamilyResolver.current,
            constraints = Constraints(
                minWidth = with(LocalDensity.current) { maxWidth.toPx() }.toInt()
            )
        )
    }
}