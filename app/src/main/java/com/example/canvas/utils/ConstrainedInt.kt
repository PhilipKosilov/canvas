package com.example.canvas.utils

class ConstrainedInt(initialValue: Int = 0, val minValue: Int = 0) {

    var value = initialValue
        private set

    var excess: Int = 0
        private set

    init {
        require(initialValue >= minValue) { "Initial value ($initialValue) must be greater than or equal to the specified minimal value ($minValue)" }
    }

    operator fun plus(amount: Int) {
        if(amount > 0) add(amount)
        else subtract(amount)
    }

    operator fun minus(amount: Int) {
        if (amount > 0) subtract(amount)
        else add(amount)
    }

    private fun add(amount: Int) {
        // 1. If excess > amount, add 0. Else add what is left after subtracting the excess from a given amount.
        value += maxOf(0, amount - excess)
        // 2. If excess > 0, decrease `excess` by 'amount' until it reaches 0.
        excess = maxOf(0, excess - amount)
    }

    private fun subtract(amount: Int) {
        // 1. Increase excess, if amount > currentValue - minValue (which is how much you can subtract from currentValue up to minValue).
        excess += maxOf(0, amount - (value - minValue))
        // 2. Subtract what 'amount' you can from currentValue, until it reaches minValue.
        value = maxOf(minValue, value - amount)
    }

    override fun toString(): String {
        return "${super.toString()}: Value=$value, Minimal value=$minValue, Excess=$excess"
    }
}