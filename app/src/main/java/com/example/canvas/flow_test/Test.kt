package com.example.canvas.flow_test

import com.example.canvas.flow_test.models.BoardEntity
import com.example.canvas.flow_test.models.BoardMapper
import com.example.canvas.flow_test.models.FirebaseBoardEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

val coroutineScope: CoroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.IO)

val boardEntity = BoardEntity(
    id = "12345",
    title = "board",
    userId = "userId",
    creator = "userId",
    createdAt = 10000,
    modifiedAt = 10000,
    modifiedBy = "userId"
)

private val firebaseBoardFlow = MutableStateFlow<FirebaseBoardEntity?>(null)

val boardMapper = BoardMapper()

fun main() {
    // 1. Subscribe to boardsFlow to collect all FirebaseBoardEntity from Firebase.
    val boardFlow = subscribeToBoard()

    // 2. Collect all BoardEntity from boardFlow
    val collectJob = coroutineScope.launch {
        boardFlow.collect {
            println("Board updated to $it")
        }
    }

    val sendJob = coroutineScope.launch {
        // 3. Simulate saving BoardEntity to Firebase. This board will be converted to FirebaseBoardEntity
        // and read back in onDataChange. It'll be send to boardsFlow to be collected.
        val firebaseBoardEntity = boardMapper.toFirebaseEntity(boardEntity)
        firebaseBoardFlow.emit(firebaseBoardEntity)
        delay(1000)

        // 4. Simulate board changing state.
        val firebaseBoardEntityChanged = firebaseBoardEntity.copy(
            title = "new title"
        )
        firebaseBoardFlow.emit(firebaseBoardEntityChanged)
        delay(1000)

        // 5. Simulate board content change.
        val firebaseBoardEntityContentChanged = firebaseBoardEntity.copy(
            content = listOf()
        )
        firebaseBoardFlow.emit(firebaseBoardEntityContentChanged)
    }


    runBlocking {
        collectJob.join()
        sendJob.join()
    }
    println("Program end....")
}

fun subscribeToBoard(): StateFlow<BoardEntity?> {
    val firebaseEntityFlow = firebaseBoardFlow

    return firebaseEntityFlow.map {
//        println("Mapping... $it")
        it?.let { fireBaseEntity ->
            boardMapper.toEntity(
                entity = fireBaseEntity,
            )
        }
    }.stateIn(coroutineScope, SharingStarted.Eagerly, null)
}