package com.example.canvas.flow_test.models

class BoardMapper {
    fun toFirebaseEntity(entity: BoardEntity) = FirebaseBoardEntity(
        id = entity.id,
        title = entity.title,
        userId = entity.userId,
        imageUrl = entity.imageUrl,
        creator = entity.creator,
        createdAt = entity.createdAt,
        modifiedBy = entity.modifiedBy,
        modifiedAt = entity.modifiedAt,
        content = entity.content
    )

    fun toEntity(entity: FirebaseBoardEntity) = BoardEntity(
        id = entity.id,
        title = entity.title!!,
        userId = entity.userId!!,
        imageUrl = entity.imageUrl,
        creator = entity.creator!!,
        createdAt = entity.createdAt!!,
        modifiedBy = entity.modifiedBy!!,
        modifiedAt = entity.modifiedAt!!,
        content = entity.content
    )
}