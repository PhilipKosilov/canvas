package com.example.canvas.flow_test.models

import android.os.Parcelable
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class BoardEntity(
    val id: String? = null,
    val title: String,
    val userId: String,
    val imageUrl: String? = null,
    val creator: String,
    val createdAt: Long,
    val modifiedBy: String,
    val modifiedAt: Long,
    @IgnoredOnParcel val content: List<BoardItemModel>? = null,
): Parcelable