package com.example.canvas.flow_test.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue

// Needs to have a default constructor.
@Parcelize
data class FirebaseBoardEntity(
    val id: String? = null,
    val title: String? = null,
    val userId: String? = null,
    val imageUrl: String? = null,
    val creator: String? = null,
    val createdAt: Long? = null,
    val modifiedBy: String? = null,
    val modifiedAt: Long? = null,
    val content: @RawValue List<BoardItemModel>? = null,
) : Parcelable