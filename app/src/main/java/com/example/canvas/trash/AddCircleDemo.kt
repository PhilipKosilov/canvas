//package com.example.canvas.trash
//
//import androidx.compose.foundation.layout.Box
//import androidx.compose.foundation.layout.Row
//import androidx.compose.foundation.layout.fillMaxSize
//import androidx.compose.foundation.layout.offset
//import androidx.compose.material3.Surface
//import androidx.compose.runtime.Composable
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.geometry.Offset
//import androidx.compose.ui.platform.LocalDensity
//import androidx.compose.ui.tooling.preview.Preview
//import androidx.compose.ui.unit.dp
//import com.example.canvas.data.CircleData
//import com.example.canvas.ui.theme.CanvasTheme
//
//
//@Composable
//fun AddCircleDemo() {
//    Box {
//        Row(
//        ) {
//            NonEditCircleText(
//                CircleData(
//                    text = "non edit",
//                    center = Offset(150f, 150f),
//                )
//            )
//        }
//
//        Row(
//            modifier = Modifier.offset(x = 110.dp)
//        ) {
//            TextFieldCircle(
//                CircleData(
//                    text = "field",
//                )
//            )
//        }
//
//        Row {
//            val offsetX = with(LocalDensity.current) { 220.dp.toPx() }
//            BasicTextFieldCircle(
//                CircleData(
//                    text = "basic",
//                    center = Offset(x = offsetX, 0f)
//                )
//            )
//        }
//
//        Row(
//            modifier = Modifier.offset(y = 110.dp)
//        ) {
//            EditCursorCircle(
//                CircleData(
//                    text = "box3",
//                )
//            )
//        }
//
//        Row(
//            modifier = Modifier.offset(x = 110.dp, y = 110.dp)
//        ) {
//            EditCanvasCircle(
//                CircleData(
//                    text = "edit",
//                )
//            )
//        }
//
//        Row(
//            modifier = Modifier.offset(x = 220.dp, y = 110.dp)
//        ) {
//            HiddenTextFieldCircle(
//                CircleData(
//                    text = "hidden",
//                )
//            )
//        }
//
//        DraggableCircle(
//            x = 400f,
//            y = 1700f
//        )
//    }
//}
//
//@Preview(showBackground = true)
//@Composable
//private fun Preview() {
//    CanvasTheme {
//        Surface(
//            modifier = Modifier.fillMaxSize(),
//        ) {
//            AddCircleDemo()
//        }
//    }
//}
//
