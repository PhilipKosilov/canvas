//package com.example.canvas.trash
//
//import androidx.compose.foundation.Canvas
//import androidx.compose.foundation.clickable
//import androidx.compose.foundation.interaction.MutableInteractionSource
//import androidx.compose.foundation.layout.Box
//import androidx.compose.foundation.layout.offset
//import androidx.compose.foundation.layout.size
//import androidx.compose.material3.ExperimentalMaterial3Api
//import androidx.compose.material3.Text
//import androidx.compose.material3.TextField
//import androidx.compose.material3.TextFieldDefaults
//import androidx.compose.runtime.Composable
//import androidx.compose.runtime.getValue
//import androidx.compose.runtime.mutableStateOf
//import androidx.compose.runtime.remember
//import androidx.compose.runtime.setValue
//import androidx.compose.ui.Alignment
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.geometry.Size
//import androidx.compose.ui.graphics.Color
//import androidx.compose.ui.text.style.TextAlign
//import androidx.compose.ui.unit.dp
//import com.example.canvas.data.CircleData
//
//@OptIn(ExperimentalMaterial3Api::class)
//@Composable
//fun TextFieldCircle(
//    data: CircleData
//) {
//    var canvasSize by remember { mutableStateOf(Size.Unspecified) }
//    var isEditMode by remember { mutableStateOf(false) }
//    var text by remember { mutableStateOf(data.text) }
//    val boxSize = remember { (data.radius).dp }
//    val interactionSource = remember { MutableInteractionSource() }
//
//    Box(
//        contentAlignment = Alignment.Center,
//        modifier = Modifier
//            .size(boxSize)
//            .offset(data.center.x.dp, data.center.y.dp)
//            .clickable(
//                interactionSource = interactionSource,
//                indication = null,
//            ) {
//                isEditMode = !isEditMode
//            }
//    ) {
//        Canvas(
//            modifier = Modifier
//        ) {
//            canvasSize = size // let Box know size of this drawing
//
//            drawCircle(
//                color = data.color,
//                radius = data.radius,
//            )
//        }
//
//        if (isEditMode) {
//            TextField(
//                value = text,
//                onValueChange = {
//                    text = it
//                },
//                colors = TextFieldDefaults.textFieldColors(
//                    disabledTextColor = Color.Transparent,
//                    focusedIndicatorColor = Color.Transparent,
//                    unfocusedIndicatorColor = Color.Transparent,
//                    disabledIndicatorColor = Color.Transparent
//                ),
//                textStyle = data.textStyle.copy(color = Color.Black)
//            )
//        } else {
//            Text(
////                modifier = Modifier.align(Alignment.Center),
//                text = text,
//                textAlign = TextAlign.Center,
//                style = data.textStyle
//            )
//        }
//    }
//}