package com.example.canvas.trash

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun CombinedClickable() {
    val interactionSource = remember { MutableInteractionSource() }
    Box(
        modifier = Modifier
            .combinedClickable(
                // Removes ripple on click
                interactionSource = interactionSource,
                indication = null,

                onClick = {

                },
                onLongClick = {},
                onDoubleClick = {

                }
            )
    )
}