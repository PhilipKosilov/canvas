//package com.example.canvas.trash
//
//import androidx.compose.foundation.Canvas
//import androidx.compose.foundation.clickable
//import androidx.compose.foundation.layout.size
//import androidx.compose.runtime.Composable
//import androidx.compose.runtime.remember
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.geometry.Offset
//import androidx.compose.ui.text.ExperimentalTextApi
//import androidx.compose.ui.text.TextMeasurer
//import androidx.compose.ui.text.buildAnnotatedString
//import androidx.compose.ui.text.drawText
//import androidx.compose.ui.text.rememberTextMeasurer
//import androidx.compose.ui.unit.dp
//import com.example.canvas.data.CircleData
//
//@OptIn(ExperimentalTextApi::class)
//@Composable
//fun NonEditCircleText(
//    data: CircleData
//) {
//    val textMeasurer: TextMeasurer = rememberTextMeasurer()
//    val labelTextLayout = textMeasurer.measure(
//        text = buildAnnotatedString { append(data.text) },
//        style = data.textStyle
//    )
//
//    val labelTopLeft = Offset(
//        data.center.x - (labelTextLayout.size.width / 2f),
//        data.center.y - (labelTextLayout.size.height / 2f)
//    )
//
//    val boxSize = remember {
//        (data.radius).dp
//    }
//
//    /* !! Canvas can't calculate it's own size! */
//    Canvas(
//        modifier = Modifier
//            .size(boxSize)
//            .clickable { }
////            .offset(data.center.x.dp, data.center.y.dp)
//    ) {
//
//        drawCircle(
//            color = data.color,
//            radius = data.radius,
//            center = data.center
//        )
//        drawText(
//            textMeasurer = textMeasurer,
//            text = data.text,
//            topLeft = labelTopLeft,
//            style = data.textStyle
//        )
//    }
//}