package com.example.canvas.trash

import android.util.Log
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import com.example.canvas.data.CircleData
import com.example.canvas.composable.drawBorder
import com.example.canvas.composable.keyboardAsState
import kotlin.math.roundToInt


//@OptIn(ExperimentalFoundationApi::class)
//@Composable
//fun BasicTextFieldCircle(
//    data: CircleData,
//) {
//    var isSelected by remember { mutableStateOf(false) }
//    var isEditMode by remember { mutableStateOf(false) }
//    var offsetX by remember { mutableStateOf(data.center.x) }
//    var offsetY by remember { mutableStateOf(data.center.y) }
//    val localDensity = LocalDensity.current
//    val boxSize = remember {
//        with(localDensity) {
//            // size of the box is D=2r
//            (data.radius * 2).dp
//        }
//    }
////    val boxSize = remember {
////        (data.radius).dp
////    }
//
//    // To set text cursor position to the end of text, when manually focusing BasicTextField
//    // ! Sets position only once. Maybe move cursor on every edit.
//    var textFieldValue by remember {
//        mutableStateOf(TextFieldValue(data.text, TextRange(data.text.length)))
//    }
//
//    // To check, when IME keyboard is open/closed
//    val isKeyboardVisible by keyboardAsState()
//
//    // To request focus in BasicTextField, when clicking on Box
//    val focusRequester = remember { FocusRequester() }
//    // To clear focus, when closing keyboard
//    val focusManager = LocalFocusManager.current
//
//    LaunchedEffect(isEditMode) {
//        if (isEditMode) {
//            focusRequester.requestFocus()
//        }
//    }
//
//    // When closing IME keyboard during editing
//    LaunchedEffect(isKeyboardVisible) {
//        if (isEditMode && !isKeyboardVisible) {
//            isEditMode = false
//            focusManager.clearFocus()
//        }
//    }
//
//    Box(
//        contentAlignment = Alignment.Center,
//        modifier = Modifier
//            .size(boxSize)
//            .offset { IntOffset(offsetX.roundToInt(), offsetY.roundToInt()) }
//            .pointerInput(Unit) {
//                detectDragGestures { change, dragAmount ->
//                    if (isSelected) {
//                        change.consume()
//                        offsetX += dragAmount.x
//                        offsetY += dragAmount.y
//                    }
//                }
//            }
//            .pointerInput(Unit) {
//                detectTapGestures(
//                    onTap = {
//                        // if click when editing, stop editing
//                        if (isEditMode) {
//                            isEditMode = false
//                        } else {
//                            isSelected = !isSelected
//                        }
//                    },
//                    onDoubleTap = {
//                        isEditMode = !isEditMode
//                    }
//                )
//            }
//            .drawBehind {
//                Log.i("ricecake", "onCreate: ${100f.dp.toPx()}")
//
//
//                drawCircle(
//                    color = data.color,
//                    radius = data.radius.dp.toPx(), // todo set proper size
//                )
//                // draw selection border
//                if (isSelected) {
//                    drawBorder(
//                        // You don't need to offset border relatively to the Box
//                        // When dragging, the whole box is moving
//                        topLeft = Offset(0f, 0f),
//                    )
//                }
//            }
//    ) {
//        if (isEditMode) {
//            BasicTextField(
//                modifier = Modifier
////                    .background(Color.LightGray) // temp background
//                    .focusRequester(focusRequester),
//                value = textFieldValue,
//                singleLine = true,
//                onValueChange = {
//                    textFieldValue = it
//                },
//                textStyle = data.textStyle.copy(textAlign = TextAlign.Center) // this is how align text
//                // Doesn't work, if change focus here
//            )
//
//        } else {
//            Text(
//                text = textFieldValue.text,
//                textAlign = TextAlign.Center,
//                style = data.textStyle,
//                maxLines = 1
//            )
//        }
//    }
//}