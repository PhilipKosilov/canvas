package com.example.canvas.trash

import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.canvas.ui.theme.CanvasTheme

/**
 * Use this, when you only need to draw custom shape
 */
@Composable
fun Drawing() {
    Canvas(
        modifier = Modifier
            .padding(20.dp)
            .size(300.dp)
    ) {
        //Drawing content
        center /* Center [Offset] for this canvas */
        size /* [Size] of this canvas (2D floating-point) */
    }
}

/**
 * Use this, when you need to draw custom shape BEHIND some content
 */
@Composable
fun Container() {
    Box(
        modifier = Modifier
            .padding(20.dp)
            .size(300.dp)
            .drawBehind {
                //Drawing content, will be drawn behind com.example.canvas.main content
            }
    ) {
        // Main content
    }
}

/**
 * All numeric values are PX, not DP. You have to convert to DP
 * width = 5f -> width = 5.dp.toPx()
 */

@Composable
fun SimpleRectangle() {
    Canvas(
        modifier = Modifier
            .padding(20.dp)
            .size(300.dp)
    ) {
        drawRect(
            color = Color.LightGray,
            size = size
        )
        drawRect(
            color = Color.White,
            topLeft = Offset(size.width / 4, size.height / 4),
            size = Size(size.width / 2, size.height / 2),
            style = Stroke(
                width = 5.dp.toPx()
            )

        )
    }
}

@Composable
fun SimpleCircle() {
    Canvas(
        modifier = Modifier
            .padding(20.dp)
            .size(300.dp)
    ) {
        val strokeSize = 30.dp.toPx()
        // Account for stroke size as well
        val radius = size.minDimension / 2 - strokeSize / 2
        val centerX = size.width / 2
        val centerY = size.height / 2

        drawCircle(
            brush = Brush.linearGradient(
                colors = listOf(
                    Color.LightGray, Color.Gray, Color.DarkGray
                )
            ),
            radius = radius,
            center = Offset(centerX, centerY),
            style = Stroke(
                width = strokeSize
            )
        )
    }
}

@Composable
fun PieChart(
    modifier: Modifier = Modifier
) {
    Canvas(
        modifier = modifier
            .padding(20.dp)
            .size(100.dp)
    ) {
        drawArc(
            color = Color.DarkGray,
            startAngle = 0f,
            sweepAngle = 270f,
            useCenter = true,
            size = size
        )
    }
}

@Composable
fun LoadingCircle(
    modifier: Modifier = Modifier
) {
    val infiniteTransition = rememberInfiniteTransition()
    val animateSweepAngle by infiniteTransition.animateFloat(
        initialValue = 270f,
        targetValue = 0f,
        animationSpec = infiniteRepeatable(
            animation = tween(durationMillis = 1500),
            repeatMode = RepeatMode.Reverse
        )
    )

    Canvas(
        modifier = modifier
            .padding(20.dp)
            .size(100.dp)
    ) {
        val strokeSize = 10.dp.toPx()
        val drawSize = Size(size.width - strokeSize / 2, size.height - strokeSize / 2)

        drawArc(
            brush = Brush.linearGradient(
                colors = listOf(
                    Color.Green, Color.Gray
                )
            ),
            startAngle = 0f,
            sweepAngle = animateSweepAngle, // animate this to rotate
            useCenter = false,
            size = drawSize,
            style = Stroke(
                width = strokeSize
            )
        )
    }
}

@Composable
fun ExampleScreen() {
    Box(
        modifier = Modifier.fillMaxSize()
    ) {
        SimpleRectangle()
        SimpleCircle()
        PieChart(Modifier.offset(y = 320.dp))
        LoadingCircle(Modifier.offset(x = 120.dp, y = 320.dp))
    }
}

@Preview(showBackground = true)
@Composable
private fun Preview() {
    CanvasTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
        ) {
            ExampleScreen()
        }
    }
}

fun main() {
    val text: String? = null
    val text2: String? = "text"
    println((text as? String)?.length)
    println((text2 as? String)?.length)
}