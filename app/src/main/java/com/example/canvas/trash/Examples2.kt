package com.example.canvas.trash

import android.util.Log
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Modifier

data class Model(
    var value: Int
)

private val _items: SnapshotStateList<Model> = mutableStateListOf(
    Model(1), Model(2)
)

private val _items2: SnapshotStateList<MutableState<Model>> = mutableStateListOf(
    mutableStateOf(Model(1)), mutableStateOf(Model(1))
)

private val _items3: SnapshotStateList<Model> = mutableStateListOf(
    mutableStateOf(Model(1)).value, mutableStateOf(Model(1)).value
)

private val backItems4: List<Model> = listOf(
    Model(1), Model(2)
)
private val _items4: SnapshotStateList<Model> = mutableStateListOf(
    Model(1), Model(2)
)



@Composable
fun Examples2() {
    Log.i("ricecake", "Examples2: recomposition")
    _items4.forEach {

    }

    Column {
        Row {
            Button(
                onClick = {
                    _items[0] = _items[0].copy(
                        value = _items[0].value + 1
                    )
                }
            ) {
                Text("Copy ${_items[0].value}")
            }
            Button(
                onClick = {
                    _items[0].value =  _items[0].value + 1
                }
            ) {
                Text("Change var ${_items[0].value}")
            }
        }
        // ---------------------------
        Row {
            Button(
                onClick = {
                    _items2[0].value = _items2[0].value.copy(
                        value = _items2[0].value.value + 1
                    )
                }
            ) {
                Text("Copy ${_items2[0].value.value}")
            }
            Button(
                onClick = {
                    _items2[0].value.value = _items2[0].value.value + 1
                }
            ) {
                Text("Change var ${_items2[0].value.value}")
            }
        }

        // ---------------------------
        Row {
            Button(
                onClick = {
                    _items3[0] = _items3[0].copy(
                        value = _items3[0].value + 1
                    )
                }
            ) {
                Text("Copy ${_items3[0].value}")
            }
            Button(
                onClick = {
                    _items3[0].value = _items3[0].value + 1
                }
            ) {
                Text("Change var ${_items3[0].value}")
            }
        }

        // -------------- Okay, works
        Row {
            Button(
                onClick = {
                    backItems4[0].value++
                    _items4.swapList(backItems4) // update
                }
            ) {
                Text("Copy ${_items4[0].value}")
            }
        }
    }
}

fun <T> SnapshotStateList<T>.swapList(newList: List<T>){
    clear()
    addAll(newList)
}



//---------------------------------
@Composable
fun ComposeListExample() {
    var mutableList: MutableState<MutableList<String>> = remember {
        mutableStateOf(mutableListOf())
    }
    var mutableList1: MutableState<MutableList<String>> = remember {
        mutableStateOf(mutableListOf())
    }
    var arrayList: MutableState<ArrayList<String>> = remember {
        mutableStateOf(ArrayList())
    }
    var arrayList1: MutableState<ArrayList<String>> = remember {
        mutableStateOf(ArrayList())
    }
    var list: MutableState<List<String>> = remember {
        mutableStateOf(listOf())
    }

    Column(
        Modifier.verticalScroll(state = rememberScrollState())
    ) {
        // Uncomment the below 5 methods one by one to understand how they work.
        // Don't uncomment multiple methods and check.


//         ShowListItems("MutableList", mutableList.value)
//         ShowListItems("Working MutableList", mutableList1.value)
//         ShowListItems("ArrayList", arrayList.value)
//         ShowListItems("Working ArrayList", arrayList1.value)
         ShowListItems("List", list.value)

        Button(
            onClick = {
                mutableList.value.add("")
                arrayList.value.add("")

                val newMutableList1 = mutableListOf<String>()
                mutableList1.value.forEach {
                    newMutableList1.add(it)
                }
                newMutableList1.add("")
                mutableList1.value = newMutableList1

                val newArrayList1 = arrayListOf<String>()
                arrayList1.value.forEach {
                    newArrayList1.add(it)
                }
                newArrayList1.add("")
                arrayList1.value = newArrayList1

                val newList = mutableListOf<String>()
                list.value.forEach {
                    newList.add(it)
                }
                newList.add("")
                list.value = newList
            },
        ) {
            Text(text = "Add")
        }
    }
}

@Composable
private fun ShowListItems(title: String, list: List<String>) {
    Text(title)
    Column {
        repeat(list.size) {
            Text("$title Item Added")
        }
    }
}