//package com.example.canvas.trash
//
//import androidx.compose.foundation.Canvas
//import androidx.compose.foundation.clickable
//import androidx.compose.foundation.layout.Box
//import androidx.compose.foundation.layout.offset
//import androidx.compose.foundation.layout.size
//import androidx.compose.runtime.Composable
//import androidx.compose.runtime.LaunchedEffect
//import androidx.compose.runtime.getValue
//import androidx.compose.runtime.mutableStateOf
//import androidx.compose.runtime.remember
//import androidx.compose.runtime.setValue
//import androidx.compose.ui.Alignment
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.geometry.Offset
//import androidx.compose.ui.graphics.Color
//import androidx.compose.ui.unit.dp
//import com.example.canvas.data.CircleData
//import kotlinx.coroutines.delay
//
//@Composable
//fun EditCursorCircle(
//    data: CircleData
//) {
//    var isShowCursor by remember {
//        mutableStateOf(false)
//    }
//    val boxSize = remember {
//        (data.radius).dp
//    }
//
//    LaunchedEffect(isShowCursor) {
//        delay(500)
//        isShowCursor = !isShowCursor
//    }
//
//    Box(
//        contentAlignment = Alignment.Center,
//        modifier = Modifier
//            .size(boxSize)
//            .offset(data.center.x.dp, data.center.y.dp)
//            .clickable {
//                isShowCursor = !isShowCursor
//            }
//    ) {
//        Canvas(
//            modifier = Modifier
//        ) {
//            drawCircle(
//                color = data.color,
//                radius = data.radius,
//            )
//
//            if (isShowCursor) {
//                drawLine(
//                    color = Color.White,
//                    start = Offset(0f, -25f),
//                    end = Offset(0f, 25f),
//                    strokeWidth = 1.dp.toPx()
//                )
//            }
//        }
//    }
//}