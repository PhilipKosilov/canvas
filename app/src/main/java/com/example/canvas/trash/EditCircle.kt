//package com.example.canvas.trash
//
//import androidx.compose.foundation.Canvas
//import androidx.compose.foundation.clickable
//import androidx.compose.foundation.layout.fillMaxSize
//import androidx.compose.runtime.Composable
//import androidx.compose.runtime.LaunchedEffect
//import androidx.compose.runtime.getValue
//import androidx.compose.runtime.mutableStateOf
//import androidx.compose.runtime.remember
//import androidx.compose.runtime.setValue
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.geometry.Offset
//import androidx.compose.ui.text.ExperimentalTextApi
//import androidx.compose.ui.text.TextMeasurer
//import androidx.compose.ui.text.buildAnnotatedString
//import androidx.compose.ui.text.drawText
//import androidx.compose.ui.text.rememberTextMeasurer
//import androidx.compose.ui.unit.dp
//import com.example.canvas.data.CircleData
//import kotlinx.coroutines.delay
//
//@OptIn(ExperimentalTextApi::class)
//@Composable
//fun EditCircle(
//    data: CircleData
//) {
//    var isEditMode by remember { mutableStateOf(false) }
//    var showEditCursor by remember { mutableStateOf(false) }
//
//    var text by remember { mutableStateOf(data.text) }
//    val boxSize = remember { (data.radius).dp }
//    val textMeasurer: TextMeasurer = rememberTextMeasurer()
//    val labelTextLayout = textMeasurer.measure(
//        text = buildAnnotatedString { append(data.text) },
//        style = data.textStyle
//    )
//    val labelTopLeft = Offset(
//        data.center.x - (labelTextLayout.size.width / 2f),
//        data.center.y - (labelTextLayout.size.height / 2f)
//    )
//
//    LaunchedEffect(showEditCursor) {
//        delay(500)
//        showEditCursor = !showEditCursor
//    }
//
//    Canvas(
//        modifier = Modifier
//            .fillMaxSize()
////            .size(boxSize)
//            .clickable {
//                isEditMode = !isEditMode
//            }
//    ) {
//        drawCircle(
//            color = data.color,
//            radius = data.radius,
//            center = data.center
//        )
//
//        drawText(
//            textMeasurer = textMeasurer,
//            text = text,
//            topLeft = labelTopLeft,
//            style = data.textStyle
//        )
//
////        if (showEditCursor) {
////            drawLine(
////                color = Color.White,
////                start = Offset(0f, -25f),
////                end = Offset(0f, 25f),
////                strokeWidth = 1.dp.toPx()
////            )
////        }
//    }
//}