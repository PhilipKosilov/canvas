package com.example.canvas.trash

import android.util.Log
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.VectorConverter
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.animateValue
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import com.example.canvas.utils.TAG
import com.example.canvas.utils.onSizeActuallyChanged
import kotlin.math.roundToInt

@Composable
fun TestResizeFloatV7(
    startOffsetX: Dp = 200.dp,
    startOffsetY: Dp = 100.dp,
) {
    val offsetX by remember { mutableStateOf(startOffsetX) }
    val offsetY by remember { mutableStateOf(startOffsetY) }
    val size by remember { mutableStateOf(DpSize(100.dp, 48.dp)) }
    val density = LocalDensity.current

    val infiniteTransition = rememberInfiniteTransition()
    val offset by infiniteTransition.animateFloat(
        initialValue = 0f,
        targetValue = 250f,
        animationSpec = infiniteRepeatable(
            animation = tween(durationMillis = 3000, easing = LinearEasing),
            repeatMode = RepeatMode.Reverse
        )
    )

    Text(
        modifier = Modifier
            .wrapContentSize(Alignment.TopStart)
            .size(with(density) {
                DpSize(
                    width = size.width + offset.toDp(),
                    height = size.height
                )
            })
            .offset {
                IntOffset(
                    x = offsetX.roundToPx() - offset.roundToInt(), // less shift, when using this
                    y = offsetY.roundToPx()
                )
            }
//            .offset(
//                x = with(density) { offsetX.roundToPx().toDp() - offset.roundToInt().toDp() },
//                y = offsetY
//            )
            .border(width = 1.dp, color = Color.Blue, shape = RectangleShape),
        text = "Float offset to IntOffset in modifier"
    )
}


@Composable
fun TestResizeFloatV6(
    startOffsetX: Dp = 200.dp,
    startOffsetY: Dp = 100.dp,
) {
    val offsetX by remember { mutableStateOf(startOffsetX) }
    val offsetY by remember { mutableStateOf(startOffsetY) }
    val size by remember { mutableStateOf(DpSize(100.dp, 48.dp)) }
    val density = LocalDensity.current

    val infiniteTransition = rememberInfiniteTransition()
    val offset by infiniteTransition.animateFloat(
        initialValue = 0f,
        targetValue = 200f,
        animationSpec = infiniteRepeatable(
            animation = tween(durationMillis = 3000, easing = LinearEasing),
            repeatMode = RepeatMode.Reverse
        )
    )
    val offsetInt = offset.roundToInt()

    Text(
        modifier = Modifier
            .wrapContentSize(Alignment.TopStart)
            .size(with(density) {
                DpSize(
                    width = size.width + offsetInt.toDp(),
                    height = size.height
                )
            })
            .offset {
                IntOffset(
                    x = offsetX.roundToPx() - offsetInt,
                    y = offsetY.roundToPx()
                )
            }
            .border(width = 1.dp, color = Color.Blue, shape = RectangleShape),
        text = "IntOffset"
    )
}

// Nothing actually works. Bug depends on Composable position on screen.
// Converting Float drag offset to Int before casting it to dp
@Composable
fun TestResizeFloatV5(
    startOffsetX: Dp = 200.dp,
    startOffsetY: Dp = 100.dp,
) {
    val offsetX by remember { mutableStateOf(startOffsetX) }
    val offsetY by remember { mutableStateOf(startOffsetY) }
    val size by remember { mutableStateOf(DpSize(100.dp, 48.dp)) }
    val density = LocalDensity.current

    val infiniteTransition = rememberInfiniteTransition()
    val offset by infiniteTransition.animateFloat(
        initialValue = 0f,
        targetValue = 20f,
        animationSpec = infiniteRepeatable(
            animation = tween(durationMillis = 3000, easing = LinearEasing),
            repeatMode = RepeatMode.Reverse
        )
    )
    val offsetDp = with(density) { offset.roundToInt().toDp() }

    Text(
        modifier = Modifier
            .wrapContentSize(Alignment.TopStart)
            .size(size.width + offsetDp, size.height)
            .onSizeActuallyChanged {
                with(density) {
                    Log.i(TAG, "1) offset: $offset, dpOffset= ${offsetDp}, size=${size.width + offsetDp}, offset=${offsetX - offsetDp} ")
                }
            }
            .offset(offsetX - offsetDp, offsetY)
            .border(width = 1.dp, color = Color.Blue, shape = RectangleShape),
        text = "Round to Int"
    )
}

// attempt to convert Float to Double for operations to prevent loss
// not working
@Composable
fun TestResizeFloatV4(
    startOffsetX: Dp = 200.dp,
    startOffsetY: Dp = 100.dp,
) {
    val offsetX by remember { mutableStateOf(startOffsetX) }
    val offsetY by remember { mutableStateOf(startOffsetY) }
    val size by remember { mutableStateOf(DpSize(100.dp, 48.dp)) }
    val density = LocalDensity.current

    val infiniteTransition = rememberInfiniteTransition()
    val offset by infiniteTransition.animateFloat(
        initialValue = 0f,
        targetValue = 250f,
        animationSpec = infiniteRepeatable(
            animation = tween(durationMillis = 3000, easing = LinearEasing),
            repeatMode = RepeatMode.Reverse
        )
    )
//    val offsetDp = with(density) { offset.toDp() }
    val offsetDp = offset.toDouble().toDp(density)

    Text(
        modifier = Modifier
            .wrapContentSize(Alignment.TopStart)
            .size(size.width + offsetDp, size.height)
            .offset(offsetX - offsetDp, offsetY)
            .border(width = 1.dp, color = Color.Blue, shape = RectangleShape),
        text = "Cast toDouble then toDp"
    )
}

fun Double.toDp(density: Density) = (this / density.density).dp

@Composable
fun TestResizeFloatV3() {
    val offsetX by remember { mutableStateOf(200.dp) }
    val offsetY by remember { mutableStateOf(100.dp) }
    val size by remember { mutableStateOf(Size(300f, 150f)) }
    val density = LocalDensity.current

    val infiniteTransition = rememberInfiniteTransition()
    val offset by infiniteTransition.animateFloat(
        initialValue = 0f,
        targetValue = 250f,
        animationSpec = infiniteRepeatable(
            animation = tween(durationMillis = 3000, easing = LinearEasing),
            repeatMode = RepeatMode.Reverse
        )
    )

    Text(
        modifier = Modifier
            .wrapContentSize(Alignment.TopStart)
            .size(with(density) {
                DpSize(
                    width = (size.width - offset).toDp(),
                    height = size.height.toDp()
                )
            })
            .offset(
                with(density) {
                    (offsetX.toPx() + offset).toDp()
                },
                offsetY
            )
            .border(width = 1.dp, color = Color.Blue, shape = RectangleShape),
        text = "TestResizeFloatV3"
    )
}

// Still janky
@Composable
fun TestResizeFloatV2() {
    val offsetX by remember { mutableStateOf(200.dp) }
    val offsetY by remember { mutableStateOf(100.dp) }
    val size by remember { mutableStateOf(DpSize(100.dp, 48.dp)) }
    val density = LocalDensity.current

    val infiniteTransition = rememberInfiniteTransition()
    val offset by infiniteTransition.animateFloat(
        initialValue = 0f,
        targetValue = 250f,
        animationSpec = infiniteRepeatable(
            animation = tween(durationMillis = 3000, easing = LinearEasing),
            repeatMode = RepeatMode.Reverse
        )
    )

    Text(
        modifier = Modifier
            .wrapContentSize(Alignment.TopStart)
            .size(with(density) {
                DpSize(
                    width = size.width - offset.toDp(),
                    height = size.height
                )
            })
            .offset(
                with(density) {
                    (offsetX.toPx() + offset).toDp()
                },
                offsetY
            )
            .border(width = 1.dp, color = Color.Blue, shape = RectangleShape),
        text = "TestResizeFloatV2"
    )
}


// Yup, here is a problem
@Composable
fun TestResizeFloat(
    startOffsetX: Dp = 200.dp,
    startOffsetY: Dp = 100.dp,
) {
    val offsetX by remember { mutableStateOf(startOffsetX) }
    val offsetY by remember { mutableStateOf(startOffsetY) }
    val size by remember { mutableStateOf(Size(300f, 130f)) }
    val density = LocalDensity.current

    val dpSize = with(density) {
        DpSize(
            width = size.width.toDp(),
            height = size.height.toDp()
        )
    }

    val infiniteTransition = rememberInfiniteTransition()
    val offset by infiniteTransition.animateFloat(
        initialValue = 0f,
        targetValue = 200f,
        animationSpec = infiniteRepeatable(
            animation = tween(durationMillis = 3000, easing = LinearEasing),
            repeatMode = RepeatMode.Reverse
        )
    )

    Text(
        modifier = Modifier
            .wrapContentSize(Alignment.TopStart)
            .size(with(density) {
                DpSize(
                    width = dpSize.width - offset.toDp(),
                    height = dpSize.height
                )
            })
            .offset(
                with(density) {
                    offsetX + offset.toDp()
                },
                offsetY
            )
            .border(width = 1.dp, color = Color.Blue, shape = RectangleShape),
        text = "Float"
    )
}

// No problems when working with DP
@Composable
fun TestResizeDp(
    startOffsetX: Dp = 200.dp,
    startOffsetY: Dp = 100.dp,
) {
    val offsetX by remember { mutableStateOf(startOffsetX) }
    val offsetY by remember { mutableStateOf(startOffsetY) }
    val size by remember { mutableStateOf(DpSize(100.dp, 48.dp)) }

    val infiniteTransition = rememberInfiniteTransition()
    val offset by infiniteTransition.animateValue(
        initialValue = 0.dp,
        targetValue = 95.dp,
        typeConverter = Dp.VectorConverter,
        animationSpec = infiniteRepeatable(
            animation = tween(durationMillis = 3000, easing = LinearEasing),
            repeatMode = RepeatMode.Reverse
        )
    )

    Text(
        modifier = Modifier
            .wrapContentSize(Alignment.TopStart)
            .size(size.width - offset, size.height)
            .offset(offsetX + offset, offsetY)
            .border(width = 1.dp, color = Color.Blue, shape = RectangleShape),
        text = "Dp only"
    )
}