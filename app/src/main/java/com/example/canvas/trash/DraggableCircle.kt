//package com.example.canvas.trash
//
//import androidx.compose.foundation.Canvas
//import androidx.compose.foundation.background
//import androidx.compose.foundation.gestures.Orientation
//import androidx.compose.foundation.gestures.detectDragGestures
//import androidx.compose.foundation.gestures.draggable
//import androidx.compose.foundation.gestures.rememberDraggableState
//import androidx.compose.foundation.layout.Box
//import androidx.compose.foundation.layout.offset
//import androidx.compose.foundation.layout.size
//import androidx.compose.foundation.shape.CircleShape
//import androidx.compose.runtime.Composable
//import androidx.compose.runtime.getValue
//import androidx.compose.runtime.mutableStateOf
//import androidx.compose.runtime.remember
//import androidx.compose.runtime.setValue
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.draw.clip
//import androidx.compose.ui.draw.drawBehind
//import androidx.compose.ui.graphics.Color
//import androidx.compose.ui.input.pointer.pointerInput
//import androidx.compose.ui.unit.IntOffset
//import androidx.compose.ui.unit.dp
//import com.example.canvas.data.CircleData
//import kotlin.math.roundToInt
//
//@Composable
//fun DraggableCircle(
//    // temp
//    x: Float = 100f,
//    y: Float = 100f
//) {
//    val data = CircleData()
//    val boxSize = remember { (data.radius).dp }
//    var offsetX by remember { mutableStateOf(x) }
//    var offsetY by remember { mutableStateOf(y) }
//
//    Box(
//        modifier = Modifier
//            .offset { IntOffset(offsetX.roundToInt(), offsetY.roundToInt()) }
//            .size(boxSize)
//            .clip(CircleShape)
//            .background(Color.Blue.copy(alpha = 0.1f))
//            .pointerInput(Unit) {
//                detectDragGestures { change, dragAmount ->
//                    change.consume()
//                    offsetX += dragAmount.x
//                    offsetY += dragAmount.y
//                }
//            }
//            .drawBehind {
//                drawCircle(
//                    color = data.color,
//                    radius = data.radius,
////                        center = data.center
//                )
//            }
//    )
//}
//
//@Composable
//fun DraggableCircle2() {
//    val data = CircleData()
//    val boxSize = remember { (data.radius).dp }
//    var offsetX by remember { mutableStateOf(100f) }
//    var offsetY by remember { mutableStateOf(100f) }
//
//    Canvas(
//        modifier = Modifier
//            .size(boxSize)
//            .offset { IntOffset(offsetX.roundToInt(), offsetY.roundToInt()) }
//            .pointerInput(Unit) {
//                detectDragGestures { change, dragAmount ->
//                    change.consume()
//                    offsetX += dragAmount.x
//                    offsetY += dragAmount.y
//                }
//            }
//    ) {
//
//        drawCircle(
//            color = data.color,
//            radius = data.radius,
////            center = data.center
//        )
//    }
//}
//
//@Composable
//fun DraggableCircle3() {
//    val data = CircleData()
//    val boxSize = remember { (data.radius).dp }
//    var offsetX by remember { mutableStateOf(100f) }
//
//    Canvas(
//        modifier = Modifier
//            .size(boxSize)
//            .offset { IntOffset(offsetX.roundToInt(), 150) }
//            .draggable(
//                orientation = Orientation.Horizontal,
//                state = rememberDraggableState { delta ->
//                    offsetX += delta
//                }
//            ),
//    ) {
//        drawCircle(
//            color = data.color,
//            radius = data.radius,
//            center = data.center
//        )
//    }
//}
