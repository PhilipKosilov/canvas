//package com.example.canvas.trash
//
//import android.util.Log
//import androidx.compose.foundation.clickable
//import androidx.compose.foundation.layout.Box
//import androidx.compose.foundation.layout.offset
//import androidx.compose.foundation.layout.size
//import androidx.compose.foundation.text.BasicTextField
//import androidx.compose.runtime.Composable
//import androidx.compose.runtime.derivedStateOf
//import androidx.compose.runtime.getValue
//import androidx.compose.runtime.mutableStateOf
//import androidx.compose.runtime.remember
//import androidx.compose.runtime.setValue
//import androidx.compose.ui.Alignment
//import androidx.compose.ui.ExperimentalComposeUiApi
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.draw.alpha
//import androidx.compose.ui.draw.drawBehind
//import androidx.compose.ui.focus.FocusRequester
//import androidx.compose.ui.focus.focusRequester
//import androidx.compose.ui.focus.onFocusChanged
//import androidx.compose.ui.geometry.Offset
//import androidx.compose.ui.platform.LocalSoftwareKeyboardController
//import androidx.compose.ui.text.ExperimentalTextApi
//import androidx.compose.ui.text.TextMeasurer
//import androidx.compose.ui.text.TextRange
//import androidx.compose.ui.text.buildAnnotatedString
//import androidx.compose.ui.text.drawText
//import androidx.compose.ui.text.input.TextFieldValue
//import androidx.compose.ui.text.rememberTextMeasurer
//import androidx.compose.ui.unit.dp
//import com.example.canvas.data.CircleData
//
//@OptIn(ExperimentalTextApi::class, ExperimentalComposeUiApi::class)
//@Composable
//fun HiddenTextFieldCircle(
//    data: CircleData
//) {
//    var isEditMode by remember { mutableStateOf(false) }
//    val boxSize = remember { (data.radius).dp }
//    var text by remember { mutableStateOf(data.text) }
//    val textFieldValue by remember(text) {
//        derivedStateOf {
//            TextFieldValue(
//                text = text,
//                selection = TextRange(text.length)
//            )
//        }
//    }
//
//    val textMeasurer: TextMeasurer = rememberTextMeasurer()
//    val labelTextLayout = textMeasurer.measure(
//        text = buildAnnotatedString { append(data.text) },
//        style = data.textStyle
//    )
//    val labelTopLeft = Offset(
//        (labelTextLayout.size.width - 60).toFloat(),
//        (labelTextLayout.size.height + 40).toFloat()
//    )
//    val kc = LocalSoftwareKeyboardController.current
//    val focusRequester = remember { FocusRequester() }
//    Log.i("ricecake", "HiddenTextFieldCircle: recomposition")
//
//    Box(
//        contentAlignment = Alignment.Center,
//        modifier = Modifier
//            .size(boxSize)
//            .offset(data.center.x.dp, data.center.y.dp)
//            .clickable {
//                isEditMode = !isEditMode
//                if (isEditMode) {
//                    focusRequester.requestFocus()
//                } else {
//                    focusRequester.freeFocus()
//                }
//            }
//            .drawBehind {
//                drawCircle(
//                    color = data.color,
//                    radius = data.radius,
//                )
//
//                drawText(
//                    textMeasurer = textMeasurer,
//                    text = text,
//                    topLeft = labelTopLeft,
//                    style = data.textStyle
//                )
//            }
//    ) {
//        BasicTextField(
//            modifier = Modifier
//                .focusRequester(focusRequester)
//                .onFocusChanged {
//                    Log.i("ricecake", "HiddenTextFieldCircle: focus change")
////                    kc?.show()
//                    if (it.isFocused) {
//                        kc?.show()
//                    } else {
//                        kc?.hide()
//                    }
//                }
//                .alpha(0f),
//            value = textFieldValue,
//            onValueChange = {
//                text = it.text
////                text = it
//            }
//        )
//    }
//}
//
//@OptIn(ExperimentalTextApi::class, ExperimentalComposeUiApi::class)
//@Composable
//fun HiddenTextFieldCircle2(
//    data: CircleData
//) {
//    var isEditMode by remember { mutableStateOf(false) }
//    val boxSize = remember { (data.radius).dp }
//    var text by remember { mutableStateOf(data.text) }
//
//    val textMeasurer: TextMeasurer = rememberTextMeasurer()
//    val labelTextLayout = textMeasurer.measure(
//        text = buildAnnotatedString { append(data.text) },
//        style = data.textStyle
//    )
//    val labelTopLeft = Offset(
//        (labelTextLayout.size.width - 60).toFloat(),
//        (labelTextLayout.size.height + 40).toFloat()
//    )
//    val kc = LocalSoftwareKeyboardController.current
//    val focusRequester = remember { FocusRequester() }
//
//    Box(
//        contentAlignment = Alignment.Center,
//        modifier = Modifier
//            .size(boxSize)
//            .offset(data.center.x.dp, data.center.y.dp)
//            .clickable {
//                isEditMode = !isEditMode
//                if (isEditMode) {
//                    focusRequester.requestFocus()
//                } else {
//                    focusRequester.freeFocus()
//                }
//            }
//            .drawBehind {
//                drawCircle(
//                    color = data.color,
//                    radius = data.radius,
//                )
//
//                drawText(
//                    textMeasurer = textMeasurer,
//                    text = text,
//                    topLeft = labelTopLeft,
//                    style = data.textStyle
//                )
//            }
//    ) {
//        BasicTextField(
//            modifier = Modifier
//                .focusRequester(focusRequester)
//                .onFocusChanged {
//                    kc?.show()
//                    if (it.isFocused) {
//                        kc?.show()
//                    } else {
//                        kc?.hide()
//                    }
//                }
//                .alpha(0f),
//            value = text,
//            onValueChange = {
//                text = it
//            }
//        )
//    }
//}