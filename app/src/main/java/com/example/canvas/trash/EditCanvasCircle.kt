//package com.example.canvas.trash
//
//import androidx.compose.foundation.Canvas
//import androidx.compose.foundation.clickable
//import androidx.compose.foundation.layout.Box
//import androidx.compose.foundation.layout.offset
//import androidx.compose.foundation.layout.size
//import androidx.compose.runtime.Composable
//import androidx.compose.runtime.LaunchedEffect
//import androidx.compose.runtime.getValue
//import androidx.compose.runtime.mutableStateOf
//import androidx.compose.runtime.remember
//import androidx.compose.runtime.setValue
//import androidx.compose.ui.Alignment
//import androidx.compose.ui.ExperimentalComposeUiApi
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.geometry.Offset
//import androidx.compose.ui.graphics.Color
//import androidx.compose.ui.platform.LocalSoftwareKeyboardController
//import androidx.compose.ui.text.ExperimentalTextApi
//import androidx.compose.ui.text.TextMeasurer
//import androidx.compose.ui.text.buildAnnotatedString
//import androidx.compose.ui.text.drawText
//import androidx.compose.ui.text.rememberTextMeasurer
//import androidx.compose.ui.unit.dp
//import com.example.canvas.data.CircleData
//import kotlinx.coroutines.delay
//
//@OptIn(ExperimentalTextApi::class, ExperimentalComposeUiApi::class)
//@Composable
//fun EditCanvasCircle(
//    data: CircleData
//) {
//    var isEditMode by remember { mutableStateOf(false) }
//    var showEditCursor by remember { mutableStateOf(false) }
//
//    var text by remember { mutableStateOf(data.text) }
//    val boxSize = remember { (data.radius).dp }
//    val textMeasurer: TextMeasurer = rememberTextMeasurer()
//    val labelTextLayout = textMeasurer.measure(
//        text = buildAnnotatedString { append(data.text) },
//        style = data.textStyle
//    )
//    val labelTopLeft = Offset(
//        (labelTextLayout.size.width + 25).toFloat(),
//        (labelTextLayout.size.height + 40).toFloat()
//    )
//    val kc = LocalSoftwareKeyboardController.current
//
//    LaunchedEffect(showEditCursor) {
//        if (isEditMode) {
//            delay(500)
//            showEditCursor = !showEditCursor
//        }
//    }
//
//    Box(
//        contentAlignment = Alignment.Center,
//        modifier = Modifier
//            .size(boxSize)
//            .offset(data.center.x.dp, data.center.y.dp)
//            .clickable {
//                isEditMode = !isEditMode
//                showEditCursor = isEditMode
//            }
//    ) {
////        Button(
////            modifier = Modifier.offset(y = 100.dp),
////            onClick = {
////                kc?.show()
////            }
////        ) {
////            Text("Keyboard")
////        }
//
//        Canvas(
//            modifier = Modifier.size(100.dp)
//        ) {
//            drawCircle(
//                color = data.color,
//                radius = data.radius,
//            )
//
//            drawText(
//                textMeasurer = textMeasurer,
//                text = text,
//                topLeft = labelTopLeft,
//                style = data.textStyle
//            )
//
//            if (isEditMode && showEditCursor) {
//                drawLine(
//                    color = Color.White,
//                    start = Offset(labelTopLeft.x + 80f, labelTopLeft.y),
//                    end = Offset(
//                        labelTopLeft.x + 80f,
//                        labelTopLeft.y + labelTextLayout.size.height
//                    ),
//                    strokeWidth = 1.dp.toPx()
//                )
//            }
//        }
//    }
//}