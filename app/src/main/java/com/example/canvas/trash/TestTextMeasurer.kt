package com.example.canvas.trash

import android.util.Log
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.TextMeasurer
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.rememberTextMeasurer
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import com.example.canvas.utils.TAG

@OptIn(ExperimentalTextApi::class)
@Composable
fun TestTextMeasurer() {
    val density = LocalDensity.current
    val measurer = rememberTextMeasurer()

    val textSize = with(density) {
        measureSizeDp(
            measurer = measurer,
            text = AnnotatedString("My long text"),
            constraints = Constraints(
                maxWidth = 30.dp.roundToPx()
            )
        )
    }
    Log.i(TAG, "TestTextMeasurer: text $textSize")

}

@OptIn(ExperimentalTextApi::class)
private fun Density.measureSizeDp(
    measurer: TextMeasurer,
    text: AnnotatedString = AnnotatedString(""),
    style: TextStyle = TextStyle.Default,
    constraints: Constraints = Constraints(),
): DpSize {
    val textSize = measurer.measure(
        text = text,
        style = style,
        density = this,
        constraints = constraints
    ).size.toSize()

    return textSize.toDpSize()
}