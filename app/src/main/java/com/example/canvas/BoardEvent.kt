package com.example.canvas

sealed interface BoardEvent{
    class Select(id: Int) : BoardEvent
}