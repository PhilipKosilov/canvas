package com.example.canvas.list_operations

/**
 * Creates copy of the original list to work with
 */
internal fun <T> MutableList<T>.beginOperation(): ListOperation<T> {
    println("beginOperation: are equals = ${this == this.toMutableList()}")
    return ListOperation(original = this, items = toMutableList())
}

// Don't nest calls or item index will not work properly.
internal open class ListOperation<T>(
    private val original: MutableList<T>,
    private val items: MutableList<T>,
) {
    //After adding new elements their position will change, but we'll be working with this element.
    fun add(index: Int, item: T): ItemOperation<T> {
        items.add(index, item)
        return ItemOperation(original = original, index = index, item = item, items = items)
    }

    fun add(item: T): ItemOperation<T> {
        return add(index = items.size, item = item)
    }

    fun addFirst(item: T): ItemOperation<T> {
        return add(index = 0, item = item)
    }

    fun removeAt(index: Int): ItemOperation<T> {
        val item = items[index]
        if (item != null) items.removeAt(index)
        return ItemOperation(original = original, index = -1, item = item, items = items)
    }

    fun remove(item: T): ItemOperation<T> {
        val index = items.indexOf(item)
        return removeAt(index)
    }

    fun removeFirst(): ItemOperation<T> {
        return removeAt(0)
    }

    fun removeLast(): ItemOperation<T> {
        return removeAt(items.lastIndex)
    }

    fun find(predicate: (T) -> Boolean): ItemOperation<T> {
        val index = items.indexOfFirst(predicate)
        val item = if (index != -1) items[index] else null
        return ItemOperation(original = original, index = index, item = item, items = items)
    }

    fun then(function: (items: MutableList<T>) -> Unit): ListOperation<T> {
        function(items)
        return this
    }

    // todo For now returns List, but can take MutableList and just apply new state here
    fun finishOperation() {
        println("finish operation")
        println("original == items ${original == items}")

        if (original != items) {
            original.clear()
            original.addAll(items)
        }
    }
}

internal class ItemOperation<T>(
    val original: MutableList<T>,
    private val index: Int,
    private val item: T?,
    private val items: MutableList<T>,
) : ListOperation<T>(original, items) {

    fun replaceItem(function: (item: T, items: MutableList<T>) -> T?): ItemOperation<T> {
        if (item == null) return this

        val replacement = function(item, items)
        return if (replacement != null && index in 0..items.size) {
            items[index] = replacement
            ItemOperation(original = original, index = index, item = replacement, items = items)
        } else this
    }

    fun replaceItemWithCondition(
        condition: (item: T?, items: MutableList<T>) -> Boolean,
        function: (item: T, items: MutableList<T>) -> T?
    ): ItemOperation<T> {
        if (condition(item, items)) replaceItem(function)
        return this
    }

    fun then(function: (item: T?, items: MutableList<T>) -> Unit): ItemOperation<T> {
        function(item, items)
        return this
    }
}